/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.civicrm

import dev.maximilian.feather.civicrm.internal.civicrm.ACLProtocol
import dev.maximilian.feather.iog.internal.settings.CiviCRMNames
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.iog.testframework.CRMApiCalls
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.testutils.ServiceConfig
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateCRMSystemTest {
    @Test
    fun `Check Precondition, Create, Delete, Check Precondition again returns CREATED and creates all groups`() {
        val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
        val apiTestUtilities = ApiTestUtilities()

        apiTestUtilities.startIogPlugin(admin = true, backgroundJobManager = backgroundJobManager, true)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
        apiTestUtilities.createRGTest()
        apiTestUtilities.createProject("deu-iog66", "DEU-IOG66")
        apiTestUtilities.loginWithAdminUser()
        val crm = CRMApiCalls(apiTestUtilities, backgroundJobManager)
        crm.deleteCMRSystemBlocking()

        ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName("rg-test-crm")?.let { ServiceConfig.CREDENTIAL_PROVIDER.deleteGroup(it) }

        assertPreconditionsForCiviCRM(apiTestUtilities)
        crm.createCRMSystemBlocking(false)
        crm.createCRMSystemBlocking(true)

        runBlocking {
            val civiGroups = apiTestUtilities.scenario?.civiCRM?.civicrm?.getGroups()!!
            assertTrue(
                civiGroups.count() > 8,
                "Not enough groups CIVICRM. Expected are minimum 9. But there are only ${civiGroups.count()}. They are: ${civiGroups.joinToString { it.name }}",
            )

            assertTrue(
                civiGroups.any { it.name == LdapNames.ETHIC_VALIDATION_GROUP },
                "${LdapNames.ETHIC_VALIDATION_GROUP} were not created in CIVICRM",
            )
            assertTrue(
                civiGroups.any { it.name == LdapNames.CENTRAL_OFFICE },
                "${LdapNames.CENTRAL_OFFICE} was not created in CIVICRM",
            )
            assertTrue(
                civiGroups.any { it.name == LdapNames.ASSOCIATION_BOARD },
                "${LdapNames.ASSOCIATION_BOARD} was not created in CIVICRM",
            )
            assertTrue(
                civiGroups.any { it.name == CiviCRMNames.CONTACT_GROUP_NAME },
                "${CiviCRMNames.CONTACT_GROUP_NAME} was not created in CIVICRM",
            )
            assertTrue(
                civiGroups.any { it.name == CiviCRMNames.ORGANIZATION_GROUP_NAME },
                "${CiviCRMNames.ORGANIZATION_GROUP_NAME} was not created in CIVICRM",
            )
            assertTrue(
                civiGroups.any { it.name == CiviCRMNames.PERSON_GROUP_NAME },
                "${CiviCRMNames.PERSON_GROUP_NAME} was not created in CIVICRM",
            )
            assertTrue(
                civiGroups.any { it.name == CiviCRMNames.USER_GROUP_NAME },
                "${CiviCRMNames.USER_GROUP_NAME} was not created in CIVICRM",
            )
            assertFalse(
                civiGroups.any { it.name == "deu-iog66-crm" },
                "deu-iog66-crm was created in CIVICRM, but should not-",
            )
            val contactGroupID = setOf(civiGroups.single { it.name == CiviCRMNames.CONTACT_GROUP_NAME }.id.toString())
            assertEquals(
                contactGroupID,
                civiGroups.single { it.name == CiviCRMNames.PERSON_GROUP_NAME }.parents,
                "${CiviCRMNames.CONTACT_GROUP_NAME} is not parent of ${CiviCRMNames.PERSON_GROUP_NAME}  ",
            )

            assertEquals(
                contactGroupID,
                civiGroups.single { it.name == CiviCRMNames.PERSON_GROUP_NAME }.parents,
                "${CiviCRMNames.CONTACT_GROUP_NAME} is not parent of ${CiviCRMNames.PERSON_GROUP_NAME}  ",
            )
            val userGroupID = setOf(civiGroups.single { it.name == CiviCRMNames.USER_GROUP_NAME }.id.toString())
            assertEquals(
                userGroupID,
                civiGroups.single { it.name == LdapNames.CENTRAL_OFFICE }.parents,
                "${CiviCRMNames.USER_GROUP_NAME} is not parent of ${LdapNames.CENTRAL_OFFICE}  ",
            )
            assertEquals(
                userGroupID,
                civiGroups.single { it.name == LdapNames.ASSOCIATION_BOARD }.parents,
                "${CiviCRMNames.USER_GROUP_NAME} is not parent of ${LdapNames.ASSOCIATION_BOARD}  ",
            )
            assertEquals(
                userGroupID,
                civiGroups.single { it.name == LdapNames.ETHIC_VALIDATION_GROUP }.parents,
                "${CiviCRMNames.USER_GROUP_NAME} is not parent of ${LdapNames.ETHIC_VALIDATION_GROUP}  ",
            )
            val contactTypes = apiTestUtilities.scenario?.civiCRM?.civicrm?.getContactTypes()!!
            IogPluginConstants.CiviCRMCustomType.values().forEach {
                    ct ->
                assertTrue(
                    contactTypes.any { foundType -> foundType.name == ct.protocolName && foundType.label == ct.label },
                    "Contact type ${ct.protocolName}} was not created in CIVICRM with label ${ct.label}",
                )
            }
            val aclEntities = apiTestUtilities.scenario?.civiCRM?.civicrm?.getACLs()!!
            assertTrue(
                aclEntities.any {
                    it.name == "Nutzer können sich sehen" &&
                        it.operation == ACLProtocol.Operations.VIEW.protocolName &&
                        it.entityTable == "civicrm_acl_role" &&
                        it.objectTable == "civicrm_saved_search" &&
                        it.objectId == civiGroups.single { it.name == CiviCRMNames.USER_GROUP_NAME }.id
                },
                "ACL <Nutzer können sich sehen> does not exist",
            )
            assertTrue(
                aclEntities.any {
                    it.name == LdapNames.ASSOCIATION_BOARD && it.operation == ACLProtocol.Operations.EDIT.protocolName &&
                        it.entityTable == "civicrm_acl_role" && it.objectTable == "civicrm_saved_search"
                },
                "ACL for ${LdapNames.ASSOCIATION_BOARD} was not created",
            )
            IogPluginConstants.CiviCRMCustomType.values().forEach {
                    ct ->
                assertTrue(
                    contactTypes.any { foundType ->
                        foundType.name == ct.protocolName &&
                            foundType.label == ct.label
                    },
                    "Contact type ${ct.protocolName}} was not created in CIVICRM with label ${ct.label}",
                )
            }
        }
        crm.deleteCMRSystemBlocking()
        crm.createCRMSystemBlocking(false)
    }

    private fun assertPreconditionsForCiviCRM(apiTestUtilities: ApiTestUtilities) {
        assertNotNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(LdapNames.IOG_MEMBERS),
            "IOG Member not created before precondition",
        )
        assertNotNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(LdapNames.INTERESTED_PEOPLE),
            "INTERESTED_PEOPLE not created before precondition",
        )
        assertNotNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(LdapNames.TRIAL_MEMBERS),
            "TRIAL_MEMBERS not created before precondition",
        )
        assertNotNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(LdapNames.ETHIC_VALIDATION_GROUP),
            "ETHIC_VALIDATION_GROUP not created before precondition",
        )

        ServiceConfig.CREDENTIAL_PROVIDER.getGroups().filter {
            it.name.endsWith(IogPluginConstants.INTERESTED_SUFFIX) &&
                it.name.startsWith(IogPluginConstants.RG_PREFIX)
        }.forEach {
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.INTERESTED_SUFFIX)
            assertNotNull(
                ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(groupNameWithoutPrefix),
                "Konnte Basisgroupp $groupNameWithoutPrefix nicht finden",
            )
            assertNotNull(
                ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX),
                "Konnte Admingruppe ${groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX} nicht finden",
            )
            assertNotNull(
                ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.TRIAL_SUFFIX),
                "Konnte Trialgruppe ${groupNameWithoutPrefix + IogPluginConstants.TRIAL_SUFFIX} nicht finden",
            )
        }
        ServiceConfig.CREDENTIAL_PROVIDER.getGroups().filter { g -> g.name.endsWith(IogPluginConstants.CRM_SUFFIX) }.forEach {
            assertTrue(false, "CRM-Gruppe ${it.name} existiert bereits. Bitte alle CRM-Gruppen vor Erstellung löschen.")
        }

        assertNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(CiviCRMNames.USER_GROUP_NAME),
            "${CiviCRMNames.USER_GROUP_NAME} in civicrm gefunden. Diese muss vorher gelöscht sein.",
        )
        assertNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(CiviCRMNames.CONTACT_GROUP_NAME),
            "${CiviCRMNames.CONTACT_GROUP_NAME} in civicrm gefunden. Diese muss vorher gelöscht sein.",
        )

        try {
            apiTestUtilities.scenario?.civiCRM?.civicrm?.findIndexOfACLRole()
        } catch (ex: Exception) {
            assertTrue(false, "ACL Rolle muss in civicrm erfüllt sein")
        }
    }

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
