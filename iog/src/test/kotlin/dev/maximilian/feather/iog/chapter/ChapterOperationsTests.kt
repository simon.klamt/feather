/*
 *    Copyright [2020-2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.chapter

import dev.maximilian.feather.User
import dev.maximilian.feather.iog.BackgroundJobWithStrings
import dev.maximilian.feather.iog.api.bindings.ChapterDetails
import dev.maximilian.feather.iog.api.bindings.ChapterDetailsRequest
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJob
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.GetRequest
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ChapterOperationsTests {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val api = ApiTestUtilities()
    private var testUser: User?

    init {
        api.startIogPlugin(admin = true, backgroundJobManager = backgroundJobManager, true)
        api.createStandardGroups()
        runBlocking {
            api.scenario!!.opUtilities.createStandardProjects()
        }
        testUser = ServiceConfig.CREDENTIAL_PROVIDER.getUserByUsername(TestUser.ADMIN_USER.username)
        api.createRGTest()
        api.createProject("deu-iog55", "DEU-IOG55")
    }

    @Test
    fun `Get CHAPTERS without admin returns FORBIDDEN`() {
        api.loginWithStandardUser()
        val res = getAllGroupIDs().asEmpty()
        api.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, res.status)
    }

    @Test
    fun `Get CHAPTER details without admin returns FORBIDDEN`() {
        api.loginWithStandardUser()
        val res = getDetails(ChapterDetailsRequest(emptyList())).asEmpty()
        api.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, res.status)
    }

    @Test
    fun `Get CHAPTER NAMES returns correct body`() {
        api.loginWithAdminUser()
        api.scenario!!.iogPlugin!!.chapterDB.deleteAll()
        api.scenario!!.iogPlugin!!.chapterDB.storeChapter(
            UUID.randomUUID(),
            "rg-blabla", IogGroupSchema.PatternTypes.MemberCrmInterested,
            GroupKind.REGIONAL_GROUP, "IOG",
            setOf(
                PermissionType.Read,
                PermissionType.Update,
                PermissionType.Create,
                PermissionType.Delete,
            ),
            "iog", "blabla", "DESCRIPTION", false,
        )
        api.scenario!!.iogPlugin!!.chapterDB.storeChapter(
            UUID.randomUUID(),
            "oms", IogGroupSchema.PatternTypes.SimpleMemberAdmin,
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET, "IOG",
            setOf(
                PermissionType.Read,
                PermissionType.Update,
                PermissionType.Create,
                PermissionType.Delete,
            ),
            "iog", "oms", "DESCRIPTION2", false,
        )

        val id1 = api.scenario!!.iogPlugin!!.chapterDB.getID("rg-blabla")!!
        val id2 = api.scenario!!.iogPlugin!!.chapterDB.getID("oms")!!
        val res = getAllGroupIDs().asObject<List<String>>()
        assertEquals(
            listOf(id2, id1),
            res.body,
        )
        assertEquals(HttpStatus.OK_200, res.status)
    }

    @Test
    fun `Create Chapter DB with simulation returns CREATED but does not change db`() {
        api.loginWithAdminUser()
        api.scenario!!.iogPlugin!!.chapterDB.deleteAll()
        api.scenario!!.iogPlugin!!.chapterDB.storeChapter(
            UUID.randomUUID(),
            "rg-blabla", IogGroupSchema.PatternTypes.MemberCrmInterested,
            GroupKind.REGIONAL_GROUP, "IOG",
            setOf(
                PermissionType.Read,
                PermissionType.Update,
                PermissionType.Create,
                PermissionType.Delete,
            ),
            "iog", "blabla", "DESCRIPTION", false,
        )
        val t = simulateChapterDB().asObject<BackgroundJobWithStrings>()

        // wait until job is completed (otherwise the parallel execution breaks the test)
        var jobWorking: BackgroundJob
        do {
            jobWorking = backgroundJobManager.getJobStatus(t.body.id)!!
            Thread.sleep(100)
        } while (jobWorking.completed == null)
        assertEquals(HttpStatus.CREATED_201, t.status)
        val n = api.scenario!!.iogPlugin!!.chapterDB.getAllChapterNames()
        assertEquals(listOf("rg-blabla"), n, "database was filled, but should not be changed in simulation mode")
    }

    @Test
    fun `Get CHAPTER DETAILS returns correct body`() {
        api.loginWithAdminUser()
        api.scenario!!.iogPlugin!!.chapterDB.deleteAll()
        api.scenario!!.iogPlugin!!.chapterDB.storeChapter(
            UUID.randomUUID(),
            "rg-blabla", IogGroupSchema.PatternTypes.MemberCrmInterested,
            GroupKind.REGIONAL_GROUP, "IOG\\RG-Intern\\RG Blabla",
            setOf(
                PermissionType.Read,
                PermissionType.Update,
                PermissionType.Create,
                PermissionType.Delete,
            ),
            "rg-intern", "rg-blabla", "RG Blabla", false,
        )

        val id = api.scenario!!.iogPlugin!!.chapterDB.getID("rg-blabla")!!
        val res = getDetails(ChapterDetailsRequest(listOf(id))).asObject<List<ChapterDetails>>()
        assertEquals(
            listOf(
                ChapterDetails(
                    id,
                    "rg-blabla",
                    IogGroupSchema.PatternTypes.MemberCrmInterested,
                    GroupKind.REGIONAL_GROUP,
                    "IOG\\RG-Intern\\RG Blabla",
                    setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                    "rg-intern",
                    "rg-blabla",
                    "RG Blabla",
                    false,
                ),
            ),
            res.body,
        )
        assertEquals(HttpStatus.OK_200, res.status)
    }

    private fun getAllGroupIDs(): GetRequest {
        return api
            .restConnection
            .get("${api.scenario!!.basePath}/bindings/iog/chapter/db")
    }

    private fun getDetails(a: ChapterDetailsRequest): RequestBodyEntity {
        return api
            .restConnection
            .post("${api.scenario!!.basePath}/bindings/iog/chapter/details").body(a)
    }

    private fun simulateChapterDB(): RequestBodyEntity {
        return api
            .restConnection
            .post("${api.scenario!!.basePath}/bindings/iog/chapter/db").body(true)
    }

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
