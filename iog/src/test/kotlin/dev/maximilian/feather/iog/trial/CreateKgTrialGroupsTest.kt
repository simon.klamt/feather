/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.trial

import dev.maximilian.feather.iog.BackgroundJobWithStrings
import dev.maximilian.feather.iog.internal.chapter.TrialOperationResult
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.settings.OPRoleNames
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJob
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.testutils.ServiceConfig
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateKgTrialGroupsTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val apiTestUtilities = ApiTestUtilities()

    init {
        apiTestUtilities.startIogPlugin(true, backgroundJobManager, true)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
    }

    private val credentials = ServiceConfig.CREDENTIAL_PROVIDER

    @Test
    fun `Create kg-trial groups with repair by standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val response = createKgTrialByApi(true).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `Create kg-trial groups without repair by standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val response = createKgTrialByApi(false).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `Create kg-trial groups creates correct trial-groups, assignes OP roles and adds group to NC`() {
        apiTestUtilities.createKG("kg-test", "KG Test")
        apiTestUtilities.removeKgTrialGroups()

        val oldMember = credentials.getGroupByName("kg-test")
        val oldAdmin = credentials.getGroupByName("kg-test-admin")
        val oldMember2 = credentials.getGroupByName("kg-test-member")

        assertNotNull(oldMember, "kg-test existiert nicht vor dem Trial-Konvertierungstest")
        assertNotNull(oldAdmin, "kg-test-admin existiert nicht vor der Trial-Konvertierungstest")
        assertNull(oldMember2, "kg-test-member existiert noch vor der Trial-Konvertierungstest")

        apiTestUtilities.loginWithAdminUser()
        // test only
        val response = createKgTrialByApi(false).asObject<BackgroundJobWithStrings>()
        assertEquals(HttpStatus.CREATED_201, response.status)
        val job = response.body
        assertNotNull(job)
        assertEquals("ADMIN", job.permission)
        assertEquals("started", job.status)

        // wait until job is completed (otherwise the parallel execution breaks the test)
        var jobWorking: BackgroundJob
        do {
            jobWorking = backgroundJobManager.getJobStatus(job.id)!!
            Thread.sleep(100)
        } while (jobWorking.completed == null)
        Thread.sleep(500)
        assertEquals("OK", backgroundJobManager.getJobResults<TrialOperationResult>(jobWorking)!!.status)

        val oldMemberAfterTest = credentials.getGroupByName("kg-test")
        val oldAdminAfterTest = credentials.getGroupByName("kg-test-admin")
        val oldMember2AfterTest = credentials.getGroupByName("kg-test-member")

        assertNotNull(oldMemberAfterTest, "kg-test existiert nicht mehr vor der Trial-Konvertierung")
        assertNotNull(oldAdminAfterTest, "kg-test-admin existiert nicht mehr vor der Trial-Konvertierung")
        assertNull(oldMember2AfterTest, "kg-test-member existiert wieder vor der Trial-Konvertierung")

        // repair now
        val response2 = createKgTrialByApi(true).asObject<BackgroundJobWithStrings>()
        assertEquals(HttpStatus.CREATED_201, response2.status)
        val job2 = response2.body
        assertNotNull(job2)
        // assertEquals(Permission.ADMIN, job.permission)
        assertEquals("ADMIN", job2.permission)
        assertEquals("started", job2.status)

        // wait until job is completed (otherwise the parallel execution breaks the test)
        var jobWorking2: BackgroundJob
        do {
            jobWorking2 = backgroundJobManager.getJobStatus(job2.id)!!
            Thread.sleep(100)
        } while (jobWorking2.completed == null)
        Thread.sleep(500)
        assertEquals("OK", backgroundJobManager.getJobResults<TrialOperationResult>(jobWorking2)!!.status)

        assertEquals(HttpStatus.CREATED_201, response2.status)

        val t = credentials.getGroupByName("kg-test-trial")
        val members = credentials.getGroupByName("kg-test-member")
        val kg1Links: List<Pair<String, String>>
        val gsLinks: List<Pair<String, String>>
        val apLinks: List<Pair<String, String>>
        runBlocking {
            val op = apiTestUtilities.scenario!!.openProject!!
            val memberships = op.getMemberships(op.getRoles())
            kg1Links =
                memberships.filter { it.project.value?.name == "KG Test" && it.principal.value is OpenProjectGroup }
                    .map { Pair((it.principal.value as OpenProjectGroup).name, it.roles.map { role -> role.name }.joinToString()) }
            gsLinks =
                memberships.filter { it.project.value?.name == "Geschäftsstelle" && it.principal.value is OpenProjectGroup }
                    .map { Pair((it.principal.value as OpenProjectGroup).name, it.roles.map { role -> role.name }.joinToString()) }
            apLinks =
                memberships.filter { it.project.value?.name == "AP SprecherInnen" && it.principal.value is OpenProjectGroup }
                    .map { Pair((it.principal.value as OpenProjectGroup).name, it.roles.map { role -> role.name }.joinToString()) }
        }

        assertNotNull(t, "Die Gruppe kg-test-trial wurde nicht in LDAP erstellt.")
        assertNotNull(
            members,
            "Die Gruppe kg-test-member wurde nicht in LDAP erstellt. Die Gruppen sind: ${
                credentials.getGroups().joinToString { it.name }
            }",
        )
        runBlocking {
            assertTrue(
                apiTestUtilities.scenario!!.openProject!!.getGroups()
                    .any { it.name == "kg-test-trial" },
                "Es wurde die kg-test-trial-Gruppe nicht in OP angelegt.",
            )
            assertTrue(
                apiTestUtilities.scenario!!.openProject!!.getGroups()
                    .any { it.name == "kg-test-member" },
                "Es wurde die kg-test-member-Gruppe nicht in OP angelegt.",
            )
            assertTrue(
                apiTestUtilities.scenario!!.openProject!!.getGroups()
                    .any { it.name == OPNameConfig(mutableMapOf()).trialMemberGroupName },
                "Es wurde die TrialMembers nicht in OP angelegt.",
            )
        }
        val onc = OPNameConfig(mutableMapOf())
        assertEquals(
            listOf(
                Pair(onc.interestedPeopleGroupName, OPRoleNames.INTERESTED),
                Pair(onc.iogMembersGroupName, OPRoleNames.READER),
                Pair("geschaeftsstelle", OPRoleNames.MEMBER),
                Pair("kg-test-admin", OPRoleNames.PROJECT_ADMIN),
                Pair("kg-test-member", OPRoleNames.MEMBER),
                Pair("kg-test-trial", OPRoleNames.MEMBER),
            ),
            kg1Links.sortedBy { it.first },
        )
        assertEquals(
            listOf(
                Pair(onc.interestedPeopleGroupName, OPRoleNames.INTERESTED),
                Pair(onc.iogMembersGroupName, OPRoleNames.INTERESTED),
                Pair("geschaeftsstelle", OPRoleNames.MEMBER),
                Pair("geschaeftsstelle-admin", OPRoleNames.PROJECT_ADMIN),
            ),
            gsLinks.sortedBy { it.first },
        )
        assertEquals(
            listOf(
                Pair(onc.interestedPeopleGroupName, OPRoleNames.INTERESTED),
                Pair(onc.iogMembersGroupName, OPRoleNames.READER),
                Pair("ap-sprecherinnen", OPRoleNames.MEMBER),
                Pair("ap-sprecherinnen-admin", OPRoleNames.PROJECT_ADMIN),
                Pair("geschaeftsstelle", OPRoleNames.MEMBER),
            ),
            apLinks.sortedBy { it.first },
        )

        val ncGroups =
            apiTestUtilities.scenario!!.ncUtilities.nextcloud.findGroupFolder(NextcloudFolders.GroupShare)?.groups?.mapNotNull {
                it.key
            } ?: emptyList()

        assertTrue(
            ncGroups.contains("kg-test-trial"),
            "NC Groupfolder enthält kg-test-trial nicht.",
        )

        apiTestUtilities.removeKG("kg-test", "KG Test")
    }

    private fun createKgTrialByApi(autorepair: Boolean): RequestBodyEntity =
        apiTestUtilities
            .restConnection
            .post("${apiTestUtilities.scenario!!.basePath}/bindings/iog/chapter/kgtrials")
            .body(autorepair)

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
