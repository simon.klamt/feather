/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.group

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.api.bindings.IogGroupDeleteRequest
import dev.maximilian.feather.iog.internal.group.GroupDetails
import dev.maximilian.feather.iog.internal.group.NextcloudDetails
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.GetRequest
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IogGroupApiTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val credentials = ServiceConfig.CREDENTIAL_PROVIDER
    private val apiTestUtilities = ApiTestUtilities()

    init {
        apiTestUtilities.startIogPlugin(true, backgroundJobManager, true)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
    }

    @Test
    fun `GET group details for admin returns OK (200)`() {
        apiTestUtilities.loginWithAdminUser()

        val group = Group(0, "rg-test", "RG Test", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        val response = getGroupDetails(apiTestUtilities, createdGroup.id).asEmpty()
        assertEquals(HttpStatus.OK_200, response.status)
    }

    @Test
    fun `GET group details for standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val group = Group(0, "rg-test2", "RG Test2", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        val response = getGroupDetails(apiTestUtilities, createdGroup.id).asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `GET group details for node admin returns OK (200)`() {
        apiTestUtilities.loginWithStandardUser()
        val userID = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.id
        val adminGroup = Group(0, "rg-test3-admin", "lalala admin", setOf(userID), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdAdminGroup = credentials.createGroup(adminGroup)
        val memberGroup = Group(0, "rg-test3", "RG Test3", emptySet(), emptySet(), setOf(), setOf(), setOf(createdAdminGroup.id), setOf())
        val createdGroup = credentials.createGroup(memberGroup)
        val response = getGroupDetails(apiTestUtilities, createdGroup.id).asEmpty()
        assertEquals(HttpStatus.OK_200, response.status)
    }

    @Test
    fun `GET group details for direct group member returns OK (200)`() {
        apiTestUtilities.loginWithStandardUser()
        val userID = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.id
        val memberGroup = Group(0, "rg-test4", "lalala", setOf(userID), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(memberGroup)
        val response = getGroupDetails(apiTestUtilities, createdGroup.id).asEmpty()
        assertEquals(HttpStatus.OK_200, response.status)
    }

    @Test
    fun `GET group details for not existing group returns NOT FOUND (404)`() {
        apiTestUtilities.loginWithAdminUser()
        val response = getGroupDetails(apiTestUtilities, -42).asEmpty()
        assertEquals(HttpStatus.NOT_FOUND_404, response.status)
    }

//    @Test
//    fun `GET group details with openproject service delivers correct body`() {
//        val apiTestUtilities = ApiTestUtilities()
//        val scenario = apiTestUtilities.startWithoutService(true)
//
//        val openProjectSettings = OpenProjectSettings("username", TestUtil.host, TestUtil.authUser, TestUtil.authPassword, TestUtil.ssoSecret, TestUtil.host)
//        val openProjectService = OpenProjectService(ApiTestUtilities.backgroundJobManager, apiTestUtilities.groupSyncMock, openProjectSettings, apiTestUtilities.credentialProvider)
//
//        val multiService = Multiservice(getConfig(apiTestUtilities))
//        multiService.services.add(openProjectService)
//        multiService.startApis(scenario.app)
//        val mainproject = OpenProjectProject(
//            1,
//            "IOG",
//            "iog",
//            OpenProjectDescription("", "iog","op/iog"),
//            null as OpenProjectProject?
//        )
//
//        var projectCreated : OpenProjectProject?
//        runBlocking {
//            val mainProjectCreated = openProjectService.openproject.createProject(mainproject)
//            val rgproject = OpenProjectProject(
//                2,
//                "RG Intern",
//                "rg intern",
//                OpenProjectDescription("", "rg-intern", "op/iog/rg-intern"),
//                mainProjectCreated
//            )
//            val rgprojectCreated = openProjectService.openproject.createProject(rgproject)
//            val project = OpenProjectProject(
//                3,
//                "RG Test",
//                "rg-test",
//                OpenProjectDescription("", "description of rg-test", "op/iog/rg-intern/rg-test"),
//                rgprojectCreated
//            )
//            projectCreated = openProjectService.openproject.createProject(project)
//        }
//
//        var openProjectGroup = OpenProjectGroup(1, "rg-test-member")
//        runBlocking {
//            openProjectGroup = openProjectService.openproject.createGroup(openProjectGroup)
//        }
//        val openProjectMembership = OpenProjectMembership(1, projectCreated!!,
//            OpenProjectPrincipalLazyEntity(openProjectGroup.id, OpenProjectPrincipalType.GROUP), listOf(OpenProjectRole(1, "Member")))
//        openProjectService.openproject.createMembership(openProjectMembership)
//
//        val group = Group("", "rg-test", "lalala", "", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
//        val createdGroup = apiTestUtilities.credentialProvider.createGroup(group)
//        val response = getGroupDetails(apiTestUtilities, createdGroup.id).asObject<GroupDetails>()
//        assertEquals(GroupDetails(listOf(createdGroup), OpenProjectDetails(listOf(openProjectGroup), projectCreated, null), null), response.body)
//    }

    @Test
    fun `GET group details with nextcloud service delivers correct body`() {
        apiTestUtilities.loginWithAdminUser()
        createRGFolder("RG Test5")

        val group = Group(0, "rg-test5", "RG Test5", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        val response = getGroupDetails(apiTestUtilities, createdGroup.id).asObject<GroupDetails>()

        apiTestUtilities.scenario!!.nextcloud!!.delete(NextcloudFolders.GroupShare + "/" + NextcloudFolders.RgIntern + "/RG Test5")
        assertEquals(
            GroupDetails(
                listOf(createdGroup),
                null,
                NextcloudDetails(NextcloudFolders.GroupShare + "/" + NextcloudFolders.RgIntern + "/RG Test5", 4),
            ),
            response.body,
        )
    }

    @Test
    fun `DELETE group by admin returns CREATED (201)`() {
        apiTestUtilities.loginWithAdminUser()
        createRGFolder("RG Test6")

        val group = Group(0, "rg-test6", "RG Test6", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        val response = deleteGroup(apiTestUtilities, createdGroup.id, TestUser.ADMIN_USER_PASSWORD, true).asEmpty()
        assertEquals(HttpStatus.CREATED_201, response.status)
    }

    @Test
    fun `DELETE group with wrong password returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithAdminUser()
        createRGFolder("RG Test7")

        val group = Group(0, "rg-test7", "RG Test7", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        val response = deleteGroup(apiTestUtilities, createdGroup.id, TestUser.ADMIN_USER_PASSWORD + "WRONG", true).asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `DELETE group not verified returns BAD REQUEST (400)`() {
        apiTestUtilities.loginWithAdminUser()
        createRGFolder("RG Test8")

        val group = Group(0, "rg-test8", "RG Test8", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        val response = deleteGroup(apiTestUtilities, createdGroup.id, TestUser.ADMIN_USER_PASSWORD, false).asEmpty()
        assertEquals(HttpStatus.BAD_REQUEST_400, response.status)
    }

    @Test
    fun `DELETE group by standard user returns Forbidden (403)`() {
        apiTestUtilities.loginWithStandardUser()
        createRGFolder("RG Test9")

        val group = Group(0, "rg-test9", "RG Test9", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        val response = deleteGroup(apiTestUtilities, createdGroup.id, TestUser.NORMAL_USER_PASSWORD, true).asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `DELETE group with nextcloud returns CREATED (201)`() {
        apiTestUtilities.loginWithAdminUser()
        createRGFolder("RG Test10")

        val group = Group(0, "rg-test10", "RG Test10", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        val response = deleteGroup(apiTestUtilities, createdGroup.id, TestUser.ADMIN_USER_PASSWORD, true).asEmpty()
        assertEquals(HttpStatus.CREATED_201, response.status)
    }

    @Test
    fun `DELETE group with nextcloud really deletes`() {
        apiTestUtilities.loginWithAdminUser()
        createRGFolder("RG Test11")
        val group = Group(0, "rg-test11", "RG Test11", emptySet(), emptySet(), setOf(), setOf(), setOf(), setOf())
        val createdGroup = credentials.createGroup(group)
        deleteGroup(apiTestUtilities, createdGroup.id, TestUser.ADMIN_USER_PASSWORD, true).asEmpty()
        val folders = apiTestUtilities.scenario!!.nextcloud!!.listFolders("IOG/RG-Intern")
        assertFalse { folders.contains("RG Test11") }
    }

    private fun deleteGroup(
        apiTestUtilities: ApiTestUtilities,
        groupID: Int,
        pwd: String,
        verified: Boolean,
    ): RequestBodyEntity =
        apiTestUtilities.restConnection.delete("${apiTestUtilities.scenario!!.basePath}/bindings/multiservice/groups/$groupID").body(
            IogGroupDeleteRequest(pwd, verified),
        )

    private fun getGroupDetails(
        apiTestUtilities: ApiTestUtilities,
        groupID: Int,
    ): GetRequest = apiTestUtilities.restConnection.get("${apiTestUtilities.scenario!!.basePath}/bindings/multiservice/groups/$groupID")

    private fun createRGFolder(name: String) =
        apiTestUtilities.scenario!!.ncUtilities.createFolderStructureIfNecessary(
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
            name,
        )

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
