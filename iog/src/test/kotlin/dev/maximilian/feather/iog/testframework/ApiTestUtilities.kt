/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.AppBuilder
import dev.maximilian.feather.Group
import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.authorization.CredentialAuthorizer
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.authorization.api.LoginRequest
import dev.maximilian.feather.authorization.api.SessionAnswer
import dev.maximilian.feather.civicrm.CiviCRMService
import dev.maximilian.feather.civicrm.CiviCRMSettings
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.iog.IogPlugin
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.groupPattern.IGroupPattern
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminInterestedPattern
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminTrialPattern
import dev.maximilian.feather.iog.internal.groupPattern.MemberCrmInterestedPattern
import dev.maximilian.feather.iog.internal.groupPattern.SimpleMemberAdminPattern
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.settings.OPPName
import dev.maximilian.feather.iog.mockserver.GroupSyncMock
import dev.maximilian.feather.iog.mockserver.SessionTerminatorMock
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogConfig
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.iog.settings.SupportMembershipFeature
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.Multiservice
import dev.maximilian.feather.multiservice.events.UserAddedEvent
import dev.maximilian.feather.multiservice.events.UserRemovedFromGroupEvent
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.nextcloud.NextcloudSettings
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.multiservice.settings.ChangeMailSetting
import dev.maximilian.feather.multiservice.settings.MultiServiceConfig
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import dev.maximilian.feather.multiservice.settings.SMTPSetting
import dev.maximilian.feather.multiservice.settings.SendMailContent
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import dev.maximilian.feather.testutils.getEnv
import kong.unirest.core.Config
import kong.unirest.core.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.Database
import redis.clients.jedis.JedisPool
import java.sql.DriverManager
import java.time.Duration
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class ApiTestUtilities {
    companion object {
        val db =
            Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:feather;DB_CLOSE_DELAY=-1") })
        private const val BASE_BATH_PREFIX = "http://127.0.0.1"
        private const val BASE_PATH_SUFFIX = "/v1"

        internal val pool: JedisPool by lazy { ServiceConfig.JEDIS_POOL }
    }

    private val credentialProvider = ServiceConfig.CREDENTIAL_PROVIDER
    private var iogPlugin: IogPlugin? = null

    internal val nextcloudSettings =
        NextcloudSettings(
            getEnv("NEXTCLOUD_USER_BINDING", "username"),
            getEnv("NEXTCLOUD_PUBLIC_URL", "http://127.0.0.1:8082"),
            getEnv("NEXTCLOUD_BASE_URL", "http://127.0.0.1:8082"),
            getEnv("NEXTCLOUD_USER", "ncadmin"),
            getEnv("NEXTCLOUD_PWP", "ncadminsecret"),
        )

    private val civiSetting =
        CiviCRMSettings(
            "API", "User", "Administrators",
            getEnv("CIVICRM_HOST", "http://localhost:8083"), getEnv("CIVICRM_APIKEY", "WuM5g2nuG8kFHXLID74p"),
            setOf(
                LdapNames.CENTRAL_OFFICE,
                LdapNames.ETHIC_VALIDATION_GROUP,
                LdapNames.ASSOCIATION_BOARD,
            ),
            setOf("-crm"), LdapNames.CENTRAL_OFFICE, "Employee", "IogMember",
        )

    val restConnection =
        UnirestInstance(
            Config().setObjectMapper(
                JacksonObjectMapper(
                    jacksonObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                        .registerModule(JavaTimeModule()),
                ),
            )
                .addDefaultHeader("Accept", "application/json")
                .connectTimeout(Duration.ofSeconds(60).toMillis().toInt()),
        )

    internal var scenario: TestScenario? = null

    internal fun startIogPlugin(
        admin: Boolean,
        backgroundJobManager: BackgroundJobManager,
        civiCRM: Boolean = false,
    ): TestScenario {
        initTestSetup()

        val app = AppBuilder().createApp("test-utilities", "test-utilities", "http://127.0.0.1", emptySet(), 0)
        val actionController = ActionController(db, ServiceConfig.ACCOUNT_CONTROLLER)
        val auth =
            AuthorizerApi(
                app,
                AuthorizationController(
                    mutableSetOf(
                        CredentialAuthorizer(
                            "credential",
                            credentialProvider,
                            "http://127.0.0.1",
                        ),
                    ),
                    ServiceConfig.ACCOUNT_CONTROLLER,
                    db,
                    actionController,
                ),
                true,
            )

        val grouppSyncMock = GroupSyncMock()
        val iogConfig =
            IogConfig(
                db,
                SupportMembershipFeature.SUPPORT_MEMBERSHIP_ENABLED,
                credentialProvider,
                OPNameConfig(mutableMapOf()),
                backgroundJobManager,
                nextcloudSettings._publicUrl,
                pool,
                auth as ISessionOperations,
                grouppSyncMock,
                IogPluginConstants.SUPPORT_MEMBERSHIP_HASH_ALGORITHM,
            )
        iogPlugin = IogPlugin(iogConfig, actionController, ServiceConfig.ACCOUNT_CONTROLLER, emptyList(), emptyList())
        val services = mutableListOf<IControllableService>()
        val n = NextcloudService(app, pool, nextcloudSettings, credentialProvider)
        services.add(n)
        val nextcloudObject = n.nextcloud
        val ncUtilities = NCUtilities(n.nextcloud)

        val openProject =
            OpenProjectService(
                backgroundJobManager,
                grouppSyncMock,
                OpenProjectTestVariables.settings,
                credentialProvider,
                3,
            )
        services.add(openProject)

        var civiService: CiviCRMService? = null
        var civiUtilities: CiviUtilities? = null
        if (civiCRM) {
            civiService = CiviCRMService(credentialProvider, civiSetting, backgroundJobManager)
            services.add(civiService)
            civiUtilities = CiviUtilities(civiService.civicrm, credentialProvider)
        }

        iogPlugin!!.startApis(app, services)
        val opUtilities = OPUtilities(openProject.openproject, backgroundJobManager, grouppSyncMock)
        scenario =
            TestScenario(
                "$BASE_BATH_PREFIX:${app.port()}$BASE_PATH_SUFFIX",
                app,
                services,
                nextcloudObject,
                n,
                openProject.openproject,
                openProject,
                iogPlugin,
                opUtilities,
                ncUtilities,
                grouppSyncMock,
                civiService,
                civiUtilities,
            )
        if (admin) {
            loginWithAdminUser()
        } else {
            loginWithStandardUser()
        }

        scenario!!.ncUtilities.reset()
        scenario!!.opUtilities.reset()
        scenario!!.civiTestUtilities?.reset()
        return scenario!!
    }

    fun loginWithUser(
        login: String,
        password: String,
    ) {
        val session =
            restConnection
                .post("${scenario!!.basePath}/authorizer/credential")
                .body(LoginRequest(login, password))
                .asObject(SessionAnswer::class.java)

        assertNotNull(session.body)
        assertEquals(login, session.body.user.username)
    }

    fun loginWithStandardUser() {
        loginWithUser(TestUser.NORMAL_USER.username, TestUser.NORMAL_USER_PASSWORD)
    }

    fun loginWithAdminUser() {
        loginWithUser(TestUser.ADMIN_USER.username, TestUser.ADMIN_USER_PASSWORD)
    }

    private fun initTestSetup() {
        val p = credentialProvider.getUsersAndGroups()
        var recreate = false
        if (p.first.size != 2 ||
            !p.first.any {
                it.username == TestUser.NORMAL_USER.username
            } ||
            !p.first.any { it.username == TestUser.ADMIN_USER.username }
        ) {
            recreate = true
        } else {
            val standard = p.first.find { it.username == TestUser.NORMAL_USER.username }!!
            val admin = p.first.find { it.username == TestUser.ADMIN_USER.username }!!
            if (!p.first.contains(
                    TestUser.NORMAL_USER.copy(
                        id = standard.id,
                        registeredSince = standard.registeredSince,
                    ),
                ) ||
                !p.first.contains(TestUser.ADMIN_USER.copy(id = admin.id, registeredSince = admin.registeredSince))
            ) {
                recreate = true
            }
        }
        if (recreate) {
            credentialProvider.getUsers().forEach { if (it.username != "sso_admin") credentialProvider.deleteUser(it) }
            credentialProvider.createUser(TestUser.NORMAL_USER, TestUser.NORMAL_USER_PASSWORD)
            credentialProvider.createUser(TestUser.ADMIN_USER, TestUser.ADMIN_USER_PASSWORD)
        }

        credentialProvider.getGroups().forEach { credentialProvider.deleteGroup(it) }
    }

    fun createStandardGroups() {
        val schema = scenario!!.iogPlugin!!.iogGroupSchema
        val url = nextcloudSettings._publicUrl

        val t =
            listOf(
                Triple(LdapNames.IOG_MEMBERS, GroupKind.LDAP_ONLY, "Alle Fördermitglieder von IOG"),
                Triple(LdapNames.INTERESTED_PEOPLE, GroupKind.LDAP_ONLY, "Interessierte Nichtfördermitglieder"),
                Triple(LdapNames.TRIAL_MEMBERS, GroupKind.LDAP_ONLY, "Nichtfördermitglieder mit erhöhten Rechten"),
                Triple(LdapNames.CRM_MEMBERS, GroupKind.LDAP_ONLY, "Alle Fördermitglieder mit Zugriff auf das CRM-System"),
                Triple(LdapNames.CENTRAL_OFFICE, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET, "Geschäftsstelle"),
                Triple(LdapNames.OMS, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET, "Ordentliche Mitglieder"),
                Triple(LdapNames.ASSOCIATION_BOARD, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET, "Vorstand"),
                Triple(LdapNames.PROKO, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC, "PROKO"),
                Triple(LdapNames.AP_SPRECHERINNEN, GroupKind.COLLABORATION, "AP SprecherInnen"),
                Triple(LdapNames.ETHIC_VALIDATION_GROUP, GroupKind.LDAP_ONLY, "Ethikprüfer"),
            )

        scenario!!.let { scenario ->
            runBlocking {
                t.asFlow().collect {
                    val groupName = it.first
                    val groups =
                        when (schema.identifyNodePatternType(groupName).first) {
                            IogGroupSchema.PatternTypes.SimpleMemberAdmin ->
                                schema.simpleAdminPattern.create(
                                    CreateGroupConfig(
                                        groupName,
                                        it.third,
                                        emptySet(),
                                        it.second,
                                        url,
                                    ),
                                )

                            IogGroupSchema.PatternTypes.MemberCrmInterested ->
                                schema.memberCrmPattern.create(
                                    CreateGroupConfig(
                                        groupName,
                                        it.third,
                                        emptySet(),
                                        it.second,
                                        url,
                                    ),
                                )

                            IogGroupSchema.PatternTypes.MemberAdminInterested ->
                                schema.memberAdminInterestedPattern.create(
                                    CreateGroupConfig(
                                        groupName,
                                        it.third,
                                        emptySet(),
                                        it.second,
                                        url,
                                    ),
                                )

                            IogGroupSchema.PatternTypes.MemberAdminTrial ->
                                schema.memberAdminTrialPattern.create(
                                    CreateGroupConfig(
                                        groupName,
                                        it.third,
                                        emptySet(),
                                        it.second,
                                        url,
                                    ),
                                )

                            else ->
                                listOf(
                                    credentialProvider.createGroup(
                                        Group(
                                            0,
                                            groupName,
                                            it.third,
                                            setOf(),
                                            setOf(),
                                            setOf(),
                                            setOf(),
                                            setOf(),
                                            setOf(),
                                        ),
                                    ),
                                )
                        }

                    scenario.opUtilities.createGroups(groups)
                    scenario.ncUtilities.addGroupToGroupFolder(groupName)
                }
            }
            scenario.ncUtilities.createStandardFolders()
            scenario.civiTestUtilities?.createStandardGroups()
        }
    }

    internal fun createRGTest() {
        createRG("rg-test", "RG Test")
    }

    internal fun removeRGTest() {
        removeMemberCrmInterested("rg-test", "RG Test", "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}")
    }

    internal fun removeKG(
        name: String,
        description: String,
    ) {
        removePattern(
            name,
            description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.KG}",
            MemberAdminTrialPattern(credentialProvider),
        )
    }

    internal fun createRG(
        name: String,
        description: String,
    ) {
        createMemberCrmInterested(
            name,
            description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
            OPPName.REGIONAL_GROUP,
            GroupKind.REGIONAL_GROUP,
        )
    }

    internal fun createProject(
        name: String,
        description: String,
    ) {
        createMemberAdminInterested(
            name,
            description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}",
            OPPName.PROJECTS,
            GroupKind.PROJECT,
        )
    }

    internal fun createKG(
        name: String,
        description: String,
    ) {
        createMemberAdminTrial(
            name,
            description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.KG}",
            OPPName.COMPETENCE_GROUP,
            GroupKind.COMPETENCE_GROUP,
        )
    }

    private fun createMemberAdminInterested(
        name: String,
        description: String,
        path: String,
        parentProjectName: String,
        groupKind: GroupKind,
    ) {
        removeMemberAdminInterested(name, description, path)
        val pat = MemberAdminInterestedPattern(credentialProvider)
        val conf = CreateGroupConfig(name, description, emptySet(), groupKind, nextcloudSettings._publicUrl)
        val groupList = pat.create(conf)
        scenario!!.let {
                scenario ->
            scenario.opUtilities.createProject(name, description, groupList, parentProjectName, groupKind)
            scenario.ncUtilities.createFolderStructureIfNecessary(path, description)
            scenario.ncUtilities.addGroupToGroupFolder("$name${IogPluginConstants.INTERESTED_SUFFIX}")
            scenario.ncUtilities.addGroupToGroupFolder("$name${IogPluginConstants.MEMBER_SUFFIX}")
            scenario.ncUtilities.addGroupToGroupFolder("$name${IogPluginConstants.TRIAL_SUFFIX}")
        }
    }

    private fun createMemberCrmInterested(
        name: String,
        description: String,
        path: String,
        parentProjectName: String,
        groupKind: GroupKind,
    ) {
        removeMemberCrmInterested(name, description, path)
        val pat = MemberCrmInterestedPattern(credentialProvider)
        val conf = CreateGroupConfig(name, description, emptySet(), groupKind, nextcloudSettings._publicUrl)
        val groupList = pat.create(conf)
        scenario!!.let {
                scenario ->
            scenario.opUtilities.createProject(name, description, groupList, parentProjectName, groupKind)
            scenario.ncUtilities.createFolderStructureIfNecessary(path, description)
            scenario.ncUtilities.addGroupToGroupFolder("$name${IogPluginConstants.INTERESTED_SUFFIX}")
            scenario.ncUtilities.addGroupToGroupFolder("$name${IogPluginConstants.MEMBER_SUFFIX}")
            scenario.ncUtilities.addGroupToGroupFolder("$name${IogPluginConstants.TRIAL_SUFFIX}")
            scenario.civiTestUtilities?.createChapterWithRolesAndSubgroups("$name${IogPluginConstants.CRM_SUFFIX}")
        }
    }

    private fun createMemberAdminTrial(
        name: String,
        description: String,
        path: String,
        parentProjectName: String,
        groupKind: GroupKind,
    ) {
        removeMemberAdminTrial(name, description, path)
        val pat = MemberAdminTrialPattern(credentialProvider)
        val conf = CreateGroupConfig(name, description, emptySet(), groupKind, nextcloudSettings._publicUrl)
        val groupList = pat.create(conf)
        scenario!!.let {
                scenario ->
            scenario.opUtilities.createProject(name, description, groupList, parentProjectName, groupKind)
            scenario.ncUtilities.createFolderStructureIfNecessary(path, description)
            scenario.ncUtilities.addGroupToGroupFolder("$name${IogPluginConstants.MEMBER_SUFFIX}")
            scenario.ncUtilities.addGroupToGroupFolder("$name${IogPluginConstants.TRIAL_SUFFIX}")
        }
    }

    private fun removePattern(
        id: String,
        description: String,
        path: String,
        pat: IGroupPattern,
    ) {
        pat.delete(id)
        val groups = pat.getAllMetaDescriptions(id, description).map { it.name }
        scenario!!.let { scenario ->
            scenario.opUtilities.removeProject(id, groups)
            scenario.ncUtilities.removeFolderIfExists(path, description)
            if (pat is MemberCrmInterestedPattern) {
                scenario.civiTestUtilities?.removeChapter(id + IogPluginConstants.CRM_SUFFIX)
            }
        }
    }

    fun removeMemberAdminInterested(
        id: String,
        description: String,
        path: String,
    ) {
        removePattern(id, description, path, MemberAdminInterestedPattern(credentialProvider))
    }

    fun removeMemberCrmInterested(
        id: String,
        description: String,
        path: String,
    ) {
        removePattern(id, description, path, MemberCrmInterestedPattern(credentialProvider))
    }

    fun removeSimpleAdmin(
        id: String,
        description: String,
        path: String,
    ) {
        removePattern(id, description, path, SimpleMemberAdminPattern(credentialProvider))
    }

    private fun removeMemberAdminTrial(
        id: String,
        description: String,
        path: String,
    ) {
        removePattern(id, description, path, MemberAdminTrialPattern(credentialProvider))
    }

    fun removeKgTrialGroups() {
        val trialGroups =
            credentialProvider.getGroups().filter {
                it.name.startsWith(IogPluginConstants.KG_PREFIX) && it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX)
            }
        trialGroups.forEach {
                trial ->
            val mainGroupName = trial.name.removeSuffix(IogPluginConstants.TRIAL_SUFFIX)
            val memberGroupName = mainGroupName.plus(IogPluginConstants.MEMBER_SUFFIX)
            val adminGroupName = mainGroupName.plus(IogPluginConstants.ADMIN_SUFFIX)
            credentialProvider.getGroupByName(adminGroupName)?.let { adminGroup ->
                credentialProvider.getGroupByName(memberGroupName)?.let { memberGroup ->
                    credentialProvider.getGroupByName(mainGroupName)?.let { oldParent ->
                        credentialProvider.updateGroup(
                            oldParent.copy(
                                groupMembers = emptySet(),
                                userMembers = emptySet(),
                                ownedGroups = emptySet(),
                                ownerGroups = emptySet(),
                                parentGroups = emptySet(),
                                owners = emptySet(),
                            ),
                        )
                        credentialProvider.deleteGroup(oldParent)
                    }
                    credentialProvider.getGroup(memberGroup.id)?.let {
                            updatedMemberGroup ->
                        credentialProvider.updateGroup(
                            updatedMemberGroup.copy(
                                name =
                                mainGroupName,
                                groupMembers = setOf(adminGroup.id),
                            ),
                        )
                    }
                }
            }

            credentialProvider.deleteGroup(trial)
            runBlocking {
                scenario!!.openProject?.getGroupByName(trial.name)?.let { opGroup ->
                    scenario!!.openProject!!.deleteGroup(opGroup)
                }
                scenario!!.openProject?.getGroupByName(memberGroupName)?.let { opGroup ->
                    scenario!!.openProject!!.deleteGroup(opGroup)
                }
            }
        }
    }

    fun startMultiservice(backgroundJobManager: BackgroundJobManager) {
        val mailSettings =
            MultiServiceMailSettings(
                "",
                SMTPSetting("", "443", "", "", "", ""),
                jakarta.mail.internet.InternetAddress(),
                SendMailContent("", ""),
                SendMailContent("", ""),
                ChangeMailSetting("", "", "", ""),
                "",
            )
        val userAdded: List<UserAddedEvent> = if (scenario?.civiCRM == null) emptyList() else listOf(scenario?.civiCRM!!)
        val userRemoved: List<UserRemovedFromGroupEvent> =
            if (scenario?.civiCRM == null) {
                iogPlugin!!.userRemovedFromGroupEvent
            } else {
                iogPlugin!!.userRemovedFromGroupEvent.plus(
                    scenario?.civiCRM!!,
                )
            }
        val events =
            MultiServiceEvents(
                iogPlugin!!.synchronizeEvent,
                iogPlugin!!.userDeletionEvents,
                iogPlugin!!.userCreationEvents,
                SessionTerminatorMock(),
                iogPlugin!!.checkUserEvent,
                listOfNotNull(iogPlugin!!.userMayAddCheck),
                userAdded,
                userRemoved,
            )

        val actionController = ActionController(db, ServiceConfig.ACCOUNT_CONTROLLER)
        val gdprController = GdprController(db, actionController, ServiceConfig.ACCOUNT_CONTROLLER)
        val m =
            Multiservice(
                MultiServiceConfig(ServiceConfig.ACCOUNT_CONTROLLER, backgroundJobManager, db, mailSettings, events, gdprController),
                ServiceConfig.ACCOUNT_CONTROLLER,
            )
        m.startApis(scenario!!.app)
    }
}
