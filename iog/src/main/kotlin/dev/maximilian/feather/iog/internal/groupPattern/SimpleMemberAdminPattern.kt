/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.groupPattern

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants

internal class SimpleMemberAdminPattern(
    var credentialProvider: ICredentialProvider,
) : IGroupPattern {
    private val adminDescriptionPrefix = "Administratoren von "

    override fun create(config: CreateGroupConfig): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val group =
            credentialProvider.createGroup(
                Group(
                    id = 0,
                    name = config.ldapName,
                    description = config.description,
                    parentGroups = setOf(iogMembers.id),
                    userMembers = emptySet(),
                    groupMembers = emptySet(),
                    owners = emptySet(),
                    ownerGroups = emptySet(), // filled later
                    ownedGroups = emptySet(),
                ),
            )

        val adminGroup =
            credentialProvider.createGroup(
                Group(
                    id = 0,
                    name = config.ldapName + IogPluginConstants.ADMIN_SUFFIX,
                    description = adminDescriptionPrefix + config.description,
                    parentGroups = setOf(group.id),
                    userMembers = config.ownerIDs,
                    owners = emptySet(),
                    groupMembers = emptySet(),
                    ownerGroups = emptySet(),
                    ownedGroups = setOf(group.id),
                ),
            )
        CreateGroup.logger.info { "SimpleMemberAdminPattern::create Created LDAP group ${group.name} with admin group ${adminGroup.name}" }

        return arrayOf(group, adminGroup).toList()
    }

    override fun search(prefixedName: String): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val group = credentialProvider.getGroupByName(prefixedName)
        requireNotNull(group) { "Cannot find group '$prefixedName'" }

        val adminName = prefixedName + IogPluginConstants.ADMIN_SUFFIX
        val adminGroup = credentialProvider.getGroupByName(prefixedName + IogPluginConstants.ADMIN_SUFFIX)

        requireNotNull(adminGroup) { "Cannot find group '$adminName'" }
        CreateGroup.logger.info { "SimpleMemberAdminPattern::search Found existing LDAP group ${group.name} with admin group ${adminGroup.name}" }

        return arrayOf(group, adminGroup).toList()
    }

    override fun getMetaDescription(
        groupName: String,
        description: String,
    ): GroupMetaDescription {
        if (groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.ADMIN_SUFFIX.length)
            return GroupMetaDescription(
                name = prefixedName + IogPluginConstants.ADMIN_SUFFIX,
                description = adminDescriptionPrefix + description,
                parentGroupNames = setOf(prefixedName),
                ownerNames = emptySet(),
                groupMemberNames = emptySet(),
                ownerGroupNames = setOf(),
                ownedGroupNames = setOf(prefixedName),
                descriptionWithoutPrefix = "",
            )
        } else {
            return GroupMetaDescription(
                name = groupName,
                description = description,
                parentGroupNames = setOf(LdapNames.IOG_MEMBERS),
                ownerNames = emptySet(),
                groupMemberNames = setOf(groupName + IogPluginConstants.ADMIN_SUFFIX),
                ownerGroupNames = setOf(groupName + IogPluginConstants.ADMIN_SUFFIX),
                ownedGroupNames = setOf(),
                descriptionWithoutPrefix = "",
            )
        }
    }

    private fun removeSuffixIfNecessary(groupName: String): String {
        var prefixedName = groupName
        if (groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.ADMIN_SUFFIX.length)
        }
        return prefixedName
    }

    override fun getAllMetaDescriptions(
        groupName: String,
        description: String,
    ): List<GroupMetaDescription> {
        val prefixedName = removeSuffixIfNecessary(groupName)
        return listOf(
            getMetaDescription(prefixedName, description),
            getMetaDescription(prefixedName + IogPluginConstants.ADMIN_SUFFIX, description),
        )
    }

    override fun isAdminGroup(groupName: String): Boolean {
        return groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX)
    }

    override fun userMembersAllowed(groupName: String): Boolean {
        return true
    }

    override fun delete(groupName: String) {
        credentialProvider.getGroupByName(groupName + IogPluginConstants.ADMIN_SUFFIX)?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(groupName)?.let { credentialProvider.deleteGroup(it) }
    }

    override fun isMainGroup(groupName: String): Boolean {
        return groupName == removeSuffixIfNecessary(groupName)
    }
}
