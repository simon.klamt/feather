/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.group.plausibility.OpenProjectPlausibility
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.tools.NextcloudFolderCreator
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.internals.plausibility.BackgroundJobStatusHelper
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.openproject.IOpenProject
import mu.KotlinLogging
import java.util.UUID

private data class SubChapterDefinition(
    val opSuffix: String,
    val groupkind: GroupKind,
    val nextcloudParentFolder: String,
    val nextcloudSuffix: String,
    val opParentProject: String,
)

internal class CreateChapterDB(
    private val backgroundJobManager: BackgroundJobManager,
    private val credentials: ICredentialProvider,
    private val chapterDB: ChapterDB,
    private val iogSchema: IogGroupSchema,
    private val onc: OPNameConfig,
    private val openproject: IOpenProject,
) {
    private val logger = KotlinLogging.logger {}

    suspend fun createChapterDB(
        jobId: UUID,
        simulate: Boolean,
    ): Boolean {
        backgroundJobManager.setJobStatus(jobId, "Fetching groups")
        val userAndGroups = credentials.getUsersAndGroups()

        val opc = OpenProjectPCreator(openproject, onc, jobId, backgroundJobManager)
        val opPlausibility = OpenProjectPlausibility(openproject, credentials, opc, onc, userAndGroups.first)

        val statusHelper =
            BackgroundJobStatusHelper(
                jobId,
                "Processing existing groups",
                userAndGroups.second.size,
                backgroundJobManager,
            )

        backgroundJobManager.setJobStatus(jobId, "Delete chapter database")
        if (!simulate) {
            chapterDB.deleteAll()
        }

        var currentEntry = 0
        userAndGroups.second.forEach { group ->
            logger.debug { "FillChapterDB::fillChapterDB check Group ${group.name}" }

            var prefixedName = group.name
            IogGroupSchema.possibleSubGroupSuffixes.forEach { prefixedName = prefixedName.substringBefore(it) }
            val opDescription = opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(prefixedName))

            val patternType = iogSchema.identifyNodePatternType(group.name)
            val pattern =
                when (patternType.first) {
                    IogGroupSchema.PatternTypes.MemberAdminInterested -> iogSchema.memberAdminInterestedPattern
                    IogGroupSchema.PatternTypes.MemberCrmInterested -> iogSchema.memberCrmPattern
                    IogGroupSchema.PatternTypes.SimpleMemberAdmin -> iogSchema.simpleAdminPattern
                    IogGroupSchema.PatternTypes.SimpleMemberAdminCRM -> iogSchema.simpleAdminPatternCRM
                    else -> null
                }
            if ((
                    patternType.first == IogGroupSchema.PatternTypes.SimpleMemberAdmin ||
                        patternType.first == IogGroupSchema.PatternTypes.MemberCrmInterested ||
                        patternType.first == IogGroupSchema.PatternTypes.MemberAdminInterested
                    ) &&
                pattern?.isMainGroup(group.name) == true && !LdapNames.opIgnoreList.contains(group.name)
            ) {
                val description =
                    when (group.name) {
                        LdapNames.OMS -> NextcloudFolders.OMS
                        LdapNames.CENTRAL_OFFICE -> NextcloudFolders.CentralOffice
                        LdapNames.ASSOCIATION_BOARD -> NextcloudFolders.AssociationBoard
                        else -> opDescription
                    }

                val path =
                    if (description == "PROKO") {
                        ""
                    } else {
                        NextcloudFolderCreator().getParentFolder(patternType.second, group.name, description)
                    }

                val parentProject =
                    opPlausibility.opProjects.find { it.identifier == group.name }?.parent?.value?.identifier

                val simulateString = if (simulate) "SIMULATION" else "STORE"
                val permission: Set<PermissionType> =
                    if (path == "") {
                        emptySet()
                    } else {
                        setOf(
                            PermissionType.Read,
                            PermissionType.Update,
                            PermissionType.Create,
                            PermissionType.Delete,
                        )
                    }

                logger.warn {
                    "FillChapterDB::fillChapterDB $simulateString groupName = ${group.name}, " +
                        "patternType = ${patternType.first}, " +
                        "groupKind = ${patternType.second}, " +
                        "nextcloudFolder = $path, " +
                        "nextcloudPermission = $permission, " +
                        "parentProject = ${parentProject ?: ""}, " +
                        "projectName = ${group.name}, " +
                        "opDescription = ${opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(group.name))}, " +
                        "archived = false"
                }
                if (!simulate) {
                    chapterDB.storeChapter(
                        UUID.randomUUID(),
                        group.name, patternType.first, patternType.second,
                        path, permission, parentProject ?: "", group.name,
                        opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(group.name)), false,
                    )
                }

                if (patternType.second == GroupKind.REGIONAL_GROUP) {
                    val subgroups =
                        listOf(
                            SubChapterDefinition(
                                IogPluginConstants.PR_FR_SUFFIX,
                                GroupKind.PR_FR_GROUP,
                                NextcloudFolders.PrFr,
                                " (PR-FR)",
                                onc.prFrGroupParentProject,
                            ),
                            SubChapterDefinition(
                                IogPluginConstants.BILA_SUFFIX,
                                GroupKind.BILA_GROUP,
                                NextcloudFolders.BiLa,
                                " (BiLa)",
                                onc.bilaGroupParentProject,
                            ),
                        )
                    subgroups.forEach { s ->

                        val pt = patternType.first
                        val p = path.replace(NextcloudFolders.RgIntern, s.nextcloudParentFolder)
                        val pname = group.name + s.opSuffix
                        val d =
                            opPlausibility.getDescription(
                                onc.credentialGroupToOpenProjectGroup(
                                    group.name,
                                ) + s.nextcloudSuffix,
                            )
                        if (openproject.getProjectByIdentifierOrName(group.name + s.opSuffix) != null) {
                            logger.warn {
                                "FillChapterDB::fillChapterDB $simulateString groupName = ${group.name}, " +
                                    "patternType = $pt, " +
                                    "groupKind = ${s.groupkind}, " +
                                    "nextcloudFolder = $p, " +
                                    "nextcloudPermission = $permission, " +
                                    "parentProject = ${s.opParentProject}, " +
                                    "projectName = $pname, " +
                                    "opDescription = ${
                                        d
                                    }, " +
                                    "archived = false"
                            }
                            if (!simulate) {
                                chapterDB.storeChapter(
                                    UUID.randomUUID(),
                                    group.name,
                                    patternType.first,
                                    patternType.second,
                                    p,
                                    permission,
                                    s.opParentProject,
                                    pname,
                                    d,
                                    false,
                                )
                            }
                        }
                    }
                }
            }

            logger.warn { "FillChapterDB::fillChapterDB ${group.name} completed." }
            currentEntry += 1
            statusHelper.updateStatus(currentEntry)
        }
        return true
    }
}
