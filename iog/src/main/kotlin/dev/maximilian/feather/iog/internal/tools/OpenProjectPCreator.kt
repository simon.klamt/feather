/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.tools

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.MembershipAssociation
import dev.maximilian.feather.iog.internal.group.MembershipAssociationArguments
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogServiceApiConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectDescription
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.openproject.OpenProjectMembership
import dev.maximilian.feather.openproject.OpenProjectProject
import dev.maximilian.feather.openproject.OpenProjectRole
import mu.KotlinLogging
import java.util.UUID

internal class OpenProjectPCreator(
    private val openproject: IOpenProject,
    private val onc: OPNameConfig,
    private val jobId: UUID?,
    private val backgroundJobManager: BackgroundJobManager,
) {
    private val logger = KotlinLogging.logger { }

    internal val parentProjectMap =
        mapOf(
            GroupKind.REGIONAL_GROUP to onc.regionalGroupParentProject,
            GroupKind.BILA_GROUP to onc.bilaGroupParentProject,
            GroupKind.PR_FR_GROUP to onc.prFrGroupParentProject,
            GroupKind.COMPETENCE_GROUP to onc.competenceGroupParentProject,
            GroupKind.COMMITTEE to onc.committeeParentProject,
            GroupKind.COLLABORATION to onc.collaborationParentProject,
            GroupKind.PROJECT to onc.projectParentProject,
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET to onc.otherParentProject,
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC to onc.otherParentProject,
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED to onc.otherParentProject,
        )

    private val projectTemplateMap =
        mapOf(
            GroupKind.REGIONAL_GROUP to onc.regionalGroupTemplate,
            GroupKind.BILA_GROUP to onc.biLaGroupTemplate,
            GroupKind.PR_FR_GROUP to onc.prFrGroupTemplate,
            GroupKind.COMPETENCE_GROUP to onc.competenceGroupTemplate,
            GroupKind.COLLABORATION to onc.collaborationTemplate,
            GroupKind.COMMITTEE to onc.committeeTemplate,
            GroupKind.PROJECT to onc.projectTemplate,
        )

    private suspend fun getOrCreateGroups(createdLdapGroups: List<String>): List<OpenProjectGroup> {
        try {
            val newOpenProjectGroups =
                createdLdapGroups.map {
                    openproject.getGroupByName(it) ?: openproject.createGroup(
                        OpenProjectGroup(
                            0,
                            it,
                            emptyList(),
                        ),
                    )
                }
            if (newOpenProjectGroups.size != createdLdapGroups.size) {
                logger.error("OpenProjectPCreator::_getOrCreateGroups Could not create openproject groups for all LDAP groups.")
                throw RuntimeException("Failed to create group in OpenProject")
            }
            return newOpenProjectGroups
        } catch (e: Exception) {
            logger.error(e) { "OpenProjectPCreator::_getOrCreateGroups Group creation in OpenProject not successfull." }
            throw RuntimeException("Failed to create group in OpenProject")
        }
    }

    suspend fun createGroups(
        createdLdapGroups: List<Group>,
        synchronizer: GroupSynchronizationEvent,
    ): List<OpenProjectGroup> {
        // Create groups in OpenProject
        jobId?.let { backgroundJobManager.setJobStatus(it, "Creating group(s) in OpenProject") }
        val newOpenProjectGroups =
            getOrCreateGroups(createdLdapGroups.map { onc.credentialGroupToOpenProjectGroup(it.name) })

        // Create group synchronisations in OpenProject
        jobId?.let { backgroundJobManager.setJobStatus(it, "Creating group synchronisation(s) in OpenProject") }
        synchronizer.createOPSync(openproject, createdLdapGroups, newOpenProjectGroups)

        jobId?.let { backgroundJobManager.setJobStatus(it, "Synchronising groups in OpenProject") }

        kotlin.runCatching { synchronizer.synchronize() }
            .onFailure {
                CreateGroup.logger.error(
                    it,
                ) { "OpenProjectPCreator::createGroupInOpenProject User group synchronisation in OpenProject not successfull." }
            }.onSuccess {
                CreateGroup.logger.info(
                    "OpenProjectPCreator::createGroupInOpenProject User group synchronisation in OpenProject successfull.",
                    it,
                )
            }
        return newOpenProjectGroups
    }

    suspend fun searchGroups(createdLdapGroups: List<Group>): List<OpenProjectGroup> {
        // search groups in OpenProject
        jobId?.let { backgroundJobManager.setJobStatus(it, "Searching group(s) in OpenProject") }
        try {
            val newOpenProjectGroups = createdLdapGroups.mapNotNull { openproject.getGroupByName(it.name) }
            if (newOpenProjectGroups.size != createdLdapGroups.size) {
                CreateGroup.logger.error("OpenProjectPCreator::searchGroups Could not find openproject groups for all LDAP groups.")
                throw RuntimeException("Failed to search group in OpenProject")
            }
            return newOpenProjectGroups
        } catch (e: Exception) {
            CreateGroup.logger.error(e) { "OpenProjectPCreator::searchGroups Group search in OpenProject not successfull." }
            throw RuntimeException("Failed to search group in OpenProject")
        }
    }

    private suspend fun copyProjectFromTemplate(
        projectTemplate: String,
        groupName: String,
    ): OpenProjectProject {
        try {
            val templateProject = openproject.getProjectByIdentifierOrName(projectTemplate)

            requireNotNull(templateProject) { "Template project $projectTemplate" }

            return openproject.cloneProject(
                templateProject,
                groupName,
                null, // copy description from template
                onc.stuffToBeClonedFromTemplate,
            )
        } catch (e: Exception) {
            CreateGroup.logger.error(e) {
                "OpenProjectPCreator::copyProjectAndAssignRoles Cloning template project $projectTemplate in OpenProject not successfull."
            }
            throw RuntimeException("Failed to clone template project in OpenProject")
        }
    }

    private suspend fun createProject(
        groupKind: GroupKind,
        groupName: String,
        groupIdentifier: String,
    ): OpenProjectProject {
        try {
            var parentProject: OpenProjectProject? = null
            val parentProjectIdentifier = parentProjectMap[groupKind]

            if (parentProjectIdentifier != null && parentProjectIdentifier != "") {
                parentProject = openproject.getProjectByIdentifierOrName(parentProjectIdentifier)
            }

            val newProject =
                openproject.createProject(
                    OpenProjectProject(
                        id = 0,
                        name = groupName,
                        identifier = groupIdentifier,
                        description =
                        OpenProjectDescription(
                            format = "",
                            raw = "",
                            html = "",
                        ),
                        parentProject,
                    ),
                )
            return newProject
        } catch (e: Exception) {
            CreateGroup.logger.error("OpenProjectPCreator::_createProject for $groupName with identifier $groupIdentifier fired ex " + e) {
                "OpenProjectPCreator::copyProjectAndAssignRoles Project creation in OpenProject not successfull."
            }
            throw RuntimeException("Failed to create project in OpenProject")
        }
    }

    suspend fun copyProjectAndAssignRoles(
        description: String?,
        groupKind: GroupKind,
        createdLdapGroupSize: Int,
        groupIdentifier: String,
        newOpenProjectGroups: List<OpenProjectGroup>,
    ): OpenProjectProject {
        val groupName = description ?: groupIdentifier
        jobId?.let { backgroundJobManager.setJobStatus(it, "Checking existence in OpenProject") }
        try {
            val existingProject = openproject.getProjectByIdentifierOrName(groupName)

            if (null != existingProject) {
                // we found a project with the intended name
                CreateGroup.logger.warn {
                    "OpenProjectPCreator::copyProjectAndAssignRoles Project ${existingProject.name} already exists (ID: ${existingProject.id})."
                }
                throw IllegalStateException("Project $groupName already exists")
            }
        } catch (e: Exception) {
            // project does not exist yet -> move on
        }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Fetching roles in OpenProject") }
        /* some business logic according to groupKind,
         * see https://openproject.iog-aachen.de/projects/implementierung-der-dkp/wiki/openproject */
        val roles = openproject.getRoles()
        val rolesToBeAssigned = getRolesToBeAssigned(groupKind, createdLdapGroupSize, roles)

        if (rolesToBeAssigned.size != createdLdapGroupSize) {
            throw IllegalStateException(
                "Mismatch in number of created LDAP groups (${rolesToBeAssigned.size} vs. $createdLdapGroupSize) for $description of type $groupKind",
            )
        }

        val projectTemplate = projectTemplateMap[groupKind]
        val newProject =
            if (projectTemplate != null && projectTemplate != "") {
                jobId?.let { backgroundJobManager.setJobStatus(it, "Cloning template in OpenProject") }
                // Clone template groups in OpenProject
                copyProjectFromTemplate(projectTemplate, groupName)
            } else {
                // Create project in OpenProject
                jobId?.let { backgroundJobManager.setJobStatus(it, "Creating project in OpenProject") }
                createProject(groupKind, groupName, groupIdentifier)
            }

        // Register project membership with appropriate roles in OpenProject
        jobId?.let { backgroundJobManager.setJobStatus(it, "Registering memberships in OpenProject") }
        try {
            newOpenProjectGroups.zip(rolesToBeAssigned).forEach {
                if (it.second != null) {
                    openproject.createMembership(
                        OpenProjectMembership(
                            0,
                            newProject,
                            it.first,
                            listOf(it.second!!),
                        ),
                    )
                }
            }

            val additionalRolesToBeAssigned = getPublicRoles(groupKind, roles)

            additionalRolesToBeAssigned.forEach {
                openproject.createMembership(
                    OpenProjectMembership(
                        0,
                        newProject,
                        it.group,
                        listOf(it.role),
                    ),
                )
            }
        } catch (e: Exception) {
            CreateGroup.logger.error(e) {
                "OpenProjectPCreator::copyProjectAndAssignRoles Project membership registration in OpenProject not successfull for $groupName: ${e.message}"
            }
            throw RuntimeException("Failed to register project membership in OpenProject")
        }
        return newProject
    }

    internal suspend fun getPublicRoles(
        groupKind: GroupKind,
        roles: List<OpenProjectRole>,
    ): List<MembershipAssociation> {
        // add additional membership roles for predefined groups
        return when (groupKind) {
            GroupKind.PROJECT ->
                arrayOf(
                    MembershipAssociationArguments(
                        onc.interestedRoleName,
                        onc.interestedPeopleGroupName,
                    ),
                    MembershipAssociationArguments(
                        onc.readerRoleName,
                        onc.iogMembersGroupName,
                    ),
                    MembershipAssociationArguments(
                        onc.memberRoleName,
                        onc.centralOfficeGroupName,
                    ),
                    MembershipAssociationArguments(
                        onc.projectAdminRoleName,
                        onc.projectAdminsGroupName,
                    ),
                )
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_CRM_PUBLIC,
            GroupKind.COMPETENCE_GROUP, GroupKind.COMMITTEE, GroupKind.COLLABORATION,
            GroupKind.REGIONAL_GROUP, GroupKind.PR_FR_GROUP, GroupKind.BILA_GROUP,
            ->
                arrayOf(
                    MembershipAssociationArguments(
                        onc.interestedRoleName,
                        onc.interestedPeopleGroupName,
                    ),
                    MembershipAssociationArguments(
                        onc.readerRoleName,
                        onc.iogMembersGroupName,
                    ),
                    MembershipAssociationArguments(
                        onc.memberRoleName,
                        onc.centralOfficeGroupName,
                    ),
                )
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET ->
                arrayOf(
                    // for special GS, OM, and board groups, more restrictions apply
                    MembershipAssociationArguments(
                        onc.interestedRoleName,
                        onc.interestedPeopleGroupName,
                    ),
                    MembershipAssociationArguments(
                        onc.interestedRoleName,
                        onc.iogMembersGroupName,
                    ),
                )
            GroupKind.LDAP_ONLY -> throw IllegalArgumentException("invalid group kind for openproject group")
        }.map { args ->
            val role = roles.find { it.name == args.roleName }
            val group = openproject.getGroupByName(args.groupName)
            if (role == null) {
                throw IllegalStateException("Could not find relevant openproject role (${args.roleName})")
            } else if (group == null) {
                throw IllegalStateException("Could not find relevant openproject group (${args.groupName})")
            }
            MembershipAssociation(role, group)
        }
    }

    internal fun getRolesToBeAssigned(
        groupKind: GroupKind,
        createdLdapGroupsSize: Int,
        roles: List<OpenProjectRole>,
    ): List<OpenProjectRole?> {
        return when (groupKind) {
            GroupKind.REGIONAL_GROUP ->
                arrayOf(
                    null,
                    onc.projectAdminRoleName,
                    onc.readerRoleName,
                    onc.memberRoleName,
                    onc.memberRoleName,
                    null,
                )
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED ->
                arrayOf(
                    null,
                    onc.projectAdminRoleName,
                    onc.readerRoleName,
                    onc.memberRoleName,
                    onc.memberRoleName,
                )
            GroupKind.PROJECT ->
                arrayOf(
                    null,
                    onc.nodeAdminRoleName,
                    onc.readerRoleName,
                    onc.memberRoleName,
                    onc.memberRoleName,
                )
            GroupKind.COMPETENCE_GROUP,
            GroupKind.COMMITTEE,
            ->
                arrayOf(
                    null,
                    onc.projectAdminRoleName,
                    onc.memberRoleName,
                    onc.memberRoleName,
                )
            GroupKind.COLLABORATION,
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC,
            ->
                arrayOf(
                    onc.memberRoleName,
                    onc.projectAdminRoleName,
                )
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_CRM_PUBLIC ->
                arrayOf(
                    onc.memberRoleName,
                    onc.projectAdminRoleName,
                    null,
                )
            GroupKind.PR_FR_GROUP,
            GroupKind.BILA_GROUP,
            ->
                if (createdLdapGroupsSize == IogServiceApiConstants.GROUPS_PER_MEMBER_CRM_INTERESTED_PATTERN) {
                    // BILA or PR/FR associated with a regional group
                    arrayOf(
                        null,
                        onc.projectAdminRoleName,
                        onc.readerRoleName,
                        onc.memberRoleName,
                        onc.memberRoleName,
                        null,
                    )
                } else if (createdLdapGroupsSize == IogServiceApiConstants.GROUPS_PER_MEMBER_ADMIN_INTERESTED_PATTERN) {
                    // BILA or PR/FR associated with a regional group
                    arrayOf(
                        null,
                        onc.projectAdminRoleName,
                        onc.readerRoleName,
                        onc.memberRoleName,
                        onc.memberRoleName,
                    )
                } else {
                    // standalone BILA orPR/FR group
                    arrayOf(
                        onc.memberRoleName,
                        onc.projectAdminRoleName,
                    )
                }
            GroupKind.LDAP_ONLY -> throw IllegalArgumentException("invalid group kind for openproject group")
        }.map { roleName ->
            if (roleName != null) {
                val result = roles.find { role -> role.name == roleName }
                requireNotNull(result) { "Could not find relevant openproject roles" }
                result
            } else {
                null
            }
        }
    }

    internal suspend fun reassignRole(
        group: Group,
        existingProject: OpenProjectProject,
        roleName: String,
        jobId: UUID?,
        synchronizer: GroupSynchronizationEvent,
    ) {
        jobId?.let { backgroundJobManager.setJobStatus(it, "Find desired role in OpenProject") }
        val roles = openproject.getRoles()
        val relevantRole =
            requireNotNull(roles.find { it.name == roleName }) {
                "Role $roleName does not exists"
            }
        val opName = onc.credentialGroupToOpenProjectGroup(group.name)
        jobId?.let {
            backgroundJobManager.setJobStatus(
                it,
                "Create group(s) for credential group ${group.name} as $opName in OpenProject if necessary",
            )
        }
        val opGroup = openproject.getGroupByName(opName) ?: createGroups(listOf(group), synchronizer).first()

        try {
            jobId?.let {
                backgroundJobManager.setJobStatus(
                    it,
                    "Create membership for ${group.name} in project ${existingProject.name}",
                )
            }
            openproject.createMembership(OpenProjectMembership(0, existingProject, opGroup, listOf(relevantRole)))
        } catch (e: Exception) {
            CreateGroup.logger.error(
                e,
            ) { "OpenProjectPCreator::reassignRole Project membership registration in OpenProject not successfull." }
            throw RuntimeException("Failed to register project membership in OpenProject", e)
        }
    }

    private suspend fun createGroupsAndSyncs(
        createdLdapGroups: List<Group>,
        synchronizer: GroupSynchronizationEvent,
    ): List<OpenProjectGroup> {
        // Create groups in OpenProject
        jobId?.let { backgroundJobManager.setJobStatus(it, "Creating group(s) in OpenProject") }
        val newOpenProjectGroups =
            getOrCreateGroups(createdLdapGroups.map { onc.credentialGroupToOpenProjectGroup(it.name) })

        // Create group synchronisations in OpenProject
        jobId?.let { backgroundJobManager.setJobStatus(it, "Creating group synchronisation(s) in OpenProject") }
        synchronizer.createOPSync(openproject, createdLdapGroups, newOpenProjectGroups)
        return newOpenProjectGroups
    }

    suspend fun addSingleGroup(
        singleGroup: Group,
        myProject: OpenProjectProject,
        memberRole: OpenProjectRole,
        synchronizer: GroupSynchronizationEvent,
    ) {
        val opTrialGroup = createGroupsAndSyncs(listOf(singleGroup), synchronizer).first()
        openproject.createMembership(OpenProjectMembership(0, myProject, opTrialGroup, listOf(memberRole)))
    }

    suspend fun getOrCreateGeneralGroup(
        generalGroupLdap: Group,
        synchronizer: GroupSynchronizationEvent,
    ): OpenProjectGroup {
        return createGroupsAndSyncs(listOf(generalGroupLdap), synchronizer).first()
    }
}
