/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.iog.api.bindings.ChapterDetails
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.UUID

class ChapterDB(val db: Database) {
    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(ChapterMapping)
        }
    }

    object ChapterMapping : Table() {
        val uid = varchar("uid", 50)
        val maingroupName = varchar("name", 30)
        val patterntype = varchar("patterntype", 30)
        val groupKind = varchar("groupKind", 50)
        val nextcloudFolder = varchar("nexcloudFolder", 80)
        val nextcloudPermissions = integer("nextcloudPermissions")
        val parentProject = varchar("parentProject", 80)
        val projectName = varchar("projectName", 80)
        val description = varchar("description", 80)
        val archived = bool("archived")
        override val primaryKey: PrimaryKey
            get() = PrimaryKey(uid)
    }

    internal fun storeChapter(
        uid: UUID,
        maingroupName: String,
        patterntype: IogGroupSchema.PatternTypes,
        groupKind: GroupKind,
        nextcloudFolder: String,
        nextcloudPermissions: Set<PermissionType>,
        parentProject: String,
        projectName: String,
        description: String,
        archived: Boolean,
    ) = transaction(db) {
        ChapterMapping.insert {
            it[ChapterMapping.uid] = uid.toString()
            it[ChapterMapping.maingroupName] = maingroupName
            it[ChapterMapping.patterntype] = patterntype.toString()
            it[ChapterMapping.groupKind] = groupKind.toString()
            it[ChapterMapping.nextcloudFolder] = nextcloudFolder
            it[ChapterMapping.nextcloudPermissions] = PermissionType.toIntValue(nextcloudPermissions)
            it[ChapterMapping.parentProject] = parentProject
            it[ChapterMapping.projectName] = projectName
            it[ChapterMapping.description] = description
            it[ChapterMapping.archived] = archived
        }
    }

    internal fun getChapterByUUID(uid: UUID): ChapterDetails? =
        transaction(db) {
            ChapterMapping.select(ChapterMapping.uid eq uid.toString()).singleOrNull()?.let { rowToChapterDetails(it) }
        }

    internal fun getChapterByName(name: String): ChapterDetails? =
        transaction(db) {
            ChapterMapping.select(ChapterMapping.maingroupName eq name).singleOrNull()?.let { rowToChapterDetails(it) }
        }

    fun deleteAll() =
        transaction(db) {
            ChapterMapping.deleteAll()
        }

    fun deleteByGroupname(groupName: String) =
        transaction(db) {
            ChapterMapping.deleteWhere { maingroupName eq groupName }
        }

    private fun rowToChapterDetails(it: ResultRow): ChapterDetails =
        ChapterDetails(
            uid = it[ChapterMapping.uid],
            maingroupName = it[ChapterMapping.maingroupName],
            patterntype = IogGroupSchema.PatternTypes.valueOf(it[ChapterMapping.patterntype]),
            groupKind = GroupKind.valueOf(it[ChapterMapping.groupKind]),
            nextcloudFolder = it[ChapterMapping.nextcloudFolder],
            nextcloudPermissions = PermissionType.fromIntValue(it[ChapterMapping.nextcloudPermissions]),
            parentProject = it[ChapterMapping.parentProject],
            description = it[ChapterMapping.description],
            archived = it[ChapterMapping.archived],
            projectName = it[ChapterMapping.projectName],
        )

    fun getAllChapterIDs() =
        transaction(db) {
            ChapterMapping.selectAll().orderBy(ChapterMapping.maingroupName).map { it[ChapterMapping.uid] }
        }

    fun getAllChapterNames() =
        transaction(db) {
            ChapterMapping.selectAll().orderBy(ChapterMapping.maingroupName).map { it[ChapterMapping.maingroupName] }
        }

    fun getID(name: String) =
        transaction(db) {
            ChapterMapping.select(ChapterMapping.maingroupName eq name).singleOrNull()?.get(ChapterMapping.uid)
        }
}
