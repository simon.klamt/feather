/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.civicrm.CiviCRMConstants
import dev.maximilian.feather.civicrm.CiviCRMService
import dev.maximilian.feather.civicrm.entities.ACL
import dev.maximilian.feather.civicrm.entities.ACLEntityRole
import dev.maximilian.feather.civicrm.entities.Group
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import dev.maximilian.feather.iog.internal.settings.CiviCRMNames
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.multiservice.BackgroundJobManager
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.util.UUID

internal class DeleteCrmGroups(
    private val credentials: ICredentialProvider,
    private val backgroundJobManager: BackgroundJobManager,
    private val civiCRMService: CiviCRMService?,
) {
    companion object : KLogging() {
        suspend fun deleteCivi(civicrm: ICiviCRM) {
            val civiGroups = civicrm.getGroups()
            val acls = civicrm.getACLs()
            val aclroles = civicrm.getACLEntityRoles()
            val aclRoleIndex = civicrm.getOptionGroup(CiviCRMConstants.ACL_ROLE)?.id

            civiGroups.firstOrNull { it.name == LdapNames.ETHIC_VALIDATION_GROUP }?.let {
                    ethicGroup ->
                acls.filter { it.entityId == ethicGroup.id }.forEach {
                    civicrm.deleteACL(it.id)
                }
                aclroles.filter {
                    it.entityId == ethicGroup.id
                }.forEach { r -> civicrm.deleteACLEntityRole(r.id) }
                civicrm.deleteGroup(ethicGroup.id)
            }

            acls.filter { it.name != "Edit All Contacts" && it.name != "Core ACL" }.forEach { civicrm.deleteACL(it.id) }
            deleteGroupAndDependencies(civicrm, IogPluginConstants.RG_PREFIX, civiGroups, acls, aclroles)
            deleteGroupAndDependencies(civicrm, IogPluginConstants.PROGRAM_PREFIX, civiGroups, acls, aclroles)
            deleteGroupAndDependencies(civicrm, CiviCRMNames.USER_GROUP_NAME, civiGroups, acls, aclroles)
            deleteGroupAndDependencies(civicrm, CiviCRMNames.CONTACT_GROUP_NAME, civiGroups, acls, aclroles)
            deleteGroupAndDependencies(civicrm, LdapNames.CENTRAL_OFFICE, civiGroups, acls, aclroles)
            civicrm.getGroups().filter { it.id > 1 }.forEach { civicrm.deleteGroup(it.id) }

            aclRoleIndex?.let {
                aclRoleIndex
                civicrm.getOptionValueByOptionGroupId(aclRoleIndex).filter {
                    it.name?.contains("Admin") == false && it.name?.contains("Auth") == false
                }.forEach {
                    civicrm.deleteOptionValue(it.id)
                }
            }
            civicrm.getEmails().filter { it.id > CiviCRMConstants.HIGHEST_ID_OF_DEFAULT_EMAILS }.forEach { civicrm.deleteEmail(it.id) }
            civicrm.getGroupContacts().forEach { civicrm.deleteGroupContact(it.id) }
            civicrm.getContacts().filter { it.id > CiviCRMConstants.HIGHEST_ID_OF_DEFAULT_CONTACTS }.forEach {
                civicrm.deleteContact(
                    it.id,
                )
            }
            civicrm.getBlockedContacts().filter { it.id > CiviCRMConstants.HIGHEST_ID_OF_DEFAULT_CONTACTS }.forEach {
                civicrm.deleteContact(
                    it.id,
                )
            }
            aclroles.forEach {
                civicrm.deleteACLEntityRole(it.id)
            }
            val contactTypes = civicrm.getContactTypes()
            contactTypes.forEach {
                if ((it.name != CiviCRMConstants.ContactTypeDefaults.Organization.protocolName) &&
                    (it.name != CiviCRMConstants.ContactTypeDefaults.Individual.protocolName) &&
                    (it.name != CiviCRMConstants.ContactTypeDefaults.Household.protocolName)
                ) {
                    civicrm.deleteContactType(it.id)
                }
            }
        }

        private suspend fun deleteGroupAndDependencies(
            civicrm: ICiviCRM,
            prefix: String,
            civiGroups: List<Group>,
            acls: List<ACL>,
            aclroles: List<ACLEntityRole>,
        ) {
            civiGroups.filter { it.name.startsWith(prefix) }.forEach { g ->
                acls.filter { it.entityId == g.id }.forEach { civicrm.deleteACL(it.id) }
                aclroles.filter { it.entityId == g.id }.forEach { civicrm.deleteACLEntityRole(it.id) }
                civicrm.deleteGroup(g.id)
            }
        }
    }

    fun delete(
        jobId: UUID,
        param: Boolean,
    ): CrmOperationResult {
        logger.info { "DeleteCrmGroups::delete remove all -crm groups " }
        backgroundJobManager.setJobStatus(jobId, "Remove all -crm groups")

        val crmGroups = credentials.getGroups().filter { it.name.endsWith(IogPluginConstants.CRM_SUFFIX) }
        crmGroups.forEach {
            credentials.deleteGroup(it)
        }
        runBlocking {
            civiCRMService?.let {
                logger.warn { "DeleteCrm::delete remove CrmMembers group from CiviCRM" }
                deleteCivi(civiCRMService.civicrm)
            }
            backgroundJobManager.setJobStatus(jobId, "Remove all CrmMembers group")
        }
        credentials.getGroupByName(LdapNames.CRM_MEMBERS)?.let { credentials.deleteGroup(it) }

        logger.warn { "DeleteCrm::delete finished." }
        backgroundJobManager.setJobStatus(jobId, "Finished.")
        return CrmOperationResult("OK", emptyList())
    }
}
