/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import dev.maximilian.feather.iog.internal.chapter.ChapterDB
import dev.maximilian.feather.iog.internal.chapter.CiviGroupScheme
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.multiservice.events.UserRemovedFromGroupEvent
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.openproject.IOpenProject
import kotlinx.coroutines.runBlocking
import mu.KLogging

internal class DeleteGroup(
    private val credentialProvider: ICredentialProvider,
    private val openproject: IOpenProject?,
    private val nextcloud: Nextcloud?,
    private val civicrm: ICiviCRM?,
    private val chapterDB: ChapterDB,
    private val userRemovedFromGroupEvent: List<UserRemovedFromGroupEvent>,
) {
    companion object : KLogging() {
    }

    private fun identifyRelatedGroups(mainGroup: Group): List<Group> {
        // find related groups
        val listOfRelatedGroups = listOf(mainGroup).toMutableList()
        IogGroupSchema.possibleSubGroupSuffixes
            .mapNotNull { credentialProvider.getGroupByName(mainGroup.name + it) }
            .forEach {
                listOfRelatedGroups.add(it)
            }
        return listOfRelatedGroups
    }

    fun deleteGroup(groupId: Int): Group {
        logger.info { "DeleteGroup::deleteGroup Deleting group structure ($groupId)" }

        val group =
            credentialProvider.getGroup(groupId)
                ?: throw IllegalArgumentException("DeleteGroup::deleteGroup Group ($groupId) not found")
        val groups = identifyRelatedGroups(group)

        groups.forEach {
            logger.info { "DeleteGroup::deleteGroup Deleting LDAP group (${it.name})" }

            group.groupMembers.forEach { member -> userRemovedFromGroupEvent.forEach { it.userRemovedFromGroup(member, group.id) } }
            credentialProvider.getGroup(it.id)?.let {
                    freshReadGroup ->
                credentialProvider.deleteGroup(freshReadGroup)
            }
        }
        chapterDB.deleteByGroupname(group.name)

        var descriptionOfOP = ""

        if (openproject != null) {
            // Delete in OpenProject
            val openProjectGroups = runBlocking { groups.mapNotNull { openproject.getGroupByName(it.name) } }

            if (openProjectGroups.isEmpty()) {
                logger.warn { "DeleteGroup::deleteGroup Could not find group(s) related to \"${group.name}\" in OpenProject." }
            } else {
                var openProjectProject = runBlocking { openproject.getProjectByIdentifierOrName(group.name) }

                if (openProjectProject == null) {
                    val kindPrefix = IogGroupSchema.possibleKindPrefixes.firstOrNull { group.name.startsWith(it) }
                    if (kindPrefix != null) {
                        openProjectProject =
                            runBlocking {
                                openproject.getProjectByIdentifierOrName(
                                    group.name.substring(kindPrefix.length),
                                )
                            }
                    }
                }

                if (openProjectProject == null) {
                    logger.warn { "DeleteGroup::deleteGroup Could not find project with name \"${group.name}\" in OpenProject." }
                } else {
                    kotlin.runCatching {
                        runBlocking {
                            descriptionOfOP = openProjectProject.name
                            openproject.deleteProject(openProjectProject)
                        }
                    }.onFailure {
                        logger.error(
                            it,
                        ) { "DeleteGroup::deleteGroup Project deletion (${openProjectProject.name}) in OpenProject not successfull." }
                    }
                }

                kotlin.runCatching {
                    runBlocking { openProjectGroups.forEach { g -> openproject.deleteGroup(g) } }
                }.onFailure {
                    logger.error(it) { "DeleteGroup::deleteGroup Group deletion in OpenProject not successfull." }
                }
            }
        } else {
            logger.warn { "DeleteGroup::deleteGroup openproject is null. Cannot delete." }
        }

        if (nextcloud != null) {
            // Identify folders in Nextcloud
            logger.info {
                "DeleteGroup::deleteGroup try to identify any folder with name ${group.name} and ${group.description} in possibleParentsfolders: ${NextcloudFolders.parentCandidates}"
            }

            val parentFolder =
                NextcloudFolders.parentCandidates.mapNotNull { parentFolder ->
                    nextcloud.listFolders(parentFolder)
                        .find { it == group.name || it == group.description || it == descriptionOfOP }
                        ?.let { "$parentFolder/$it" }
                }.firstOrNull()

            // Delete group in Nextcloud if a folder was found
            if (parentFolder != null) {
                logger.info { "DeleteGroup::deleteGroup Removing Nextcloud folder: $parentFolder" }

                try {
                    nextcloud.delete(parentFolder)
                } catch (e: Exception) {
                    logger.error { "Failed to delete Nextcloud folders: $parentFolder -> $e" }
                }
            }

            // Remove group from shared folder
            val mainGroupFolder = nextcloud.findGroupFolder(NextcloudFolders.GroupShare)
            if (mainGroupFolder != null) {
                val mainFolderGroups: Map<String, Set<PermissionType>>? = mainGroupFolder.groups
                if (mainFolderGroups != null) {
                    groups.filter { mainFolderGroups.containsKey(it.name) }
                        .forEach { nextcloud.removeGroupFromGroupFolder(mainGroupFolder, it.name) }
                }
            }

            // Remove input group folder (if it exists)
            val inputMountpoint = NextcloudFolders.inputFolder + " " + group.description
            val inputGroupFolder = nextcloud.findGroupFolder(inputMountpoint)
            if (inputGroupFolder != null) {
                nextcloud.removeGroupFolder(inputGroupFolder)
            }
        } else {
            logger.warn { "DeleteGroup::deleteGroup nextcloud is null. cannot delete" }
        }

        civicrm?.let {
            val specialNames = listOf(LdapNames.ASSOCIATION_BOARD, LdapNames.CENTRAL_OFFICE, LdapNames.ETHIC_VALIDATION_GROUP)
            val civiGroupName =
                if (specialNames.contains(group.name) || group.name.endsWith(IogPluginConstants.CRM_SUFFIX)) {
                    group.name
                } else {
                    group.name + IogPluginConstants.CRM_SUFFIX
                }

            CiviGroupScheme(credentialProvider, civicrm).deleteChapter(civiGroupName)
        }

        return group
    }
}
