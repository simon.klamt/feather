/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.openproject.OpenProjectProject
import kotlinx.coroutines.runBlocking
import mu.KLogging

internal data class OpenProjectDetails(
    val groups: List<OpenProjectGroup>,
    val project: OpenProjectProject?,
    val workPackageStats: Map<String, Int>?,
)

internal data class NextcloudDetails(
    val rootPath: String,
    val numEntries: Int?,
)

internal data class GroupDetails(
    val groups: List<Group>,
    var openProject: OpenProjectDetails?,
    var nextcloud: NextcloudDetails?,
) {
    fun addOpenProjectDetails(
        groups: List<OpenProjectGroup>,
        project: OpenProjectProject?,
        workPackageStats: Map<String, Int>?,
    ) {
        if (openProject != null) {
            throw IllegalStateException("OpenProject details already defined")
        }
        openProject = OpenProjectDetails(groups, project, workPackageStats)
    }

    fun addNextcloudDetails(
        rootPath: String,
        numEntries: Int?,
    ) {
        if (nextcloud != null) {
            throw IllegalStateException("Nextcloud details already defined")
        }
        nextcloud = NextcloudDetails(rootPath, numEntries)
    }
}

internal class GetGroupDetails(
    private val openproject: IOpenProject?,
    private val nextcloud: Nextcloud?,
    private val credentialProvider: ICredentialProvider,
) {
    companion object : KLogging()

    private fun identifyRelatedGroups(group: Group): List<Group> {
        // identify main group
        val possibleMainGroups =
            IogGroupSchema.possibleSubGroupSuffixes.filter { group.name.endsWith(it) }
                .associateWith { group.name.substring(0, group.name.length - it.length) }
                .mapValues { credentialProvider.getGroupByName(it.value) }
                .filter { it.value != null }
        val mainGroup =
            if (possibleMainGroups.isNotEmpty()) {
                val prioritizedKey = IogGroupSchema.possibleSubGroupSuffixes.first { possibleMainGroups.containsKey(it) }
                prioritizedKey to possibleMainGroups[prioritizedKey]!!
            } else {
                "" to group
            }

        // find related groups
        val listOfRelatedGroups = listOf(mainGroup.component2()).toMutableList()
        if (group.id != mainGroup.component2().id) {
            listOfRelatedGroups.add(group)
        }
        IogGroupSchema.possibleSubGroupSuffixes.filter { it != mainGroup.component1() }
            .mapNotNull { credentialProvider.getGroupByName(it + mainGroup.component2().name) }
            .forEach {
                listOfRelatedGroups.add(it)
            }
        return listOfRelatedGroups
    }

    fun getGroupDetails(groupId: Int): GroupDetails {
        logger.info { "GetGroupDetails::getGroupDetails Identifying group ($groupId)" }

        val group =
            credentialProvider.getGroup(groupId)
                ?: throw IllegalArgumentException("GetGroupDetails::getGroupDetails Group ($groupId) not found")
        val groups = identifyRelatedGroups(group)

        val mainGroup = groups.firstOrNull()!!

        logger.info { "GetGroupDetails::getGroupDetails Identified main group: ${mainGroup.name}" }
        groups.drop(1).forEach {
            logger.info { "GetGroupDetails::getGroupDetails Identified sub group: ${it.name}" }
        }

        val groupDetails = GroupDetails(groups, null, null)
        var descriptionOfOP = ""
        openproject?.let {
            logger.info { "GetGroupDetails::getGroupDetails identify group in openproject ..." }
            // Identify in OpenProject
            val openProjectGroups = runBlocking { groups.mapNotNull { openproject.getGroupByName(it.name) } }

            if (openProjectGroups.isEmpty()) {
                logger.warn { "GetGroupDetails::getGroupDetails Could not find group(s) related to \"${mainGroup.name}\" in OpenProject." }
            } else {
                var openProjectProject = runBlocking { openproject.getProjectByIdentifierOrName(mainGroup.name) }

                if (openProjectProject == null) {
                    val kindPrefix = IogGroupSchema.possibleKindPrefixes.firstOrNull { mainGroup.name.startsWith(it) }
                    if (kindPrefix != null) {
                        openProjectProject =
                            runBlocking {
                                openproject.getProjectByIdentifierOrName(
                                    mainGroup.name.substring(kindPrefix.length),
                                )
                            }
                    }
                }

                if (openProjectProject == null) {
                    logger.warn { "GetGroupDetails::getGroupDetails Could not find project with name \"${mainGroup.name}\" in OpenProject." }

                    groupDetails.addOpenProjectDetails(openProjectGroups, null, null)
                } else {
                    descriptionOfOP = openProjectProject.name
                    val workPackages =
                        runBlocking {
                            openproject.getWorkPackagesOfProject(openProjectProject, openproject.getStatus())
                        }

                    val statusList = workPackages.map { it.status.name }

                    val workPackageStats =
                        statusList.associateWith {
                            workPackages.filter { e ->
                                e.status.name == it
                            }.size
                        }

                    groupDetails.addOpenProjectDetails(
                        openProjectGroups,
                        openProjectProject,
                        workPackageStats,
                    )
                    logger.info { "GetGroupDetails::getGroupDetails groups: ${openProjectGroups.map { it.name }}  project: ${openProjectProject.name}" }
                }
            }
        } ?: logger.warn { "GetGroupDetails::getGroupDetails openproject service is null!" }

        nextcloud?.let {
            logger.info { "GetGroupDetails::getGroupDetails analyze nextcloud ..." }

            logger.info { "GetGroupDetails::getGroupDetails check folder candidates: ${NextcloudFolders.parentCandidates.map { it }}" }

            val parentFolder =
                NextcloudFolders.parentCandidates.firstNotNullOfOrNull { parentFolder ->
                    nextcloud.listFolders(parentFolder)
                        .find { it == mainGroup.name || it == mainGroup.description || it == descriptionOfOP }
                        ?.let {
                            logger.info { "GetGroupDetails::getGroupDetails found folder $parentFolder/$it" }
                            "$parentFolder/$it"
                        }
                }

            // fill in details if a folder was found
            if (parentFolder != null) {
                logger.info { "GetGroupDetails::getGroupDetails Select first of it. Identified nextcloud folder: $parentFolder" }

                val numEntries =
                    try {
                        // count real child entries (first entry is parent)
                        nextcloud.listFiles(parentFolder).size
                    } catch (e: Exception) {
                        logger.error { "Failed to get entries of Nextcloud folders: $parentFolder -> $e" }
                        null
                    }
                groupDetails.addNextcloudDetails(parentFolder, numEntries)
            }

            // TODO: report on Input Folder (if this exists as fix for the upload problem)
            /*val inputMountpoint = NextcloudFolders.inputFolder + " " + (group.description ?: group.name)
            val inputGroupFolder = nextcloud.findGroupFolder(inputMountpoint)*/
        } ?: logger.warn { "GetGroupDetails::getGroupDetails nextcloud service is null!" }

        logger.info {
            "GetGroupDetails::getGroupDetails return with: " +
                "nextcloud rootfolder: ${groupDetails.nextcloud?.rootPath} and ${groupDetails.nextcloud?.numEntries} as well as" +
                "openproject groups: ${groupDetails.openProject?.groups?.map{it.name}} and project ${groupDetails.openProject?.project?.name}"
        }
        return groupDetails
    }
}
