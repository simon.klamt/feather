/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.settings

object LdapNames {
    const val INTERESTED_PEOPLE = "interestedpeople"
    const val TRIAL_MEMBERS = "trialmembers"
    const val CRM_MEMBERS = "crmmembers"
    const val IOG_MEMBERS = "iogmembers"
    const val CENTRAL_OFFICE = "geschaeftsstelle"
    const val OMS = "oms"
    const val PROKO = "proko"
    const val ASSOCIATION_BOARD = "vorstand"
    const val AP_SPRECHERINNEN = "ap-sprecherinnen"
    const val KG_FUNDRAISING = "kg-fundraising"
    const val ETHIC_VALIDATION_GROUP = "ethikpruefer"

    val opIgnoreList = listOf("password_policy_no_change", "editors", "trust admins", "ipausers")
}
