/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.settings

import dev.maximilian.feather.civicrm.CiviCRMConstants

object IogPluginConstants {
    const val MEMBER_SUFFIX = "-member"
    const val INTERESTED_SUFFIX = "-interested"
    const val TRIAL_SUFFIX = "-trial"
    const val ADMIN_SUFFIX = "-admin"
    const val CRM_SUFFIX = "-crm"
    const val RG_PREFIX = "rg-"
    const val KG_PREFIX = "kg-"
    const val AS_PREFIX = "as-"
    const val BILA_SUFFIX = "-bila"
    const val PR_FR_SUFFIX = "-prfr"
    const val PROGRAM_PREFIX = "progr-"
    const val ANKER_PREFIX = "ankerpartner-"
    const val CONTACT_SUFFIX = "-contacts"
    const val ORGA_SUFFIX = "-organizations"

    const val REGIONAL_GROUP_TEMPLATE = ""
    const val BILA_GROUP_TEMPLATE = ""
    const val PR_FR_GROUP_TEMPLATE = ""
    const val COMPETENCE_GROUP_TEMPLATE = ""
    const val COMMITTEE_TEMPLATE = ""
    const val COLLABORATION_TEMPLATE = ""
    const val PROJECT_TEMPLATE = ""
    const val SANDBOX_PROJECT_ID = "sandbox"
    const val TEMPLATE_ONLY = "work_packages"

    const val SUPPORT_MEMBERSHIP_HASH_ALGORITHM = "SHA-256"

    enum class CiviCRMCustomType(val protocolName: String, val parent: CiviCRMConstants.ContactTypeDefaults, val label: String) {
        IogMember("IogMember", CiviCRMConstants.ContactTypeDefaults.Individual, "IOG-Mitglied"),
        Employee("Employee", CiviCRMConstants.ContactTypeDefaults.Individual, "Mitarbeiter"),
        Association("Association", CiviCRMConstants.ContactTypeDefaults.Organization, "Unternehmen"),
        Foundation("Foundation", CiviCRMConstants.ContactTypeDefaults.Organization, "Stiftung"),
        Company("Company", CiviCRMConstants.ContactTypeDefaults.Organization, "Verband"),
        MediaCompany("MediaCompany", CiviCRMConstants.ContactTypeDefaults.Organization, "Medienunternehmen"),
        DefaultOrganization("DefaultOrganization", CiviCRMConstants.ContactTypeDefaults.Organization, "Sonstige Körperschaft"),
    }
}
