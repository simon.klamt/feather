/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group.plausibility

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.iog.api.GroupConstraints
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.groupPattern.IGroupPattern
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupResolver
import dev.maximilian.feather.iog.settings.GroupKind

internal data class LDAPStatusResult(
    val errorMessage: MutableList<String>,
    val patternType: Pair<IogGroupSchema.PatternTypes, GroupKind>,
    val pattern: IGroupPattern?,
)

internal class CredentialPlausibility(private val iogSchema: IogGroupSchema, private val credentials: ICredentialProvider) {
    private val repairable = RepairMessages.AUTO
    private val repaired = RepairMessages.REPAIR_DONE
    private val notRepairable = RepairMessages.MANUAL

    internal fun checkLDAPStatus(
        group: Group,
        allUsers: Collection<User>,
        allGroups: Collection<Group>,
        autoRepair: Boolean,
        description: String?,
    ): LDAPStatusResult {
        val errorMessages = mutableListOf<String>()

        if (group.name.isEmpty()) errorMessages += "Der Gruppenname darf nicht leer sein $notRepairable."
        if (!GroupConstraints.NameLength.contains(group.name.length)) {
            errorMessages += "Ein Name benötigt mindestens ${GroupConstraints.NameLength.first} und maximal ${GroupConstraints.NameLength.last} Zeichen $notRepairable."
        }

        group.owners.forEach { ownerID ->
            if (!allUsers.any {
                    ownerID == it.id
                }
            ) {
                errorMessages += "$ownerID bei Owners konnte nicht gefunden werden $notRepairable."
            }
        }

        if (group.description.isBlank()) {
            errorMessages += "Die Gruppenbeschreibung darf nicht leer sein $notRepairable."
        }
        if (!GroupConstraints.DescriptionLength.contains(group.description.length)) {
            errorMessages += "Eine Beschreibung benötigt mindestens ${GroupConstraints.DescriptionLength.first} und maximal ${GroupConstraints.DescriptionLength.last} Zeichen $notRepairable."
        }

        val resolver = GroupResolver(allUsers, allGroups)
        val patternType = iogSchema.identifyNodePatternType(group.name)

        val pattern =
            when (patternType.first) {
                IogGroupSchema.PatternTypes.MemberAdminInterested -> iogSchema.memberAdminInterestedPattern
                IogGroupSchema.PatternTypes.MemberCrmInterested -> iogSchema.memberCrmPattern
                IogGroupSchema.PatternTypes.SimpleMemberAdmin -> iogSchema.simpleAdminPattern
                IogGroupSchema.PatternTypes.SimpleMemberAdminCRM -> iogSchema.simpleAdminPatternCRM
                else -> null
            }

        description?.let {
            pattern?.let {
                val nominalMeta = pattern.getMetaDescription(group.name, description)
                val existingGroup = resolver.getMetaDescription(group)

                if (existingGroup.description != nominalMeta.description && (patternType.second == GroupKind.REGIONAL_GROUP || patternType.second == GroupKind.PROJECT)) {
                    errorMessages += "Die Beschreibung weicht von der geforderten Beschreibung ab. Gefordert ist <${nominalMeta.description}>"
                    if (autoRepair) {
                        credentials.updateGroup(group.copy(description = nominalMeta.description))
                        errorMessages += "$repaired."
                    } else {
                        errorMessages += "$repairable."
                    }
                } else {
                    if (!existingGroup.description.startsWith(nominalMeta.description)) {
                        errorMessages += "Die Beschreibung weicht von der geforderten Beschreibung ab. Gefordert ist der Anfang <${nominalMeta.description}> $notRepairable."
                    }
                }

                if (!it.userMembersAllowed(group.name) && !group.userMembers.isEmpty()) {
                    errorMessages += "Diese Gruppe darf keine direkten Member beinhalten $notRepairable."
                }

                if (existingGroup.groupMemberNames != nominalMeta.groupMemberNames) {
                    errorMessages += "Die Gruppenmitglieder (Groupmember) weichen von den geforderten Gruppenmitgliedern ab. Gefordert ist <${nominalMeta.groupMemberNames}>, real ist <${existingGroup.groupMemberNames}>."
                    if (autoRepair) {
                        credentials.updateGroup(
                            group.copy(
                                groupMembers =
                                nominalMeta.groupMemberNames.mapNotNull { g ->
                                    credentials.getGroupByName(
                                        g,
                                    )?.id
                                }.toSet(),
                            ),
                        )
                        errorMessages += "$repaired."
                    } else {
                        errorMessages += "$repairable."
                    }
                }

                if (existingGroup.ownedGroupNames != nominalMeta.ownedGroupNames) {
                    errorMessages += "Die besessenen Gruppen (OwnedGroup) weichen von den geforderten ab: Gefordert ist <${nominalMeta.ownedGroupNames}>. Gesetzt ist <${existingGroup.ownedGroupNames}>."
                    if (autoRepair) {
                        credentials.updateGroup(
                            group.copy(
                                ownedGroups =
                                nominalMeta.ownedGroupNames.mapNotNull { g ->
                                    credentials.getGroupByName(
                                        g,
                                    )?.id
                                }.toSet(),
                            ),
                        )
                        errorMessages += "$repaired."
                    } else {
                        errorMessages += "$repairable."
                    }
                }
                group.groupMembers.forEach {
                        groupM ->
                    if (!allGroups.any {
                            it.id == groupM
                        }
                    ) {
                        errorMessages += "Die Gruppe ${group.name} von groupMembers konnte nicht gefunden werden $notRepairable."
                    }
                }
                group.ownedGroups.forEach {
                        owned ->
                    if (!allGroups.any { it.id == owned }) errorMessages += "Die Gruppe $owned von ownedGroups konnte nicht gefunden werden $notRepairable."
                }
            }
        }

        return LDAPStatusResult(errorMessages, patternType, pattern)
    }
}
