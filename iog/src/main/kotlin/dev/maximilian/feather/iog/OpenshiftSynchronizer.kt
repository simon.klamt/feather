/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.kubernetes.OpenProjectLdapSync
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectGroup

class OpenshiftSynchronizer(
    private val kubernetesOpenProjectLDAPSync: OpenProjectLdapSync?,
    private val baseDnGroups: String,
) :
    GroupSynchronizationEvent {
    override val k8sSyncEnabled: Boolean = kubernetesOpenProjectLDAPSync != null

    override fun synchronize() {
        kubernetesOpenProjectLDAPSync?.synchronizeOpenProjectGroupsWithLDAP()
    }

    override suspend fun createOPSync(
        openproject: IOpenProject,
        createdLdapGroups: List<Group>,
        newOpenProjectGroups: List<OpenProjectGroup>,
    ) {
        try {
            createdLdapGroups.map { it.name }.zip(newOpenProjectGroups).forEach {
                openproject.createLdapGroupSynchronisation(
                    it.second,
                    "cn=${it.first},$baseDnGroups",
                    1,
                )
            }
        } catch (e: Exception) {
            CreateGroup.logger.error(e) {
                "OpenshiftSynchronizer::createOPSync Registering group synchronisation in OpenProject not successful." +
                    "LDAP Groups: <${createdLdapGroups.joinToString(",") { it.name }}> " +
                    "OpenProjectGroups: <${newOpenProjectGroups.joinToString(",") { it.name }}>" +
                    " baseDnGroups = $baseDnGroups"
            }
            throw RuntimeException("Failed to register group synchronisation in OpenProject")
        }
    }
}
