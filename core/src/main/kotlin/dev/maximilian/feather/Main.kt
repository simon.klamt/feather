/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.api.ActionApi
import dev.maximilian.feather.api.ConfigApi
import dev.maximilian.feather.api.GdprAPI
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.authorization.AuthorizerProviderFactory
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.authorization.LdapProviderFactory
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.database.DatabaseProperties
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.iog.IogPluginFactory
import dev.maximilian.feather.iog.ServiceFactory
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.api.BackgroundJobApi
import dev.maximilian.feather.multiservice.events.UserCreationEvent
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.plugin.FeatherPluginInitialization
import dev.maximilian.feather.plugin.PluginLoader
import dev.maximilian.feather.util.Benchmark
import dev.maximilian.feather.util.CredentialProfiler
import dev.maximilian.feather.util.FeatherProperties
import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import redis.clients.jedis.JedisPool
import java.sql.DriverManager
import java.time.Instant
import java.util.Properties
import java.util.TimeZone
import java.util.UUID

internal fun getEnv(
    name: String,
    default: String? = null,
): String = System.getenv(name) ?: default ?: throw IllegalArgumentException("Environment variable $name not found")

public fun main() {
    System.setProperty("loglevel", getEnv("LOGLEVEL", "INFO"))
    val logger = KotlinLogging.logger { }
    logger.info { "Starting Feather Backend version: ${Main.VERSION}" }
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    FeatherProperties.apply { /* Init all database values! */ }

    logger.info { "Init acc controller" }
    Main.ACCOUNT_CONTROLLER

    val additionalAllowedOrigins =
        jacksonObjectMapper().readValue<Set<String>>(FeatherProperties.ADDITIONAL_ALLOWED_ORIGINS)

    if (Main.CREDENTIAL_MANAGER.getUsers().none { it.permissions.contains(Permission.ADMIN) }) {
        val password = UUID.randomUUID().toString()
        logger.warn {
            """
            ########## NO ADMIN USER FOUND


            No admin user found, creating "admin <admin@example.org>" with password "$password"


            ########## CREATED DEFAULT ADMIN
            """.trimIndent()
        }
        Main.CREDENTIAL_MANAGER.createUser(
            User(
                id = 0,
                username = "admin",
                displayName = "Feather Admin",
                firstname = "Admin",
                surname = "Admin",
                mail = "admin@example.org",
                groups = emptySet(),
                registeredSince = Instant.now(),
                ownedGroups = emptySet(),
                permissions = setOf(Permission.ADMIN),
                disabled = false,
                lastLoginAt = Instant.ofEpochSecond(0),
            ),
            password,
        )
    }

    val appBuilder = AppBuilder()
    val app =
        appBuilder.createApp(
            Main.VERSION,
            "feather",
            FeatherProperties.BASE_URL,
            additionalAllowedOrigins,
            7000,
            FeatherProperties.METRICS_PASSWORD,
        )
    val pluginLoader = PluginLoader()

    runCatching {
        logger.info { "main() Init ActionController ..." }
        val actionController = ActionController(Main.DATABASE, Main.ACCOUNT_CONTROLLER)

        logger.info { "main() Init GdprController ..." }
        val gdprController = GdprController(Main.DATABASE, actionController, Main.ACCOUNT_CONTROLLER)

        logger.info { "main() Init AuthoriationController ..." }
        val authorizationController =
            AuthorizationController(
                Main.AUTHORIZER_SET,
                Main.ACCOUNT_CONTROLLER,
                Main.DATABASE,
                actionController,
            )

        logger.info { "main() Init BackgroundJobManager and BackgroundJobApi ..." }
        val backgroundJobManager = BackgroundJobManager(Main.REDIS)
        BackgroundJobApi(app, backgroundJobManager)

        logger.info { "main() Init pluginLoader..." }
        pluginLoader.initialize(
            FeatherPluginInitialization(
                propertyMap = Main.PROPERTIES,
                baseUrl = FeatherProperties.BASE_URL,
                db = Main.DATABASE,
                app = app,
                actionController = actionController,
                authorizationController = authorizationController,
                accountController = Main.ACCOUNT_CONTROLLER,
            ),
        )
        pluginLoader.start()

        logger.info { "main() Init AuthorizerApi..." }
        val authorizerApi =
            AuthorizerApi(app, authorizationController, FeatherProperties.DISABLE_SECURE_COOKIE == "true")
        logger.info { "main() Init ActionAPI..." }
        ActionApi(app, actionController, authorizationController)

        logger.info { "main() Init GdprAPI..." }
        GdprAPI(app, gdprController)

        logger.info { "main() Start IOG PluginFactory ..." }
        val iogPlugin =
            IogPluginFactory.create(
                backgroundJobManager,
                authorizerApi as ISessionOperations,
                actionController,
                Main.ACCOUNT_CONTROLLER,
                emptyList(),
                listOf(
                    object :
                        UserCreationEvent {
                        override fun userCreated(user: User) {
                            gdprController.handleUserCreated(user)
                        }
                    },
                ),
            )

        logger.info { "main() Assign multiservice events ..." }

        val events =
            MultiServiceEvents(
                iogPlugin.synchronizeEvent,
                iogPlugin.userDeletionEvents,
                iogPlugin.userCreationEvents,
                authorizerApi as ISessionOperations,
                iogPlugin.checkUserEvent,
                listOfNotNull(iogPlugin.userMayAddCheck),
                emptyList(),
                iogPlugin.userRemovedFromGroupEvent,
            )

        logger.info { "main() Create multiservice ..." }
        val multiservice =
            ServiceFactory(app).createMultiService(
                backgroundJobManager,
                gdprController,
                actionController,
                events,
            )

        logger.info { "main() Init iogPlugin::startApis ..." }
        iogPlugin.startApis(app, multiservice.services)

        logger.info { "main() Init ConfigApi ..." }
        ConfigApi(app, Main.AUTHORIZER_SET, Main.VERSION, iogPlugin.config.syncEvent.k8sSyncEnabled)

        logger.info { "Add missing actions and periodically check for actions" }
        actionController.setupActions()
    }.onFailure {
        logger.error("Error while initialising", it)
        pluginLoader.stop()
        app.stop()
        pluginLoader.dispose()
    }
}

internal object Main {
    private val DB_URL by lazy { getEnv("DATABASE_URL") }
    val DATABASE: Database by lazy { Database.Companion.connect(getNewConnection = { DriverManager.getConnection(DB_URL) }) }
    val PROPERTIES: DatabaseProperties by lazy { DatabaseProperties(DATABASE) }
    val BENCHMARK: Benchmark by lazy { Benchmark() }
    val ACCOUNT_CONTROLLER by lazy { AccountController(DATABASE, LdapProviderFactory(PROPERTIES).create(), PROPERTIES) }
    private val UNCACHED_CREDENTIAL_MANAGER: ICredentialProvider by lazy { ACCOUNT_CONTROLLER }
    private val CREDENTIAL_MANAGER_LAZY = lazy { CredentialProfiler(BENCHMARK, UNCACHED_CREDENTIAL_MANAGER) }
    val CREDENTIAL_MANAGER: ICredentialProvider by CREDENTIAL_MANAGER_LAZY
    val AUTHORIZER_SET: MutableSet<IAuthorizer> by lazy {
        AuthorizerProviderFactory(PROPERTIES).create(
            FeatherProperties.BASE_URL,
            CREDENTIAL_MANAGER,
        ).toMutableSet()
    }
    val REDIS: JedisPool by lazy {
        kotlin.runCatching {
            RedisFactory(PROPERTIES).create().apply {
                resource.use { it.ping() }
            }
        }.onFailure {
            if (CREDENTIAL_MANAGER_LAZY.isInitialized()) {
                CREDENTIAL_MANAGER.close()
            }
        }.getOrThrow()
    }
    val VERSION: String by lazy {
        Properties().apply { load(Main::class.java.getResourceAsStream("/application.properties")) }
            .getProperty("version") ?: "undefined"
    }
}
