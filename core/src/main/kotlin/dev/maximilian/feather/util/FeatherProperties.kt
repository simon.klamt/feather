/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.util

import dev.maximilian.feather.Main
import dev.maximilian.feather.iog.CiviCrmConstants
import dev.maximilian.feather.iog.OpenProjectConstants
import dev.maximilian.feather.iog.settings.IogPluginConstants

internal object FeatherProperties {
    private val props = Main.PROPERTIES

    val SITENAME: String = props.getOrPut("sitename") { FeatherConstants.SITENAME.trimIndent() }
    val BOTNAME: String = props.getOrPut("botname") { FeatherConstants.BOTNAME.trimIndent() }
    val BASE_URL: String = props.getOrPut("baseurl") { FeatherConstants.BASE_URL.trimIndent() }
    val DISABLE_SECURE_COOKIE: String =
        props.getOrPut("disable_secure_cookie") { FeatherConstants.DISABLE_SECURE_COOKIE.trimIndent() }
    val ADDITIONAL_ALLOWED_ORIGINS: String =
        props.getOrPut("additional_allowed_origins") { FeatherConstants.ADDITIONAL_ALLOWED_ORIGINS.trimIndent() }

    val MAIL_PASSWORD_FORGOT_CONTENT: String =
        props.getOrPut("mail.password_forgot") { FeatherConstants.MAIL_PASSWORD_FORGOT.trimIndent() }
    val MAIL_PASSWORD_FORGOT_SUBJECT: String =
        props.getOrPut("mail.password_forgot_subject") { FeatherConstants.MAIL_PASSWORD_FORGOT_SUBJECT.trimIndent() }

    val MAIL_INVITATION: String = props.getOrPut("mail.invitation") { FeatherConstants.MAIL_INVITATION.trimIndent() }
    val MAIL_INVITATION_SUBJECT: String =
        props.getOrPut("mail.invitation_subject") { FeatherConstants.MAIL_INVITATION_SUBJECT.trimIndent() }

    val MAIL_CHANGE_MAIL_ADDRESS_CONTENT: String =
        props.getOrPut("mail.change_mail_address") { FeatherConstants.MAIL_CHANGE_MAIL_ADDRESS.trimIndent() }
    val MAIL_CHANGE_MAIL_ADDRESS_SUBJECT: String =
        props.getOrPut("mail.change_mail_address_subject") { FeatherConstants.MAIL_CHANGE_MAIL_ADDRESS_SUBJECT.trimIndent() }

    val MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION1: String =
        props.getOrPut("mail.change_mail_address_notification1") { FeatherConstants.MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION1.trimIndent() }
    val MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION2: String =
        props.getOrPut("mail.change_mail_address_notification2") { FeatherConstants.MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION2.trimIndent() }

    // Mail Properties
    val MAIL_HOST: String = props.getOrPut("mail.host") { FeatherConstants.MAIL_HOST.trimIndent() }
    val MAIL_PORT: String = props.getOrPut("mail.port") { FeatherConstants.MAIL_PORT.trimIndent() }
    val MAIL_AUTH: String = props.getOrPut("mail.auth") { FeatherConstants.MAIL_AUTH.trimIndent() }
    val MAIL_STARTTLS: String = props.getOrPut("mail.starttls") { FeatherConstants.MAIL_STARTTLS.trimIndent() }
    val MAIL_USERNAME: String = props.getOrPut("mail.username") { FeatherConstants.MAIL_USERNAME.trimIndent() }
    val MAIL_PASSWORD: String = props.getOrPut("mail.password") { FeatherConstants.MAIL_PASSWORD.trimIndent() }
    val MAIL_FROM: String = props.getOrPut("mail.from") { FeatherConstants.MAIL_FROM.trimIndent() }

    // Redis Properties
    val REDIS_HOST: String = props.getOrPut("redis.host") { FeatherConstants.REDIS_HOST.trimIndent() }
    val REDIS_PORT: String = props.getOrPut("redis.port") { FeatherConstants.REDIS_PORT.trimIndent() }

    // Nextcloud Binding Properties
    val NEXTCLOUD_BASE_URL: String =
        props.getOrPut("nextcloud.base_url") { FeatherConstants.NEXTCLOUD_BASE_URL.trimIndent() }
    val NEXTCLOUD_USERNAME: String =
        props.getOrPut("nextcloud.username") { FeatherConstants.NEXTCLOUD_USERNAME.trimIndent() }
    val NEXTCLOUD_PASSWORD: String =
        props.getOrPut("nextcloud.password") { FeatherConstants.NEXTCLOUD_PASSWORD.trimIndent() }
    val NEXTCLOUD_USER_BINDING: String =
        props.getOrPut("nextcloud.user_binding") { FeatherConstants.NEXTCLOUD_USER_BINDING.trimIndent() }
    val NEXTCLOUD_PUBLIC_URL: String =
        props.getOrPut("nextcloud.public_url") { FeatherConstants.NEXTCLOUD_PUBLIC_URL.trimIndent() }

    // OpenProject Properties
    val OPENPROJECT_BASE_URL: String =
        props.getOrPut("openproject.base_url") { OpenProjectConstants.BASEURL.trimIndent() }
    val OPENPROJECT_API_KEY: String = props.getOrPut("openproject.api_key") { OpenProjectConstants.APIKEY.trimIndent() }
    val OPENPROJECT_USER_BINDING: String =
        props.getOrPut("openproject.user_binding") { OpenProjectConstants.USER_BINDING.trimIndent() }
    val OPENPROJECT_PUBLIC_URL: String =
        props.getOrPut("openproject.public_url") { OpenProjectConstants.PUBLICURL.trimIndent() }

    // CiviCRM Binding Properties
    val CIVICRM_BASE_URL: String =
        props.getOrPut("civicrm.base_url") { CiviCrmConstants.BASEURL.trimIndent() }
    val CIVICRM_APIKEY: String =
        props.getOrPut("civicrm.apikey") { CiviCrmConstants.APIKEY.trimIndent() }

    // OpenProject-specific Properties for the Kubernetes Binding
    val K8S_OPENPROJECT_NAMESPACE: String =
        props.getOrPut("k8s.openproject.namespace") { FeatherConstants.K8S_OPENPROJECT_NAMESPACE.trimIndent() }
    val K8S_OPENPROJECT_DEPLOYMENT: String =
        props.getOrPut("k8s.openproject.deployment") { FeatherConstants.K8S_OPENPROJECT_DEPLOYMENT.trimIndent() }
    val K8S_OPENPROJECT_CONTAINER: String =
        props.getOrPut("k8s.openproject.container") { FeatherConstants.K8S_OPENPROJECT_CONTAINER.trimIndent() }

    // Multiservice IoG Plugin Binding Properties
    val IOG_SUPPORT_MEMBERSHIP_ENABLED: Boolean =
        props.getOrPut("iog.support_membership.enabled") { "false" } != "false"
    val IOG_SUPPORT_MEMBERSHIP_HASH_ALGO =
        props.getOrPut("iog.support_membership.hash_algorithm") { IogPluginConstants.SUPPORT_MEMBERSHIP_HASH_ALGORITHM.trimIndent() }
    val IOG_SUPPORT_MEMBERSHIP_ENFORCED: Boolean =
        props.getOrPut("iog.support_membership.enforced") { "false" } != "false"

    // Migration config
    val MIGRATE_OLD_IDS: Int = (props.getOrPut("MIGRATE_OLD_IDS") { "0" }).toIntOrNull() ?: 0

    // Metrics password
    val METRICS_PASSWORD: String? = props.getOrPut("metrics.password") { "" }.trim().takeIf { it.isNotBlank() }
}

internal object FeatherConstants {
    const val MAIL_PASSWORD_FORGOT_SUBJECT: String = "{{ sitename }} - Passwort zurücksetzen"
    const val MAIL_PASSWORD_FORGOT: String =
        """
        Hallo {{ firstname }} {{ surname }} ({{ username }}),
        du oder jemand anderes hat versucht das Passwort für deinen Account zurückzusetzen. Falls du das warst, klicke bitte auf folgenden Link, falls nicht ignoriere diese Mail einfach.
        {{ link }}
        
        --
        Mit freundlichen Grüßen
        {{ botname }}
        """

    const val MAIL_INVITATION_SUBJECT: String = "{{ sitename }} - Einladung"
    const val MAIL_INVITATION: String =
        """
        Hallo {{ firstname }} {{ surname }},
        du wurdest auf unsere Kollaborationsplattform eingeladen. Bitte klicke auf den Link um deine Registrierung abzuschließen:
        {{ link }}
        
        --
        Mit freundlichen Grüßen
        {{ botname }}
        """

    const val MAIL_CHANGE_MAIL_ADDRESS_SUBJECT: String = "{{ sitename }} - Mail-Addresse ändern"
    const val MAIL_CHANGE_MAIL_ADDRESS: String =
        """
        Hallo {{ firstname }} {{ surname }} ({{ username }}),
        du willst die für deinen Account hinterlegte Mail-Addresse ändern. Falls dies dein Account ist, klicke bitte auf folgenden Link um die Änderung abzuschließen, falls nicht ignoriere diese Mail einfach.
        {{ link }}
        
        --
        Mit freundlichen Grüßen
        {{ botname }}
        """

    const val MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION1: String =
        """
        Hallo {{ firstname }} {{ surname }} ({{ username }}),
        du willst die für deinen Account hinterlegte Mail-Addresse zu {{ newMail }} ändern. Falls du dies nicht warst, dann informiere bitte deinen Knotenadmin über diesen Vorgang.
        
        --
        Mit freundlichen Grüßen
        {{ botname }}
        """

    const val MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION2: String =
        """
        Hallo {{ firstname }} {{ surname }} ({{ username }}),
        du hast die für deinen Account hinterlegte Mail-Addresse erfolgreich von {{ oldMail }} zu {{ newMail }} geändert.
        
        --
        Mit freundlichen Grüßen
        {{ botname }}
        """

    const val SITENAME: String = "Feather"
    const val BOTNAME: String = "Feather Bot"
    const val BASE_URL: String = "http://localhost:8080"
    const val DISABLE_SECURE_COOKIE: String = "false"
    const val ADDITIONAL_ALLOWED_ORIGINS: String = "[]"

    // Mail Constants
    const val MAIL_HOST: String = "localhost"
    const val MAIL_PORT: String = "25"
    const val MAIL_AUTH: String = "false"
    const val MAIL_STARTTLS: String = "false"
    const val MAIL_USERNAME: String = ""
    const val MAIL_PASSWORD: String = ""
    const val MAIL_FROM: String = "feather@maximilian.dev"

    // Redis Constants
    const val REDIS_HOST: String = "localhost"
    const val REDIS_PORT: String = "6379"

    // Nextcloud Binding Constants
    const val NEXTCLOUD_BASE_URL: String = ""
    const val NEXTCLOUD_USERNAME: String = ""
    const val NEXTCLOUD_PASSWORD: String = ""
    const val NEXTCLOUD_USER_BINDING: String = "id"
    const val NEXTCLOUD_PUBLIC_URL: String = ""

    // OpenProject-specific Constants for the Kubernetes Binding
    const val K8S_OPENPROJECT_NAMESPACE: String = ""
    const val K8S_OPENPROJECT_DEPLOYMENT: String = ""
    const val K8S_OPENPROJECT_CONTAINER: String = ""
}
