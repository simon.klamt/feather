/*
*    Copyright [2020-2021] Feather development team, see AUTHORS.md
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package dev.maximilian.feather.api

import dev.maximilian.feather.Permission
import dev.maximilian.feather.gdpr.GdprAcceptance
import dev.maximilian.feather.gdpr.GdprAcceptanceType
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.gdpr.GdprDocument
import dev.maximilian.feather.sessionOrMinisession
import dev.maximilian.feather.sessionOrMinisessionOrNull
import dev.maximilian.feather.toUserRequestInfo
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.Context
import io.javalin.http.NotFoundResponse
import io.javalin.http.bodyAsClass
import io.javalin.openapi.HttpMethod
import io.javalin.openapi.OpenApi
import io.javalin.openapi.OpenApiContent
import io.javalin.openapi.OpenApiParam
import io.javalin.openapi.OpenApiRequestBody
import io.javalin.openapi.OpenApiResponse
import org.eclipse.jetty.http.HttpStatus
import java.time.Instant

internal class GdprAPI(
    app: Javalin,
    private val gdprController: GdprController,
) {
    init {
        app.routes {
            path("/gdpr") {
                path("/documents") {
                    post(::handlePublishDocument, Permission.ADMIN, Permission.MANAGE_GDPR)
                    get(::handleGetAllDocuments)

                    path("/{id}") {
                        get(::handleGetDocument)
                        post("/accept", ::handleAcceptGDPR)

                        get("/status", ::handleGetStatus, Permission.ADMIN, Permission.MANAGE_GDPR)
                        get("/status/me", ::handleGetMyStatus)
                    }
                }
            }
        }
    }

    @OpenApi(
        summary = "Get GDPR(DSGVO) acceptance status for all users (admin permission required) or self",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/gdpr/documents/{id}/status",
        methods = [HttpMethod.GET],
        pathParams = [OpenApiParam(name = "id", description = "ID of document. Number or \"latest\"", required = true, example = "latest")],
    )
    private fun handleGetStatus(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val documentId = ctx.pathParam("id")
        val answer = gdprController.getAcceptanceInfo(session.toUserRequestInfo(), documentId) ?: throw NotFoundResponse("GDPR document with id $documentId not found")
        val acceptMap = answer.map { it.toGdprAcceptanceAnswer() }

        ctx.status(HttpStatus.OK_200)
        ctx.json(acceptMap)
    }

    @OpenApi(
        summary = "Get GDPR(DSGVO) acceptance status for all users (admin permission required) or self",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/gdpr/documents/{id}/status/me",
        methods = [HttpMethod.GET],
        pathParams = [OpenApiParam(name = "id", description = "ID of document. Number or \"latest\"", required = true, example = "latest")],
    )
    private fun handleGetMyStatus(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val documentId = ctx.pathParam("id")
        val result = gdprController.getAcceptanceInfoOfUser(session.toUserRequestInfo(), documentId, session.user) ?: throw NotFoundResponse("Not accepted to this gdpr document")

        ctx.status(HttpStatus.OK_200)
        ctx.json(result.timestamp)
    }

    @OpenApi(
        summary = "Currently authenticated user accepts latest GDPR(DSGVO) document release",
        responses = [OpenApiResponse("204"), OpenApiResponse("400"), OpenApiResponse("403"), OpenApiResponse("404")],
        path = "/v1/gdpr/documents/{id}/accept",
        methods = [HttpMethod.POST],
        pathParams = [OpenApiParam(name = "id", description = "ID of document. Number or \"latest\"", required = true, example = "latest")],
    )
    private fun handleAcceptGDPR(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val documentId = ctx.pathParam("id").toIntOrNull()

        requireNotNull(documentId) { "id is not an integer" }

        ctx.json(gdprController.storeAcceptanceInfo(session.toUserRequestInfo(), documentId))
        ctx.status(HttpStatus.OK_200)
    }

    @OpenApi(
        summary = "Get all available GDPR(DSGVO) documents",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200")],
        path = "/v1/gdpr/documents",
        methods = [HttpMethod.GET],
    )
    private fun handleGetAllDocuments(ctx: Context) {
        val session = ctx.sessionOrMinisessionOrNull()

        ctx.status(HttpStatus.OK_200)
        ctx.json(gdprController.getDocuments(session?.toUserRequestInfo()))
    }

    @OpenApi(
        summary = "Get specific GDPR(DSGVO) document, use latest as id to get the latest one",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("404")],
        path = "/v1/gdpr/documents/{id}",
        methods = [HttpMethod.GET],
        pathParams = [OpenApiParam(name = "id", description = "ID of document. Number or \"latest\"", required = true, example = "latest")],
    )
    private fun handleGetDocument(ctx: Context) {
        val session = ctx.sessionOrMinisessionOrNull()
        val documentId = ctx.pathParam("id")
        val document = gdprController.getDocument(session?.toUserRequestInfo(), documentId) ?: throw NotFoundResponse("Document $documentId not found")

        ctx.status(HttpStatus.OK_200)
        ctx.json(document)
    }

    @OpenApi(
        summary = "Release GDPR(DSGVO) document and set it as latest (admin permission required)",
        requestBody = OpenApiRequestBody([OpenApiContent(CreateGdprDocumentRequest::class)]),
        responses = [OpenApiResponse("201"), OpenApiResponse("400"), OpenApiResponse("401"), OpenApiResponse("403")],
        path = "/v1/gdpr/documents/",
        methods = [HttpMethod.POST],
    )
    private fun handlePublishDocument(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val reqBody = ctx.bodyAsClass<CreateGdprDocumentRequest>()

        val result =
            gdprController.createDocument(
                session.toUserRequestInfo(),
                GdprDocument(0, Instant.ofEpochSecond(0), reqBody.forceDate, reqBody.validDate, reqBody.content),
            )

        ctx.status(HttpStatus.OK_200)
        ctx.json(result)
    }

    private data class GdprAcceptanceAnswer(
        val userId: Int,
        val username: String,
        val fullName: String,
        val mail: String,
        val timestamp: Instant,
        val type: GdprAcceptanceType,
    )

    private fun GdprAcceptance.toGdprAcceptanceAnswer() =
        GdprAcceptanceAnswer(
            user.id,
            user.username,
            "${user.firstname} ${user.surname}",
            user.mail,
            timestamp,
            type,
        )

    private data class CreateGdprDocumentRequest(
        val content: String,
        val forceDate: Instant,
        val validDate: Instant,
    )
}
