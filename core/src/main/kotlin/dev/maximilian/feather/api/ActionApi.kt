/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.api

import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.requireAdminPermission
import dev.maximilian.feather.session
import dev.maximilian.feather.sessionOrMinisession
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.ConflictResponse
import io.javalin.http.Context

public class ActionApi(
    app: Javalin,
    private val actionController: ActionController,
    private val authorizationController: AuthorizationController,
) {
    init {
        app.routes {
            path("/actions") {
                get(::handleGetActions)
                path("/my") {
                    get(::handleGetMyActions)
                    post(::handleFinalizeMyActions)
                }
            }
        }
    }

    private fun handleGetActions(ctx: Context) {
        val session = ctx.session()
        session.user.requireAdminPermission("Get all open actions for all users")

        val actions = actionController.getOpenActions()

        ctx.json(
            actions.map {
                UserIdWithActionAnswer(it.key, it.value.map { v -> ActionAnswer(v.name, v.description) })
            },
        )
        ctx.status(200)
    }

    private fun handleGetMyActions(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val actions = actionController.getOpenUserActions(session.user)

        ctx.json(actions.map { ActionAnswer(it.name, it.description) })
        ctx.status(200)
    }

    private fun handleFinalizeMyActions(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val actions = actionController.getOpenUserActions(session.user)

        if (actions.isNotEmpty()) {
            throw ConflictResponse("Open actions")
        }

        ctx.json(authorizationController.getFinalizedRedirectUrl(session))
    }

    private data class UserIdWithActionAnswer(
        val userId: Int,
        val actions: List<ActionAnswer>,
    )

    private data class ActionAnswer(
        val name: String,
        val description: String,
    )
}
