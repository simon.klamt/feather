/*
 *    Copyright [2021] Maximilian Hippler <hello@maximilian.dev>
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.Main
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.ExternalLdapProvider
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.iog.settings.IogConfig
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.kubernetes.Kubernetes
import dev.maximilian.feather.kubernetes.OpenProjectLdapSync
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.UserCreationEvent
import dev.maximilian.feather.multiservice.events.UserDeletionEvent
import dev.maximilian.feather.util.FeatherProperties

internal class IogPluginFactory {
    companion object {
        fun create(
            backgroundJobManager: BackgroundJobManager,
            sessionOps: ISessionOperations,
            actionController: ActionController,
            accountController: AccountController,
            externalUserDeletionEvents: List<UserDeletionEvent>,
            externalUserCreationEvents: List<UserCreationEvent>,
        ): IogPlugin {
            val baseDN = (Main.ACCOUNT_CONTROLLER.externalUserProvider as ExternalLdapProvider).ldap.baseDnGroups
            val iogConfig =
                IogConfig(
                    Main.DATABASE,
                    ServiceFactory.getSupportMembershipFeature(),
                    Main.CREDENTIAL_MANAGER,
                    OPNameConfig(Main.PROPERTIES),
                    backgroundJobManager,
                    FeatherProperties.NEXTCLOUD_PUBLIC_URL,
                    Main.REDIS,
                    sessionOps,
                    OpenshiftSynchronizer(initK8sSynchonizer(), baseDN),
                    FeatherProperties.IOG_SUPPORT_MEMBERSHIP_HASH_ALGO,
                )
            return IogPlugin(
                iogConfig,
                actionController,
                accountController,
                externalUserDeletionEvents,
                externalUserCreationEvents,
            )
        }

        private fun initK8sSynchonizer(): OpenProjectLdapSync? {
            val openProjectNamespace = FeatherProperties.K8S_OPENPROJECT_NAMESPACE
            val openProjectDeployment = FeatherProperties.K8S_OPENPROJECT_DEPLOYMENT
            val openProjectContainer = FeatherProperties.K8S_OPENPROJECT_CONTAINER

            if (openProjectNamespace.trim().isBlank() ||
                openProjectDeployment.trim()
                    .isBlank() ||
                openProjectContainer.trim().isBlank()
            ) {
                return null
            }

            return OpenProjectLdapSync(Kubernetes(), openProjectNamespace, openProjectDeployment, openProjectContainer)
        }
    }
}
