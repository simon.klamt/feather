package dev.maximilian.feather.openproject

import java.awt.Dimension
import java.awt.Image
import java.awt.image.BufferedImage
import kotlin.math.min

public object ImageUtil {
    private fun getScaledDimension(imageSize: Dimension, boundary: Dimension): Dimension {
        val widthRatio = boundary.getWidth() / imageSize.getWidth()
        val heightRatio = boundary.getHeight() / imageSize.getHeight()
        val ratio = min(widthRatio, heightRatio)
        return Dimension((imageSize.width * ratio).toInt(), (imageSize.height * ratio).toInt())
    }

    public fun getToMaximumScaledImage(input: BufferedImage): BufferedImage {
        val adjustImage = input.width > 128 || input.height > 128
        val scaleDimension = getScaledDimension(Dimension(input.width, input.height), Dimension(128, 128))
        val adjustedImage = if (adjustImage) {
            input.getScaledInstance(
                scaleDimension.width,
                scaleDimension.height,
                Image.SCALE_SMOOTH,
            )
        } else {
            input
        }

        return BufferedImage(
            adjustedImage.getWidth(null),
            adjustedImage.getHeight(null),
            BufferedImage.TYPE_INT_ARGB,
        ).also { buf ->
            buf.createGraphics().also { g2d ->
                g2d.drawImage(adjustedImage, 0, 0, null)
                g2d.dispose()
            }
        }
    }
}
