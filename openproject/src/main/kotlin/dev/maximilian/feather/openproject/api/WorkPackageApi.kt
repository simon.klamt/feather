/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import dev.maximilian.feather.openproject.OpenProjectDescription
import dev.maximilian.feather.openproject.OpenProjectProject
import dev.maximilian.feather.openproject.OpenProjectStatus
import dev.maximilian.feather.openproject.OpenProjectUser
import dev.maximilian.feather.openproject.OpenProjectWorkPackage
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.request.patch
import io.ktor.client.request.setBody
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

public interface IOpenProjectWorkPackageApi {
    public suspend fun getWorkPackagesOfUser(user: OpenProjectUser, statusList: List<OpenProjectStatus>): List<OpenProjectWorkPackage>
    public suspend fun getWorkPackagesOfProject(
        project: OpenProjectProject,
        statusList: List<OpenProjectStatus>,
        includeClosed: Boolean = true,
    ): List<OpenProjectWorkPackage>
}

internal class WorkPackageApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : IOpenProjectWorkPackageApi {
    companion object {
        private fun assigneeFilter(userId: Int) = buildFilter(setOf(Triple("assignee", "=", setOf("$userId"))))

        internal fun projectWorkPackagesUrl(baseUrl: String, id: Int) =
            ProjectApi.projectUrl(baseUrl, id) + "/work_packages"
    }

    private val reqUrl = "$baseUrl/api/v3/work_packages"

    override suspend fun getWorkPackagesOfUser(
        user: OpenProjectUser,
        statusList: List<OpenProjectStatus>,
    ): List<OpenProjectWorkPackage> =
        try {
            client.getAllPages<WorkPackageAnswer>(reqUrl) {
                url {
                    parameters.append("filters", assigneeFilter(user.id))
                }
            }
                .toOpenProjectEntity(statusList.associateBy { it.id })
        }
        // OpenProject Bug
        // If the user has no proper project membership yet,
        // OpenProject returns a BadRequest with an error
        // instead of an empty list
        catch (e: ClientRequestException) {
            emptyList()
        }

    override suspend fun getWorkPackagesOfProject(
        project: OpenProjectProject,
        statusList: List<OpenProjectStatus>,
        includeClosed: Boolean,
    ): List<OpenProjectWorkPackage> = client.getAllPages<WorkPackageAnswer>(projectWorkPackagesUrl(baseUrl, project.id)) {
        if (includeClosed) {
            url {
                parameters.append("filters", "[]")
            }
        }
    }
        .toOpenProjectEntity(statusList.associateBy { it.id })

    // Here to test getWorkPackagesOfUser(), not production ready / well implemented
    internal suspend fun assignUserToWorkPackage(workPackage: OpenProjectWorkPackage, user: OpenProjectUser) {
        client.patch("$reqUrl/${workPackage.id}") {
            setBody(AssignUserToWorkPackageRequest(AssignUserToWorkPackageRequestLinks(HrefAnswer(UserApi.userUrl(baseUrl, user.id))), workPackage.lockVersion))
        }.body<Unit>()
    }
}

@Serializable
private data class AssignUserToWorkPackageRequest(
    @SerialName("_links")
    val links: AssignUserToWorkPackageRequestLinks,
    val lockVersion: Int,
)

@Serializable
private data class AssignUserToWorkPackageRequestLinks(
    val assignee: HrefAnswer,
)

@Serializable
private data class WorkPackageAnswer(
    val id: Int,
    val subject: String,
    val description: FormattedAnswer,
    val startDate: String? = null,
    val dueDate: String? = null,
    val percentageDone: Int,
    @SerialName("_links")
    val links: WorkPackageLinks,
    val lockVersion: Int,
)

@Serializable
private data class FormattedAnswer(
    val format: String,
    val raw: String?,
    val html: String,
)

@Serializable
private data class WorkPackageLinks(
    val status: HrefAnswer,
)

private fun WorkPackageAnswer.toOpenProjectEntity(statusMap: Map<Int, OpenProjectStatus>) = OpenProjectWorkPackage(
    id = id,
    subject = subject,
    description = OpenProjectDescription(
        format = description.format,
        raw = description.raw ?: "",
        html = description.html,
    ),
    startDate = startDate,
    dueDate = dueDate,
    percentageDone = percentageDone,
    status = links.status.href?.idFromURL()?.let { statusMap[it] } ?: OpenProjectStatus(-1, "unknown", true),
    // Here to test getWorkPackagesOfUser(), not production ready / well implemented
    lockVersion = lockVersion,
)

private fun Iterable<WorkPackageAnswer>.toOpenProjectEntity(statusMap: Map<Int, OpenProjectStatus>) =
    map { it.toOpenProjectEntity(statusMap) }
