#!/bin/sh

#
#    Copyright [2021] Feather development team, see AUTHORS.md
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

NEXTCLOUD_DOCKER_HOST_PORT="${OPENPROJECT_DOCKER_HOST_PORT:-8082}"

NEXTCLOUD_BASE_URL="${NEXTCLOUD_BASE_URL:-http://127.0.0.1:$NEXTCLOUD_DOCKER_HOST_PORT}"
NEXTCLOUD_HEALTH_URL="$NEXTCLOUD_BASE_URL/status.php"

NEXTCLOUD_USER="${NEXTCLOUD_USER:-ncadmin}"
NEXTCLOUD_PWD="${NEXTCLOUD_PWD:-ncadminsecret}"
