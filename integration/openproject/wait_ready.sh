#!/bin/sh

#
#    Copyright [2021] Maximilian Hippler <hello@maximilian.dev>
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

SCRIPT_DIR="$(dirname "$0")"
. "$SCRIPT_DIR/variables.sh"

op_ready() {
  curl --fail -s -o /dev/null "$OPENPROJECT_HEALTH_URL" || false
  return $?
}

set +e
RESULT=1
WAITED=0
while [ $RESULT -ne 0 ]; do
  sleep 5
  op_ready
  RESULT=$?
  WAITED=$((WAITED+5))
  echo "Waited $WAITED seconds on OpenProject to be ready"
done;
set -e
echo "OpenProject ready"
