/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.CiviCRMConstants.Companion.ACL_ACCESSIBLE
import dev.maximilian.feather.civicrm.CreateGroupResult
import dev.maximilian.feather.civicrm.entities.ACL
import dev.maximilian.feather.civicrm.entities.ACLEntityRole
import dev.maximilian.feather.civicrm.entities.OptionValue
import io.ktor.client.HttpClient
import mu.KotlinLogging

internal class CiviCRM(
    internal val client: HttpClient,
    internal val baseUrl: String,
    private val apiKey: String,
    private val siteKey: String,
    aclApi: IACLApi = ACLApi(client, baseUrl),
    aclEntityRoleApi: IACLEntityRoleApi = ACLEntityRoleApi(client, baseUrl),
    contactApi: IContactApi = ContactApi(client, baseUrl),
    groupContactApi: IGroupContactApi = GroupContactApi(client, baseUrl),
    groupApi: IGroupApi = GroupApi(client, baseUrl),
    emailApi: IEmailApi = EmailApi(client, baseUrl),
    optionGroupApi: IOptionGroupApi = OptionGroupApi(client, baseUrl),
    optionValueApi: IOptionValueApi = OptionValueApi(client, baseUrl),
    contactTypeApi: IContactTypeApi = ContactTypeApi(client, baseUrl),
    customGroupApi: ICustomGroupApi = CustomGroupApi(client, baseUrl),
    customFieldApi: ICustomFieldApi = CustomFieldApi(client, baseUrl),
    civiRulesApi: ICiviRulesApi = CiviRulesApi(client, baseUrl, apiKey, siteKey),
) : ICiviCRM,
    IACLApi by aclApi,
    IACLEntityRoleApi by aclEntityRoleApi,
    IContactApi by contactApi,
    IGroupContactApi by groupContactApi,
    IGroupApi by groupApi,
    IEmailApi by emailApi,
    IOptionGroupApi by optionGroupApi,
    IOptionValueApi by optionValueApi,
    IContactTypeApi by contactTypeApi,
    ICustomGroupApi by customGroupApi,
    ICustomFieldApi by customFieldApi,
    ICiviRulesApi by civiRulesApi {
    private val logger = KotlinLogging.logger {}

    init {
        logger.info { "CiviCRM::CiviCRM created with: $baseUrl" }
    }

    override suspend fun createOrReplaceTopLevelGroupWithACLPermission(
        name: String,
        title: String,
        roleName: String,
    ): CreateGroupResult {
        val aclRoleIndex = findIndexOfACLRole()
        val lastUsedRoleIndex = findLastRoleIndex() + 1
        val g =
            dev.maximilian.feather.civicrm.entities.Group(
                0,
                name,
                title,
                title,
                null,
                setOf(ACL_ACCESSIBLE),
            )
        val roleValue =
            OptionValue(
                1,
                aclRoleIndex,
                roleName,
                name,
                lastUsedRoleIndex.toString(),
                null,
                roleName,
                true,
            )
        getOptionValues().filter { it.name == name }.forEach { deleteOptionValue(it.id) }
        createOptionValue(roleValue)
        getGroupByName(g.name)?.let { deleteGroupWithAclAndContactDependencies(it.name) }
        val createdGroup = createGroup(g)

        val aclEntityRole =
            ACLEntityRole(0, lastUsedRoleIndex, ACLProtocol.ACLEntityType.CRM_GROUP.protocolName, createdGroup.id, true)

        createACLEntityRole(aclEntityRole)

        return CreateGroupResult(lastUsedRoleIndex, createdGroup)
    }

    override suspend fun createOrReplaceSubgroupWithACLPermission(
        name: String,
        title: String,
        roleName: String,
        topLevelRole: String,
    ): CreateGroupResult {
        val q = createOrReplaceTopLevelGroupWithACLPermission(name, title, roleName)
        getRoleIndexByName(topLevelRole)?.let { topLevelRoleIndex ->
            val aclEntityRole =
                ACLEntityRole(0, topLevelRoleIndex, ACLProtocol.ACLEntityType.CRM_GROUP.protocolName, q.group.id, true)
            createACLEntityRole(aclEntityRole)
        }

        return q
    }

    private suspend fun findLastRoleIndex(): Int {
        val aclRoleIndex = findIndexOfACLRole()
        return getOptionValueByOptionGroupId(aclRoleIndex).maxOf { it.value?.toInt() ?: 0 }
    }

    override suspend fun getRoleIndexByName(n: String): Int? {
        val aclRoleIndex = findIndexOfACLRole()
        return getOptionValueByOptionGroupId(aclRoleIndex).filter { it.name == n }.firstOrNull()?.value?.toInt()
    }

    override suspend fun deleteRoleWithAclDependencies(civiRoleNameSuffix: String) {
        val acls = getACLs()
        val aclRoleIndex = findIndexOfACLRole()
        getOptionValueByOptionGroupId(aclRoleIndex).filter { it.name?.endsWith(civiRoleNameSuffix) ?: false }.forEach { foundRole ->
            if (foundRole.value != null) {
                val foundRoleID = foundRole.value.toInt()
                deleteAclsOfRole(foundRoleID, acls)
                deleteACLEntityRole(foundRoleID)
            }
            deleteOptionValue(foundRole.id)
        }
    }

    override suspend fun deleteGroupWithAclAndContactDependencies(civiGroupName: String) {
        getGroupByName(civiGroupName)?.let { group ->
            val contactsToCheck = deleteGroupContactsOfGroup(group.id)
            contactsToCheck.forEach { deleteIfWithoutGroup(it) }
            deleteAclsOfGroup(group.id)
            deleteGroup(group.id)
        }
    }

    private suspend fun deleteIfWithoutGroup(contactID: Int) {
        if (getGroupContactsByContactId(contactID).isEmpty()) {
            getEmailByContactId(contactID)?.let { deleteEmail(it.id) }
            deleteContact(contactID)
        }
    }

    private suspend fun deleteGroupContactsOfGroup(groupId: Int): List<Int> {
        val contactsOfDeletedGroupContacts = mutableListOf<Int>()
        getGroupContactsByGroupId(groupId).forEach {
            contactsOfDeletedGroupContacts.add(it.contactId)
            deleteGroupContact(it.id)
        }
        return contactsOfDeletedGroupContacts
    }

    private suspend fun deleteAclsOfRole(
        foundRoleId: Int,
        acls: List<ACL>,
    ) {
        acls.filter { it.entityId == foundRoleId }.forEach {
            getACL(it.id)?.let { deleteACL(it.id) }
        }
    }

    private suspend fun deleteAclsOfGroup(groupId: Int) {
        getACLsByEntityId(groupId).forEach {
            deleteACL(it.id)
        }
    }
}
