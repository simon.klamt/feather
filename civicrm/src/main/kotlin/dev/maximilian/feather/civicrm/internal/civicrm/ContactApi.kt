/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.Contact
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.post
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IContactApi {
    public suspend fun getContacts(): List<Contact>

    public suspend fun getBlockedContacts(): List<Contact>

    public suspend fun getContact(userId: Int): Contact?

    public suspend fun getContactByName(
        firstname: String,
        lastname: String,
    ): Contact?

    public suspend fun blockContact(id: Int)

    public suspend fun unblockContact(id: Int)

    // public suspend fun createContact(user: Contact): Contact
    public suspend fun createContact(
        contactType: String,
        contactSubType: String?,
        firstname: String,
        lastname: String,
    ): Contact

    public suspend fun changeName(
        user: Contact,
        newDisplayName: String,
        newFirstname: String,
        newLastName: String,
    ): Contact?

    public suspend fun deleteContact(id: Int)
}

internal class ContactApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : IContactApi, IGetElement {
    private companion object : KLogging()

    private val generalAPI = GeneralAPI(client, baseUrl, "Contact", this)

    override suspend fun getContacts(): List<Contact> = generalAPI.get().body<GeneralAPI.Answer<Contact>>().values

    override suspend fun getBlockedContacts(): List<Contact> =
        generalAPI.get(
            setOf(Triple("is_deleted", "=", true)),
        ).body<GeneralAPI.Answer<Contact>>().values

    override suspend fun getContact(userId: Int): Contact? = generalAPI.get(userId).body<GeneralAPI.Answer<Contact>>().values.singleOrNull()

    override suspend fun getContactByName(
        firstname: String,
        lastname: String,
    ): Contact? =
        generalAPI.getString(
            setOf(
                Triple(
                    "first_name",
                    "=",
                    firstname,
                ),
                Triple(
                    "last_name",
                    "=",
                    lastname,
                ),
            ),
        ).body<GeneralAPI.Answer<Contact>>().values.singleOrNull()

    /*override suspend fun createContact(user: Contact): Contact {
        val s = mutableMapOf<String, JsonElement>()
        s["contact_type"] = JsonPrimitive(user.contactType)
        //user.contactSubType?.let { s["contact_sub_type"] = JsonPrimitive(user.contactSubType.joinToString()) }
        user.firstName?.let { s["first_name"] = JsonPrimitive(user.firstName) }
        user.lastName?.let { s["last_name"] = JsonPrimitive(user.lastName) }

        logger.info { "body is params=${ParamStringBuilder.value(s)}" }

        var q = generalAPI.create(s).body<GeneralAPI.Answer<Contact>>().values.single()

        if (user.deleted == true) {
            blockContact(q.id)
            q = requireNotNull(getContact(q.id)) {
                "ContactAPI::createContact could not create contact for user: ${user.firstName} ${user.lastName}"
            }
        }

        return q
    }*/

    override suspend fun createContact(
        contactType: String,
        contactSubType: String?,
        firstname: String,
        lastname: String,
    ): Contact {
        val s = mutableMapOf<String, JsonElement>()
        s["contact_type"] = JsonPrimitive(contactType)
        contactSubType?.let { s["contact_sub_type"] = JsonPrimitive(contactSubType) }
        s["first_name"] = JsonPrimitive(firstname)
        s["last_name"] = JsonPrimitive(lastname)

        logger.info { "body is params=${ParamStringBuilder.value(s)}" }

        return generalAPI.create(s).body<GeneralAPI.Answer<Contact>>().values.single()
    }

    override suspend fun changeName(
        user: Contact,
        newDisplayName: String,
        newFirstname: String,
        newLastName: String,
    ): Contact? {
        val s =
            mapOf<String, JsonElement>(
                "display_name" to JsonPrimitive(newDisplayName),
                "first_name" to JsonPrimitive(newFirstname),
                "last_name" to JsonPrimitive(newLastName),
            )

        return client.post(
            generalAPI.url(baseUrl, GeneralAPI.Action.UPDATE) +
                "?params=${ParamStringBuilder.valueWhere(s, setOf(Triple("id", "=", user.id)))}",
        )
            .body<GeneralAPI.Answer<Contact>>().values.singleOrNull()
    }

    override suspend fun deleteContact(id: Int) {
        logger.info { "CiviCRM::ContactAPI::deleteContact with id $id" }
        generalAPI.deleteWithLoop(id, "\"useTrash\":false")
    }

    override suspend fun blockContact(id: Int) {
        logger.info { "CiviCRM::ContactAPI::blockContact with id $id" }
        val s = mutableMapOf<String, JsonElement>()
        s["is_deleted"] = JsonPrimitive(true)

        logger.info { "body is params=${ParamStringBuilder.value(s)}" }
        generalAPI.update(id, s)
    }

    override suspend fun unblockContact(id: Int) {
        logger.info { "CiviCRM::ContactAPI::unblockContact with id $id" }
        val s = mutableMapOf<String, JsonElement>()
        s["is_deleted"] = JsonPrimitive(false)

        logger.info { "body is params=${ParamStringBuilder.value(s)}" }
        generalAPI.update(id, s)
    }

    override suspend fun getElement(id: Int): Any? = getContact(id)
}
