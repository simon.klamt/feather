/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

public class CiviCRMConstants {
    public companion object {
        public const val ACL_ROLE: String = "acl_role"
        public const val ACL_ACCESSIBLE: Int = 1
        public const val HIGHEST_ID_OF_DEFAULT_CONTACTS: Int = 4
        public const val HIGHEST_ID_OF_DEFAULT_EMAILS: Int = 4
    }

    public enum class ContactTypeDefaults(public val protocolName: String, public val icon: String) {
        Organization("Organization", "fa-building"),
        Individual("Individual", "fa-user"),
        Household("Household", "fa-home"),
    }
}
