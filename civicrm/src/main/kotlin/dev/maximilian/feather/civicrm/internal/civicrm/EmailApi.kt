/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.Email
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IEmailApi {
    public suspend fun getEmails(): List<Email>

    public suspend fun getEmail(emailId: Int): Email?

    public suspend fun getContactIdByEmail(email: String): Int?

    public suspend fun getEmailByContactId(contactId: Int): Email?

    public suspend fun createEmail(
        email: String,
        contactID: Int,
    ): Email

    public suspend fun updateEmail(
        id: Int,
        newEmail: String,
    ): Email?

    public suspend fun deleteEmail(id: Int)
}

internal class EmailApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : IEmailApi, IGetElement {
    private companion object : KLogging()

    private val generalApi = GeneralAPI(client, baseUrl, "Email", this)

    override suspend fun getEmails(): List<Email> = generalApi.get().body<GeneralAPI.Answer<Email>>().values

    override suspend fun getContactIdByEmail(email: String): Int? {
        val a =
            client.get(
                generalApi.url(
                    baseUrl,
                    GeneralAPI.Action.GET,
                ) + "?params=${ParamStringBuilder.where(setOf(Triple("email", "=", email)), "")}",
            ).body<GeneralAPI.Answer<Email>>().values
        return a.singleOrNull()?.contactId
    }

    override suspend fun getEmailByContactId(contactId: Int): Email? =
        client.get(
            generalApi.url(baseUrl, GeneralAPI.Action.GET) + "?params=${ParamStringBuilder.where(setOf(Triple("contact_id", "=", contactId)), "")}",
        )
            .body<GeneralAPI.Answer<Email>>().values.singleOrNull()

    override suspend fun getEmail(emailId: Int): Email? = generalApi.get(emailId).body<GeneralAPI.Answer<Email>>().values.singleOrNull()

    override suspend fun createEmail(
        email: String,
        contactID: Int,
    ): Email {
        logger.info { "CiviCRM::EmailApi::createEmail with id $email for contactID $contactID" }
        val s =
            mapOf<String, JsonElement>(
                "email" to JsonPrimitive(email),
                "contact_id" to JsonPrimitive(contactID.toString()),
            )
        return generalApi.create(s).body<GeneralAPI.Answer<Email>>().values.single()
    }

    override suspend fun updateEmail(
        id: Int,
        newEmail: String,
    ): Email? {
        generalApi.update(id, mapOf("email" to JsonPrimitive(newEmail)))
        return getEmail(id)
    }

    override suspend fun deleteEmail(id: Int) {
        logger.info { "CiviCRM::EmailApi::deleteEmail with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getEmail(id)
}
