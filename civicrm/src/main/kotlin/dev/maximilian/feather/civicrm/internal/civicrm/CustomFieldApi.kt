/*
 * Copyright [2023] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.CustomField
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface ICustomFieldApi {
    public suspend fun getCustomField(): List<CustomField>

    public suspend fun getCustomField(id: Int): CustomField?

    public suspend fun createCustomField(
        customGroupId: Int,
        name: String,
        dataType: String,
        default: String,
        label: String,
        htmlType: String,
    ): CustomField

    public suspend fun deleteCustomField(id: Int)
}

internal class CustomFieldApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : ICustomFieldApi, IGetElement {
    private companion object : KLogging()

    private val generalApi = GeneralAPI(client, baseUrl, "CustomField", this)

    override suspend fun getCustomField(): List<CustomField> = generalApi.get().body<GeneralAPI.Answer<CustomField>>().values

    override suspend fun getCustomField(id: Int): CustomField? =
        generalApi.get(
            id,
        ).body<GeneralAPI.Answer<CustomField>>().values.singleOrNull()

    override suspend fun createCustomField(
        customGroupId: Int,
        name: String,
        dataType: String,
        default: String,
        label: String,
        htmlType: String,
    ): CustomField {
        logger.info { "CiviCRM::CustomGroupApi::createCustomGroup with name $name" }
        val s =
            mapOf<String, JsonElement>(
                "custom_group_id" to JsonPrimitive(customGroupId),
                "name" to JsonPrimitive(name),
                "data_type" to JsonPrimitive(dataType),
                "default_value" to JsonPrimitive(default),
                "html_type" to JsonPrimitive(htmlType),
                "label" to JsonPrimitive(label),
            )
        return generalApi.create(s).body<GeneralAPI.Answer<CustomField>>().values.single()
    }

    override suspend fun deleteCustomField(id: Int) {
        logger.info { "CiviCRM::CustomFieldApi::deleteCustomField with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getCustomField(id)
}
