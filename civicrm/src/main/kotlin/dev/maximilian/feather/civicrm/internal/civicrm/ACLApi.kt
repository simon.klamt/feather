/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.ACL
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IACLApi {
    public suspend fun getACLs(): List<ACL>

    public suspend fun getACL(id: Int): ACL?

    public suspend fun getACLsByEntityId(roleId: Int): List<ACL>

    public suspend fun createACL(acl: ACL): ACL

    public suspend fun deleteACL(id: Int)

    public suspend fun createACL(
        name: String,
        sourceRoleIndex: Int,
        targetGroupId: Int,
        operation: ACLProtocol.Operations,
    )
}

internal class ACLApi(
    client: HttpClient,
    baseUrl: String,
) : IACLApi, IGetElement {
    private companion object : KLogging()

    private val genericApi = GeneralAPI(client, baseUrl, "ACL", this)

    override suspend fun getACLs(): List<ACL> = genericApi.get().body<GeneralAPI.Answer<ACL>>().values

    override suspend fun getACL(id: Int): ACL? = genericApi.get(id).body<GeneralAPI.Answer<ACL>>().values.singleOrNull()

    override suspend fun getACLsByEntityId(groupId: Int): List<ACL> =
        genericApi.getInt(setOf(Triple("entity_id", "=", groupId))).body<GeneralAPI.Answer<ACL>>().values

    override suspend fun createACL(acl: ACL): ACL {
        logger.info { "CiviCRM::ACLApi::createACL ${acl.name}" }
        val s = mutableMapOf<String, JsonElement>()
        // acl.aclTable?.let { s.add(Pair("acl_table", acl.aclTable)) }
        s["deny"] = JsonPrimitive(acl.deny)
        s["name"] = JsonPrimitive(acl.name)
        s["operation"] = JsonPrimitive(acl.operation)
        acl.objectTable?.let { s["object_table"] = JsonPrimitive(acl.objectTable) }
        acl.objectId?.let { s["object_id"] = JsonPrimitive(acl.objectId) }
        s["is_active"] = JsonPrimitive(acl.active)
        acl.entityId?.let { s["entity_id"] = JsonPrimitive(acl.entityId) }
        return genericApi.create(s).body<GeneralAPI.Answer<ACL>>().values.single()
    }

    override suspend fun deleteACL(id: Int) {
        logger.info { "CiviCRM::ACLApi::deleteACL with id $id" }
        genericApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getACL(id)

    override suspend fun createACL(
        name: String,
        sourceRoleIndex: Int,
        targetGroupId: Int,
        operation: ACLProtocol.Operations,
    ) {
        val acl =
            ACL(
                0,
                name,
                false,
                ACLProtocol.ACLEntityType.ACL_ROLE.protocolName,
                sourceRoleIndex,
                operation.protocolName,
                ACLProtocol.TargetType.SAVED_SEARCH.protocolName,
                targetGroupId,
                null,
                true,
            )
        createACL(acl)
    }
}
