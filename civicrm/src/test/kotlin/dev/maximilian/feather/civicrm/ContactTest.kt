/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.ContactType
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ContactTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create normal CiviCRM contact`() {
        runBlocking {
            val responseContact =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    null,
                    "Harry",
                    "Schmidt",
                )
            assertEquals("Harry", responseContact.firstName)
            assertEquals("Schmidt", responseContact.lastName)
        }
    }

    @Test
    fun `Create contact for custom type`() {
        runBlocking {
            val specialType: ContactType =
                civicrm.getContactTypes().firstOrNull { it.name == "SpecialType2" }
                    ?: civicrm.createContactType("SpecialType2", "SpecialType2", CiviCRMConstants.ContactTypeDefaults.Individual)

            val firstname = "Hubert der ${Random.nextInt()}"
            val createdContact =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    specialType.name,
                    firstname,
                    "Testnachname",
                )

            assertEquals(CiviCRMConstants.ContactTypeDefaults.Individual.protocolName, createdContact.contactType)
            assertEquals(firstname, createdContact.firstName)
            assertEquals("Testnachname", createdContact.lastName)
        }
    }

    @Test
    fun `Get contact by name`() {
        runBlocking {
            val n = civicrm.getContactByName("API", "User")
            assertEquals("API", n!!.firstName)
            assertEquals("User", n.lastName)
        }
    }

    @Test
    fun `Change contact name`() {
        runBlocking {
            val r = Random.nextInt(1000)
            val responseContact =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    null,
                    "Rüdiger$r",
                    "Müller",
                )
            civicrm.changeName(responseContact, "VVV$r", "Helmut", "Knoblauch")
            val n = civicrm.getContact(responseContact.id)
            assertEquals("Helmut", n!!.firstName)
            assertEquals("Knoblauch", n.lastName)
        }
    }

    @Test
    fun `Delete contact`() {
        runBlocking {
            val originalList = civicrm.getContacts()
            val t = civicrm.createContact(CiviCRMConstants.ContactTypeDefaults.Individual.protocolName, null, "Mary", "Schmitt")
            civicrm.deleteContact(t.id)
            val newList = civicrm.getContacts()
            assertEquals(originalList, newList, "Original contact list differs from list after create and delete")
        }
    }

    @Test
    fun `Block contact`() {
        runBlocking {
            val t = civicrm.createContact(CiviCRMConstants.ContactTypeDefaults.Individual.protocolName, null, "Marion", "Schulze")
            civicrm.blockContact(t.id)
            val newList = civicrm.getBlockedContacts()
            assertTrue(
                newList.any { it.firstName == "Marion" && it.lastName == "Schulze" },
                "Blocked contact was removed from contact list",
            )
            assertTrue(
                newList.any {
                    it.firstName == "Marion" && it.lastName == "Schulze" && it.deleted == true
                },
                "Is deleted flag is not set of contact",
            )
            civicrm.deleteContact(t.id)
        }
    }

    @Test
    fun `Unblock contact`() {
        runBlocking {
            val t = civicrm.createContact(CiviCRMConstants.ContactTypeDefaults.Individual.protocolName, null, "Marlon", "Schmitt")
            val originalList = civicrm.getContacts()
            civicrm.blockContact(t.id)
            civicrm.unblockContact(t.id)
            val newList = civicrm.getContacts()
            assertEquals(
                originalList.sortedBy {
                    it.id
                }.map {
                    Triple(it.id, it.lastName, it.deleted)
                },
                newList.sortedBy {
                    it.id
                }.map { Triple(it.id, it.lastName, it.deleted) },
                "Original contact list differs from list after create and delete",
            )
            civicrm.deleteContact(t.id)
        }
    }

    @Test
    fun `Read all contacts return at least one result`() {
        runBlocking {
            val t = civicrm.getContacts()
            assertTrue(t.isNotEmpty())
        }
    }
}
