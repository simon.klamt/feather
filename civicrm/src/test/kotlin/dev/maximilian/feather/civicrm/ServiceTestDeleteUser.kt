/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import dev.maximilian.feather.civicrm.helper.ContactSnapshot
import dev.maximilian.feather.civicrm.helper.CredentialScenario
import dev.maximilian.feather.testutils.ServiceConfig
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals

class ServiceTestDeleteUser {
    @Test
    fun `CiviCRMService delete does not change something if user does not exist in civicrm`() {
        val randomNumber = Random.nextInt()
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()

        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "delete2")

        runBlocking {
            ServiceTest.createContactTypes(a.civicrm)

            val old = ContactSnapshot(a.civicrm)

            a.deleteUser(credentialScenario.testUser)

            val new = ContactSnapshot(a.civicrm)
            credentialScenario.deleteScenario()

            assertEquals(old.contacts.count(), new.contacts.count(), "Contact number was changed")
            assertEquals(old.contacts, new.contacts, "Contacts was changed")
            assertEquals(old.groupContacts, new.groupContacts, "GroupContacts was changed")
            assertEquals(old.emails, new.emails, "Emails was changed")
            assertEquals(old.blockedContacts, new.blockedContacts, "Blocked user was changed")
        }
    }

    @Test
    fun `CiviCRMService delete really deletes`() {
        val randomNumber = Random.nextInt()
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()
        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "delete")

        runBlocking {
            ServiceTest.createContactTypes(a.civicrm)
            val civiGroup = a.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")

            val old = ContactSnapshot(a.civicrm)
            a.userAddedToGroup(credentialScenario.admin, credentialScenario.group, credentialScenario.testUser.id)
            val testUserRead = ServiceConfig.CREDENTIAL_PROVIDER.getUser(credentialScenario.testUser.id)
            a.deleteUser(testUserRead!!)

            val groupContacts = a.civicrm.getGroupContacts()
            val contacts = a.civicrm.getContacts()
            val emails = a.civicrm.getEmails()
            val blockedContacts = a.civicrm.getBlockedContacts()

            credentialScenario.deleteScenario()
            a.civicrm.deleteGroup(civiGroup.id)

            assertEquals(old.contacts.count(), contacts.count(), "Contact number was changed")
            assertEquals(old.contacts, contacts, "Contacts were changed")
            assertEquals(old.groupContacts, groupContacts, "GroupContacts were changed")
            assertEquals(old.emails, emails, "Emails were changed")
            assertEquals(old.blockedContacts, blockedContacts, "Blocked contacts were changed")
        }
    }
}
