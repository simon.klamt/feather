/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.OptionValue
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class OptionValueTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create OptionValue`() {
        runBlocking {
            val r = Random.nextInt()
            val optionValue =
                OptionValue(
                    1,
                    1,
                    "labelA$r",
                    "testnameA$r",
                    "$r",
                    null,
                    "testdescriptionA$r",
                    true,
                )
            val response = civicrm.createOptionValue(optionValue)
            assertEquals(optionValue.name, response.name)
            assertEquals(optionValue.description, response.description)
            // assertEquals(optionValue.isLocked, response.isLocked)
            assertEquals(optionValue.isActive, response.isActive)
            // assertNull(response.color)
            // assertNull(response.domainId)
        }
    }

    @Test
    fun `Get OptionValue by id`() {
        val r = Random.nextInt()
        runBlocking {
            val optionValue =
                OptionValue(
                    1,
                    1,
                    "labeBl$r",
                    "testnameB$r",
                    "$r",
                    null,
                    "testdescriptionB$r",
                    true,
                )
            val response = civicrm.createOptionValue(optionValue)
            val response2 = civicrm.getOptionValue(response.id)
            assertEquals(optionValue.name, response2!!.name)
            civicrm.deleteOptionGroup(response.id)
        }
    }

    @Test
    fun `Delete OptionValue`() {
        runBlocking {
            val r = Random.nextInt()
            val originalList = civicrm.getOptionValues()
            val optionValue =
                OptionValue(
                    1,
                    1,
                    "labelC$r",
                    "testnameC$r",
                    "$r",
                    null,
                    "testdescriptionC$r",
                    true,
                )
            val response = civicrm.createOptionValue(optionValue)
            civicrm.deleteOptionValue(response.id)
            val newList = civicrm.getOptionValues()
            assertEquals(originalList, newList, "Original Groupt list differs from list after create and delete")
        }
    }

    @Test
    fun `Read all OptionValue return at least one result`() {
        runBlocking {
            val t = civicrm.getOptionValues()
            assertTrue(t.isNotEmpty())
        }
    }
}
