dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            version("kotlin", "1.9.21")
            version("git-version", "3.0.0")

            version("kotlinx-coroutines", "1.7.3")
            version("slf4j", "2.0.9")
            version("microutils", "3.0.5")
            version("junit", "5.10.1")
            version("exposed", "0.45.0")
            version("javalin", "5.6.3")
            version("unirest", "4.2.0")
            version("oidc", "11.7")
            version("jedis", "5.1.0")
            version("directory", "2.1.5")
            version("mustache", "0.9.11")
            version("h2", "2.2.224")
            version("postgres", "42.7.0")
            version("swagger-core", "2.2.19")
            version("swagger-ui", "5.10.3")
            version("jackson", "2.16.0")
            version("ktor", "2.3.6")
            version("jgrapht", "1.5.2")
            version("micrometer", "1.12.0")
            version("jsoup", "1.17.1")
            version("sardine", "5.12")
            version("kubernetes-client", "6.9.2")
            version("mail", "2.0.1")
            version("semver", "1.4.2")

            plugin("kotlin-jvm", "org.jetbrains.kotlin.jvm").versionRef("kotlin")
            plugin("kotlin-kapt", "org.jetbrains.kotlin.kapt").versionRef("kotlin")
            plugin("git-version", "com.palantir.git-version").versionRef("git-version")
            plugin("kotlin-serialization", "org.jetbrains.kotlin.plugin.serialization").versionRef("kotlin")

            library("slf4j-api", "org.slf4j", "slf4j-api").versionRef("slf4j")
            library("slf4j-simple", "org.slf4j", "slf4j-simple").versionRef("slf4j")
            library("slf4j-log4j", "org.slf4j", "log4j-over-slf4j").versionRef("slf4j")

            library("junit-core", "org.junit.jupiter", "junit-jupiter").versionRef("junit")
            library("junit-engine", "org.junit.jupiter", "junit-jupiter-engine").versionRef("junit")

            library("exposed-core", "org.jetbrains.exposed", "exposed-core").versionRef("exposed")
            library("exposed-dao", "org.jetbrains.exposed", "exposed-dao").versionRef("exposed")
            library("exposed-jdbc", "org.jetbrains.exposed", "exposed-jdbc").versionRef("exposed")
            library("exposed-time", "org.jetbrains.exposed", "exposed-java-time").versionRef("exposed")
            bundle("exposed", listOf("exposed-core", "exposed-dao", "exposed-jdbc", "exposed-time"))

            library("javalin-core", "io.javalin", "javalin").versionRef("javalin")
            library("javalin-micrometer", "io.javalin", "javalin-micrometer").versionRef("javalin")
            library("javalin-openapi-core", "io.javalin.community.openapi", "javalin-openapi-plugin").versionRef("javalin")
            library("javalin-openapi-kapt", "io.javalin.community.openapi", "openapi-annotation-processor").versionRef("javalin")
            library("javalin-openapi-swagger", "io.javalin.community.openapi", "javalin-swagger-plugin").versionRef("javalin")
            library("javalin-openapi-redoc", "io.javalin.community.openapi", "javalin-redoc-plugin").versionRef("javalin")

            bundle(
                "javalin",
                listOf("javalin-core", "javalin-micrometer", "javalin-openapi-core", "javalin-openapi-swagger", "javalin-openapi-redoc"),
            )

            library("unirest-core", "com.konghq", "unirest-java-core").versionRef("unirest")
            library("unirest-jackson", "com.konghq", "unirest-objectmapper-jackson").versionRef("unirest")
            bundle("unirest", listOf("unirest-core", "unirest-jackson"))

            library("jackson-databind", "com.fasterxml.jackson.core", "jackson-databind").versionRef("jackson")
            library("jackson-kotlin", "com.fasterxml.jackson.module", "jackson-module-kotlin").versionRef("jackson")
            library("jackson-jsr310", "com.fasterxml.jackson.datatype", "jackson-datatype-jsr310").versionRef("jackson")
            bundle("jackson", listOf("jackson-databind", "jackson-kotlin", "jackson-jsr310"))

            bundle(
                "jacksonAndUnirest",
                listOf("unirest-core", "unirest-jackson") + listOf("jackson-databind", "jackson-kotlin", "jackson-jsr310"),
            )

            library("ktor-client-core", "io.ktor", "ktor-client-core").versionRef("ktor")
            library("ktor-client-cio", "io.ktor", "ktor-client-cio").versionRef("ktor")
            library("ktor-client-contentNegotiation", "io.ktor", "ktor-client-content-negotiation").versionRef("ktor")
            library("ktor-client-kotlinxJson", "io.ktor", "ktor-serialization-kotlinx-json").versionRef("ktor")
            library("ktor-client-auth", "io.ktor", "ktor-client-auth").versionRef("ktor")
            library("ktor-client-logging", "io.ktor", "ktor-client-logging").versionRef("ktor")

            bundle(
                "ktor-client",
                listOf(
                    "ktor-client-core",
                    "ktor-client-cio",
                    "ktor-client-contentNegotiation",
                    "ktor-client-kotlinxJson",
                    "ktor-client-auth",
                    "ktor-client-logging",
                ),
            )

            library("ktor-server-core", "io.ktor", "ktor-server-core").versionRef("ktor")
            library("ktor-server-netty", "io.ktor", "ktor-server-netty").versionRef("ktor")

            bundle(
                "ktor-server",
                listOf(
                    "ktor-server-core",
                    "ktor-server-netty",
                ),
            )

            library("micrometer-core", "io.micrometer", "micrometer-core").versionRef("micrometer")
            library("micrometer-prometheus", "io.micrometer", "micrometer-registry-prometheus").versionRef("micrometer")
            bundle("micrometer", listOf("micrometer-core", "micrometer-prometheus"))

            library("kotlinx-coroutines", "org.jetbrains.kotlinx", "kotlinx-coroutines-core").versionRef("kotlinx-coroutines")
            library("kotlin-junit", "org.jetbrains.kotlin", "kotlin-test-junit5").versionRef("kotlin")

            library("microutils", "io.github.microutils", "kotlin-logging-jvm").versionRef("microutils")
            library("oidc", "com.nimbusds", "oauth2-oidc-sdk").versionRef("oidc")
            library("jedis", "redis.clients", "jedis").versionRef("jedis")
            library("directory", "org.apache.directory.api", "api-all").versionRef("directory")
            library("mustache", "com.github.spullara.mustache.java", "compiler").versionRef("mustache")
            library("h2", "com.h2database", "h2").versionRef("h2")
            library("postgres", "org.postgresql", "postgresql").versionRef("postgres")
            library("swagger-core", "io.swagger.core.v3", "swagger-core").versionRef("swagger-core")
            library("swagger-ui", "org.webjars", "swagger-ui").versionRef("swagger-ui")
            library("jgrapht", "org.jgrapht", "jgrapht-core").versionRef("jgrapht")
            library("jsoup", "org.jsoup", "jsoup").versionRef("jsoup")
            library("sardine", "com.github.lookfirst", "sardine").versionRef("sardine")
            library("kubernetes", "io.fabric8", "kubernetes-client").versionRef("kubernetes-client")
            library("mail", "com.sun.mail", "jakarta.mail").versionRef("mail")
            library("semver", "io.github.z4kn4fein", "semver").versionRef("semver")
        }
    }
}

rootProject.name = "feather"
include("lib")
include("test_utils")
include("core")
include("iog")
include("nextcloud")
include("openproject")
include("kubernetes")
include("authorization")
include("multiservice")
include("keycloak-actions")
include("civicrm")
