/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.authorization.internals.RedisCacheAdapter
import redis.clients.jedis.JedisPool

class CachedCredentialProvider(
    pool: JedisPool,
    private val credentialProvider: ICredentialProvider,
) : ICredentialProvider by credentialProvider {
    private val cacheAdapter = RedisCacheAdapter(pool, credentialProvider)

    override fun getUser(id: Int): User? = cacheAdapter.getUser(id)

    override fun getUsers(): Collection<User> = cacheAdapter.getUsers()

    override fun createUser(
        user: User,
        password: String?,
    ): User =
        credentialProvider.createUser(user, password).also {
            cacheAdapter.refreshCache()
        }

    override fun deleteUser(user: User) {
        credentialProvider.deleteUser(user)
        cacheAdapter.refreshCache()
    }

    override fun updateUser(user: User): User? =
        credentialProvider.updateUser(user).also {
            cacheAdapter.refreshCache()
        }

    override fun updateLastLoginOf(user: User): User? =
        credentialProvider.updateLastLoginOf(user).also {
            cacheAdapter.refreshCache()
        }

    override fun getGroup(id: Int): Group? = cacheAdapter.getGroup(id)

    override fun getGroupByName(groupname: String): Group? = cacheAdapter.getGroupByName(groupname)

    override fun getGroups(): Collection<Group> = cacheAdapter.getGroups()

    override fun createGroup(group: Group): Group =
        credentialProvider.createGroup(group).also {
            cacheAdapter.refreshCache()
        }

    override fun deleteGroup(group: Group) {
        credentialProvider.deleteGroup(group)
        cacheAdapter.refreshCache()
    }

    override fun updateGroup(group: Group): Group? =
        credentialProvider.updateGroup(group).also {
            cacheAdapter.refreshCache()
        }

    override fun getUsersAndGroups(): Pair<Collection<User>, Collection<Group>> = Pair(cacheAdapter.getUsers(), cacheAdapter.getGroups())

    override fun close() {
        credentialProvider.close()
        cacheAdapter.close()
    }

    override fun getUserByMail(mail: String): User? = cacheAdapter.getUserByMail(mail)

    override fun getUserByUsername(username: String): User? = cacheAdapter.getUserByUsername(username)
}
