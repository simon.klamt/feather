/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.internals.ldap

import dev.maximilian.feather.authorization.ILdapProvider
import java.time.Instant

internal sealed class LdapEntity<T : LdapEntity<T>> {
    abstract val dn: String
    abstract val entryUUID: String
    abstract val rdn: String

    open fun className(ldap: ILdapProvider): String? = this::class.simpleName
}

internal data class InetOrgPerson(
    override val dn: String,
    override val entryUUID: String,
    override val rdn: String,
    val sn: String,
    val mail: Set<String>? = null,
    val givenName: String? = null,
    val displayName: String? = null,
    val employeeNumber: String? = null,
    val userPassword: String? = null,
    val createTimestamp: Instant = Instant.ofEpochSecond(0),
    val cn: String? = null,
    val jpegPhoto: ByteArray? = null,
) : LdapEntity<InetOrgPerson>() {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as InetOrgPerson

        if (dn != other.dn) return false
        if (entryUUID != other.entryUUID) return false
        if (rdn != other.rdn) return false
        if (sn != other.sn) return false
        if (mail != other.mail) return false
        if (givenName != other.givenName) return false
        if (displayName != other.displayName) return false
        if (employeeNumber != other.employeeNumber) return false
        if (userPassword != other.userPassword) return false
        if (createTimestamp != other.createTimestamp) return false
        if (cn != other.cn) return false
        if (jpegPhoto != null) {
            if (other.jpegPhoto == null) return false
            if (!jpegPhoto.contentEquals(other.jpegPhoto)) return false
        } else if (other.jpegPhoto != null) {
            return false
        }

        return true
    }

    override fun hashCode(): Int {
        var result = dn.hashCode()
        result = 31 * result + entryUUID.hashCode()
        result = 31 * result + rdn.hashCode()
        result = 31 * result + sn.hashCode()
        result = 31 * result + (mail?.hashCode() ?: 0)
        result = 31 * result + (givenName?.hashCode() ?: 0)
        result = 31 * result + (displayName?.hashCode() ?: 0)
        result = 31 * result + (employeeNumber?.hashCode() ?: 0)
        result = 31 * result + (userPassword?.hashCode() ?: 0)
        result = 31 * result + createTimestamp.hashCode()
        result = 31 * result + (cn?.hashCode() ?: 0)
        result = 31 * result + (jpegPhoto?.contentHashCode() ?: 0)
        return result
    }
}

internal data class OrganizationalUnit(
    override val dn: String,
    override val entryUUID: String,
    override val rdn: String,
) : LdapEntity<OrganizationalUnit>()

internal data class GroupOfNames(
    override val dn: String,
    override val entryUUID: String,
    override val rdn: String,
    val member: Set<String>,
    val owner: Set<String>,
    val description: String?,
) : LdapEntity<GroupOfNames>() {
    override fun className(ldap: ILdapProvider): String = if (ldap.uniqueGroups) "groupOfUniqueNames" else "groupOfNames"
}
