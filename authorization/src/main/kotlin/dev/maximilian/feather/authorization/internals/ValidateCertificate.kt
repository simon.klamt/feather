/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.internals

import dev.maximilian.feather.authorization.LdapProviderFactory
import java.io.ByteArrayInputStream
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.Base64

internal fun validateCertificate(unvalidated: String): X509Certificate? {
    val trimmedWithoutPrefixSuffix =
        unvalidated
            .trim()
            .removePrefix("-----BEGIN CERTIFICATE-----")
            .removeSuffix("-----END CERTIFICATE-----")
            .replace("\r", "")
            .replace("\n", "")
            .takeIf { it.isNotBlank() } ?: return null

    val decoded: ByteArray =
        runCatching { Base64.getDecoder().decode(trimmedWithoutPrefixSuffix) }.onFailure {
            LdapProviderFactory.logger.warn(it) { "Base64 decoding of LDAP certificate failed, ignoring it" }
        }.getOrNull() ?: return null

    val certificateFactory =
        runCatching { CertificateFactory.getInstance("X.509") }.onFailure {
            LdapProviderFactory.logger.warn(it) { "No X.509 certificate factory found, ignoring LDAP certificate" }
        }.getOrNull() ?: return null

    return runCatching { certificateFactory.generateCertificate(ByteArrayInputStream(decoded)) }.onFailure {
        LdapProviderFactory.logger.warn(it) { "The provided LDAP X.509 certificate cannot be parsed, ignoring it" }
    }.getOrNull() as X509Certificate?
}
