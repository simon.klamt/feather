/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.internals.ldap

import dev.maximilian.feather.Permission
import dev.maximilian.feather.authorization.ILdapProvider
import mu.KotlinLogging
import org.apache.directory.api.ldap.model.constants.SchemaConstants
import org.apache.directory.api.ldap.model.entry.Entry
import org.apache.directory.api.ldap.model.entry.Modification
import org.apache.directory.api.ldap.model.exception.LdapEntryAlreadyExistsException
import org.apache.directory.api.ldap.model.message.SearchScope
import org.apache.directory.api.ldap.model.name.Dn
import org.apache.directory.ldap.client.api.LdapNetworkConnection
import java.util.Locale
import javax.net.ssl.TrustManager

internal class Ldap(
    private val host: String,
    private val port: Int,
    private val useTls: Boolean,
    private val trustManagers: Set<TrustManager>?,
    private val bindDn: String?,
    private val bindPassword: String?,
    override val baseDn: String,
    override val baseDnUsers: String,
    override val baseDnGroups: String,
    override val baseDnPermissions: String,
    override val rdnUser: String,
    override val rdnGroup: String,
    override val uniqueGroups: Boolean,
    override val plainTextPasswords: Boolean,
) :
    ILdapProvider {
    private val logger = KotlinLogging.logger { }

    private val connection: LdapNetworkConnection =
        LdapNetworkConnection(host, port, useTls).apply {
            if (trustManagers != null && trustManagers.isNotEmpty()) {
                config.setTrustManagers(*trustManagers.toTypedArray())
            }
        }

    private val dummyAccount by lazy {
        ensureConnected()
        createDummyAccountIfNotExists()
    }

    override val uuidType: String =
        when {
            dummyAccount.containsAttribute(SchemaConstants.ENTRY_UUID_AT) -> SchemaConstants.ENTRY_UUID_AT
            dummyAccount.containsAttribute("nsUniqueId") -> "nsUniqueId"
            else -> throw IllegalStateException(
                "No entry uuid attribute found",
            )
        }

    init {
        ensureConnected()

        disposeOnException {
            // connection.schemaManager.

            createOrganizationalUnitIfNotExists(baseDnGroups, "Group")
            createOrganizationalUnitIfNotExists(baseDnUsers, "User")
            createOrganizationalUnitIfNotExists(baseDnPermissions, "Permission")

            Permission.values().map { it.name.lowercase(Locale.getDefault()) }.associateWith { "cn=$it,$baseDnPermissions" }.forEach {
                if (searchOneEntry(it.value) == null) {
                    logger.warn { "Permission \"${it.key.uppercase(Locale.getDefault())}\" not found, creating it" }
                    create(
                        GroupOfNames(
                            it.value,
                            "",
                            it.key.lowercase(Locale.getDefault()),
                            setOf(dummyAccountDn),
                            emptySet(),
                            null,
                        ).toLdapEntry(this),
                    )
                }
            }

            // TODO check that dummy user is member and owner of each group (also in permission groups)
            // IF not, autofix

            Runtime.getRuntime().addShutdownHook(
                Thread {
                    close()
                },
            )
        }
    }

    private fun createDummyAccountIfNotExists(): Entry =
        searchOneEntry(dummyAccountDn) ?: run {
            logger.warn { "Dummy Account not existing, creating it" }
            create(
                InetOrgPerson(
                    dummyAccountDn,
                    "",
                    "dummy",
                    "dummy",
                    displayName = "Dummyaccount",
                ).toLdapEntry(this),
            )
        }

    private fun createOrganizationalUnitIfNotExists(
        dn: String,
        name: String,
    ) {
        if (searchOneEntry(dn) == null) {
            logger.warn {
                "${name.lowercase(
                    Locale.getDefault(),
                ).replaceFirstChar { it.titlecase(Locale.getDefault()) }} base dn \"$dn\" not found, creating it"
            }
            requireNotNull(searchOneEntry(Dn(dn).parent.toString())) {
                "Cannot create ${name.lowercase(Locale.getDefault())} base dn, parent is not existing and cannot be inferred"
            }
            create(
                OrganizationalUnit(
                    dn,
                    "",
                    Dn(dn).rdn.value,
                ).toLdapEntry(this),
            )
        }
    }

    override fun reconnect(): Unit =
        disposeOnException {
            close()
            ensureConnected()
        }

    @Synchronized
    private fun ensureConnected() {
        if (!connection.isConnected) {
            logger.info { "Connecting to ldap server $host:$port, useSSL: $useTls" }

            connection.connect()

            disposeOnException {
                when {
                    bindDn.isNullOrBlank() -> {
                        logger.info { "Binding to ldap server as anonymous user" }
                        connection.anonymousBind()
                    }
                    bindPassword.isNullOrBlank() -> {
                        logger.info { "Binding to ldap server as \"$bindDn\" without password" }
                        connection.bind(bindDn)
                    }
                    else -> {
                        logger.info { "Binding to ldap server as \"$bindDn\" with password" }
                        connection.bind(bindDn, bindPassword)
                    }
                }

                requireNotNull(searchOneEntry(baseDn)) { "Ldap basedn not found" }
                logger.info { "Connected to ldap server" }

                check(connection.isConnected) { "Connection established, but directly broken" }
            }
        }
    }

    override fun searchEntries(
        dn: String,
        filter: String,
        scope: SearchScope,
        attributeList: Array<String>,
    ): List<Entry> =
        disposeOnException {
            ensureConnected()
            connection.search(dn, filter, scope, *attributeList).use { it.toList() }
        }

    override fun create(entry: Entry): Entry =
        disposeOnException {
            ensureConnected()
            connection.add(
                entry.apply {
                    setOf("entryUUID", "nsUniqueId", "createTimestamp").forEach {
                        if (this.containsAttribute(it)) this.removeAttributes(it)
                    }
                },
            )
            searchOneEntry(entry.dn.toString())!!
        }

    override fun delete(entry: Entry) {
        connection.delete(entry.dn)
    }

    override fun modify(
        dn: String,
        vararg modifications: Modification,
    ) = disposeOnException {
        ensureConnected()
        connection.modify(dn, *modifications)
    }

    override fun getUserPermissions(userDn: String): Set<Permission> =
        disposeOnException {
            ensureConnected()
            searchEntries(baseDnPermissions, "(member=$userDn)", SearchScope.ONELEVEL, SchemaConstants.ENTRY_DN_AT)
                .map { it.dn.rdn.value }
                .mapNotNull { dn -> Permission.values().firstOrNull { it.name.equals(dn, true) } }
                .toSet()
        }

    @Synchronized
    override fun close() {
        if (connection.isConnected) {
            connection.unBind()
            connection.close()
            logger.info { "Connection to ldap server closed" }
        }
    }

    override fun rename(
        dn: String,
        newRdnValue: String,
    ) = disposeOnException {
        ensureConnected()
        try {
            connection.rename(dn, newRdnValue)
        } catch (e: LdapEntryAlreadyExistsException) {
            throw IllegalArgumentException("Entity already exists with that name in ldap", e)
        }
    }

    override fun bind(
        dn: String,
        password: String,
    ) = kotlin.runCatching {
        LdapNetworkConnection(host, port, useTls).use {
            if (trustManagers != null && trustManagers.isNotEmpty()) {
                it.config.setTrustManagers(*trustManagers.toTypedArray())
            }

            it.connect()
            it.bind(dn, password)
        }
    }.isSuccess

    private fun <T> disposeOnException(block: () -> T): T =
        try {
            block()
        } catch (t: Throwable) {
            connection.close()
            throw t
        }
}
