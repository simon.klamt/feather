package dev.maximilian.feather.authorization

import io.javalin.http.Context
import jakarta.servlet.http.HttpServletResponse
import java.util.Locale

internal fun Context.cookie(
    name: String,
    value: String,
    maxAge: Int = -1,
    httpOnly: Boolean = true,
    secure: Boolean = true,
    sameSite: SameSite = if (secure) SameSite.NONE else SameSite.LAX,
) = res().addCookie(name, value, maxAge, httpOnly, secure, sameSite)

internal fun HttpServletResponse.addCookie(
    name: String,
    value: String,
    maxAge: Int = -1,
    httpOnly: Boolean = true,
    secure: Boolean = true,
    sameSite: SameSite = if (secure) SameSite.NONE else SameSite.LAX,
) {
    val cookieParts = mutableSetOf("$name=$value", "Path=/")

    if (maxAge > 0) {
        cookieParts.add("Max-Age=$maxAge")
    }

    if (httpOnly) {
        cookieParts.add("HttpOnly")
    }

    if (secure) {
        cookieParts.add("Secure")
    }

    cookieParts.add(
        "SameSite=${
            sameSite.toString()
                .lowercase(Locale.getDefault())
                .replaceFirstChar { it.titlecase(Locale.getDefault()) }
        }",
    )

    addHeader("Set-Cookie", cookieParts.joinToString(separator = "; "))
}

internal enum class SameSite {
    NONE,
    STRICT,
    LAX,
}
