/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.ExternalGroup
import dev.maximilian.feather.ExternalUser
import dev.maximilian.feather.IExternalCredentialProvider
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull

abstract class ExternalProviderTestBase(private val externalCredentialProvider: IExternalCredentialProvider) {
    @Test
    fun `Test get not existing user`() {
        assertNull(externalCredentialProvider.getUserById(UUID.randomUUID().toString()))
    }

    @Test
    fun `Test create simple user`() {
        val user = createTestExternalUser()
        val createdUser = externalCredentialProvider.createUser(user)
        val freshUser = externalCredentialProvider.getUserById(createdUser.externalId)

        assertEquals(user.copy(externalId = createdUser.externalId, registeredSince = createdUser.registeredSince), createdUser)
        assertEquals(createdUser, freshUser)
    }

    @Test
    fun `Test create user works`() {
        // TODO extend by testing that disabled flag is checked
        val memberGroup = externalCredentialProvider.createGroup(createTestExternalGroup())
        val ownedGroup = externalCredentialProvider.createGroup(createTestExternalGroup())
        val user = createTestExternalUser(groups = setOf(memberGroup.externalId), ownedGroups = setOf(ownedGroup.externalId))

        val createdUser = externalCredentialProvider.createUser(user)
        val freshUser = externalCredentialProvider.getUserById(createdUser.externalId)

        assertNotNull(freshUser)
        assert(createdUser.externalId.isNotBlank()) { "User was created with a blank id" }
        assertEquals(
            user.copy(
                externalId = createdUser.externalId,
                registeredSince = createdUser.registeredSince,
                groups = setOf(ownedGroup.externalId, memberGroup.externalId),
            ),
            createdUser,
        )
        assert(
            Instant.now().truncatedTo(ChronoUnit.MINUTES)
                .minusSeconds(180) <= createdUser.registeredSince && createdUser.registeredSince <= Instant.now(),
        )
        assertEquals(createdUser, freshUser)
        assertEquals(setOf(createdUser.externalId), externalCredentialProvider.getGroupById(memberGroup.externalId)?.userMembers)
        assertEquals(setOf(createdUser.externalId), externalCredentialProvider.getGroupById(ownedGroup.externalId)?.owners)
    }

    @Test
    fun `Test authenticate user works`() {
        val password = UUID.randomUUID().toString()
        val user = createTestExternalUser()
        val created = externalCredentialProvider.createUser(user, password)

        val correctAuthenticatedByUsername =
            externalCredentialProvider.authenticateUserByUsernameOrMail(created.username, password)
        val correctAuthenticatedByMail =
            externalCredentialProvider.authenticateUserByUsernameOrMail(created.mail, password)
        val failAuthenticatedByUsername =
            externalCredentialProvider.authenticateUserByUsernameOrMail(created.username, password + "broken")
        val failAuthenticatedByMail =
            externalCredentialProvider.authenticateUserByUsernameOrMail(created.mail, password + "broken")

        assertEquals(created.externalId, correctAuthenticatedByUsername)
        assertEquals(created.externalId, correctAuthenticatedByMail)
        assertNull(failAuthenticatedByUsername)
        assertNull(failAuthenticatedByMail)
    }

    @Test
    fun `Test update password works`() {
        // One time without any password
        val created1 = externalCredentialProvider.createUser(createTestExternalUser())
        val updatedPassword1 = UUID.randomUUID().toString()

        assert(externalCredentialProvider.updateUserPassword(created1.externalId, updatedPassword1))
        assertEquals(
            created1.externalId,
            externalCredentialProvider.authenticateUserByUsernameOrMail(created1.username, updatedPassword1),
        )

        // And the second time with an initial password
        val created2 = externalCredentialProvider.createUser(createTestExternalUser(), UUID.randomUUID().toString())
        val updatedPassword2 = UUID.randomUUID().toString()

        assert(externalCredentialProvider.updateUserPassword(created2.externalId, updatedPassword2))
        assertEquals(
            created2.externalId,
            externalCredentialProvider.authenticateUserByUsernameOrMail(created2.username, updatedPassword2),
        )

        // Try to update password of deleted user
        externalCredentialProvider.deleteUser(created2.externalId)
        assertFalse(externalCredentialProvider.updateUserPassword(created2.externalId, UUID.randomUUID().toString()))
    }

    @Test
    fun `Test update user works`() {
        // TODO extend by testing that disabled flag is checked

        val user =
            externalCredentialProvider.createUser(
                createTestExternalUser(
                    groups = setOf(externalCredentialProvider.createGroup(createTestExternalGroup()).externalId),
                    ownedGroups = setOf(externalCredentialProvider.createGroup(createTestExternalGroup()).externalId),
                ),
            )

        val memberGroup = externalCredentialProvider.createGroup(createTestExternalGroup())
        val ownedGroup = externalCredentialProvider.createGroup(createTestExternalGroup())

        val toUpdate =
            user.copy(
                username = UUID.randomUUID().toString(),
                displayName = UUID.randomUUID().toString(),
                firstname = UUID.randomUUID().toString(),
                surname = UUID.randomUUID().toString(),
                mail = "${UUID.randomUUID()}@example.org",
                groups = setOf(memberGroup.externalId),
                ownedGroups = setOf(ownedGroup.externalId),
                registeredSince = Instant.ofEpochSecond(180),
                disabled = false,
            )

        val updatedUser = externalCredentialProvider.updateUser(toUpdate)

        assertNotNull(updatedUser)
        assertEquals(
            toUpdate.copy(
                registeredSince = user.registeredSince,
                groups = setOf(memberGroup.externalId, ownedGroup.externalId),
            ),
            updatedUser,
        )

        val freshUser = externalCredentialProvider.getUserById(updatedUser.externalId)
        assertEquals(updatedUser, freshUser)

        // Check old refs cleared
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(user.groups.first())?.userMembers)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(user.ownedGroups.first())?.owners)

        // Check new refs created
        assertEquals(setOf(updatedUser.externalId), externalCredentialProvider.getGroupById(memberGroup.externalId)?.userMembers)
        assertEquals(setOf(updatedUser.externalId), externalCredentialProvider.getGroupById(ownedGroup.externalId)?.owners)

        externalCredentialProvider.deleteUser(updatedUser.externalId)
        assertNull(externalCredentialProvider.updateUser(toUpdate.copy(displayName = UUID.randomUUID().toString())))
    }

    @Test
    fun `Test delete user works`() {
        val createdUser = externalCredentialProvider.createUser(createTestExternalUser())
        externalCredentialProvider.createGroup(createTestExternalGroup())
        val group2 =
            externalCredentialProvider.createGroup(createTestExternalGroup(userMembers = setOf(createdUser.externalId)))
        val group3 =
            externalCredentialProvider.createGroup(
                createTestExternalGroup(
                    userMembers = setOf(createdUser.externalId),
                    owners = setOf(createdUser.externalId),
                ),
            )

        val freshUser = externalCredentialProvider.getUserById(createdUser.externalId)

        assertNotNull(freshUser)
        assertEquals(setOf(group2.externalId, group3.externalId), freshUser.groups)
        assertEquals(setOf(group3.externalId), freshUser.ownedGroups)
        // Check also that any references are cleared by recreating the user with the same data,
        // but no groups and permissions
        externalCredentialProvider.deleteUser(createdUser.externalId)

        // Check old refs cleared
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(group2.externalId)?.userMembers)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(group3.externalId)?.owners)

        assertNull(externalCredentialProvider.getUserById(createdUser.externalId))
        val recreatedUser = externalCredentialProvider.createUser(createdUser.copy(externalId = ""), null)

        val freshAfterDelete = externalCredentialProvider.getUserById(recreatedUser.externalId)

        assertNotNull(freshAfterDelete)
        assertEquals(emptySet(), freshAfterDelete.groups)
        assertEquals(emptySet(), freshAfterDelete.ownedGroups)
    }

    @Test
    fun `Test update user image works`() {
        val user = externalCredentialProvider.createUser(createTestExternalUser())
        val image: ByteArray = Random.nextBytes(10)
        externalCredentialProvider.updatePhotoForUser(user.externalId, image)

        assertContentEquals(externalCredentialProvider.getPhotoForUser(user.externalId), image)
    }

    @Test
    fun `Test update 'Nothing to update' user`() {
        val testGroups = (0 until 3).map { externalCredentialProvider.createGroup(createTestExternalGroup()) }
        val user =
            externalCredentialProvider.createUser(
                createTestExternalUser().copy(
                    groups = setOf(testGroups[0].externalId, testGroups[2].externalId),
                    ownedGroups = setOf(testGroups[2].externalId),
                ),
            )

        val updatedUser = externalCredentialProvider.updateUser(user)
        val getUser = externalCredentialProvider.getUserById(user.externalId)

        assertEquals(user, updatedUser)
        assertEquals(updatedUser, getUser)
    }

    @Test
    fun `Test get not existing group`() {
        assertNull(externalCredentialProvider.getGroupById(UUID.randomUUID().toString()))
    }

    @Test
    fun `Test create simple group`() {
        val group = createTestExternalGroup()
        val createdGroup = externalCredentialProvider.createGroup(group)
        val freshGroup = externalCredentialProvider.getGroupById(createdGroup.externalId)

        assertEquals(group.copy(externalId = createdGroup.externalId), createdGroup)
        assertEquals(createdGroup, freshGroup)
    }

    @Test
    fun `Test create group`() {
        val user1 = externalCredentialProvider.createUser(createTestExternalUser(), UUID.randomUUID().toString())
        val user2 = externalCredentialProvider.createUser(createTestExternalUser(), UUID.randomUUID().toString())

        val ownerGroup =
            externalCredentialProvider.createGroup(createTestExternalGroup())
        val ownedGroup =
            externalCredentialProvider.createGroup(createTestExternalGroup())

        val memberGroup =
            externalCredentialProvider.createGroup(createTestExternalGroup())
        val parentGroup =
            externalCredentialProvider.createGroup(createTestExternalGroup())

        val toCreate =
            createTestExternalGroup(
                id = "0",
                userMembers = setOf(user1.externalId),
                owners = setOf(user2.externalId),
                ownerGroups = setOf(ownerGroup.externalId),
                ownedGroups = setOf(ownedGroup.externalId),
                groupMembers = setOf(memberGroup.externalId),
                parentGroups = setOf(parentGroup.externalId),
            )
        val created = externalCredentialProvider.createGroup(toCreate)
        val fresh = externalCredentialProvider.getGroupById(created.externalId)

        // Every group a user/group is admin, the entity is also member
        val expectedUser =
            toCreate.copy(
                externalId = created.externalId,
                userMembers = setOf(user1.externalId, user2.externalId),
                groupMembers = setOf(memberGroup.externalId, ownerGroup.externalId),
                parentGroups = setOf(parentGroup.externalId, ownedGroup.externalId),
            )

        assert(created.externalId != "0")
        assertEquals(expectedUser, created)
        assertEquals(created, fresh)

        // Check that references are set
        assertEquals(setOf(created.externalId), externalCredentialProvider.getUserById(user1.externalId)?.groups)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getUserById(user2.externalId)?.ownedGroups)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getGroupById(ownerGroup.externalId)?.ownedGroups)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getGroupById(ownedGroup.externalId)?.ownerGroups)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getGroupById(parentGroup.externalId)?.groupMembers)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getGroupById(memberGroup.externalId)?.parentGroups)
    }

    @Test
    fun `Test update group`() {
        val created =
            externalCredentialProvider.createGroup(
                createTestExternalGroup(
                    id = "0",
                    userMembers =
                    setOf(
                        externalCredentialProvider.createUser(
                            createTestExternalUser(),
                            UUID.randomUUID().toString(),
                        ).externalId,
                    ),
                    owners =
                    setOf(
                        externalCredentialProvider.createUser(
                            createTestExternalUser(),
                            UUID.randomUUID().toString(),
                        ).externalId,
                    ),
                    ownerGroups =
                    setOf(
                        externalCredentialProvider.createGroup(
                            createTestExternalGroup(),
                        ).externalId,
                    ),
                    ownedGroups =
                    setOf(
                        externalCredentialProvider.createGroup(
                            createTestExternalGroup(),
                        ).externalId,
                    ),
                    groupMembers =
                    setOf(
                        externalCredentialProvider.createGroup(
                            createTestExternalGroup(),
                        ).externalId,
                    ),
                    parentGroups =
                    setOf(
                        externalCredentialProvider.createGroup(
                            createTestExternalGroup(),
                        ).externalId,
                    ),
                ),
            )

        val name = UUID.randomUUID().toString()
        val description = UUID.randomUUID().toString()

        val user1 = externalCredentialProvider.createUser(createTestExternalUser(), UUID.randomUUID().toString())
        val user2 = externalCredentialProvider.createUser(createTestExternalUser(), UUID.randomUUID().toString())

        val ownerGroup =
            externalCredentialProvider.createGroup(createTestExternalGroup())
        val ownedGroup =
            externalCredentialProvider.createGroup(createTestExternalGroup())

        val memberGroup =
            externalCredentialProvider.createGroup(createTestExternalGroup())
        val parentGroup =
            externalCredentialProvider.createGroup(createTestExternalGroup())

        val toUpdate =
            createTestExternalGroup(
                id = created.externalId,
                name = name,
                description = description,
                owners = setOf(user2.externalId),
                ownerGroups = setOf(ownerGroup.externalId),
                ownedGroups = setOf(ownedGroup.externalId),
                userMembers = setOf(user1.externalId, user2.externalId),
                groupMembers = setOf(memberGroup.externalId, ownerGroup.externalId),
                parentGroups = setOf(parentGroup.externalId, ownedGroup.externalId),
            )
        val updated = externalCredentialProvider.updateGroup(toUpdate)
        val fresh = externalCredentialProvider.getGroupById(created.externalId)

        assertNotNull(updated)
        assertEquals(toUpdate, updated)
        assertEquals(updated, fresh)

        // Check that old references are cleared
        assertEquals(emptySet(), externalCredentialProvider.getUserById(created.userMembers.first())?.groups)
        assertEquals(emptySet(), externalCredentialProvider.getUserById(created.owners.first())?.ownedGroups)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(created.ownerGroups.first())?.ownedGroups)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(created.ownedGroups.first())?.ownerGroups)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(created.groupMembers.first())?.groupMembers)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(created.parentGroups.first())?.parentGroups)

        // Check that references are set
        assertEquals(setOf(created.externalId), externalCredentialProvider.getUserById(user1.externalId)?.groups)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getUserById(user2.externalId)?.ownedGroups)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getGroupById(ownerGroup.externalId)?.ownedGroups)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getGroupById(ownedGroup.externalId)?.ownerGroups)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getGroupById(parentGroup.externalId)?.groupMembers)
        assertEquals(setOf(created.externalId), externalCredentialProvider.getGroupById(memberGroup.externalId)?.parentGroups)
    }

    @Test
    fun `Test delete group works`() {
        val created =
            externalCredentialProvider.createGroup(
                createTestExternalGroup(
                    id = "0",
                    userMembers =
                    setOf(
                        externalCredentialProvider.createUser(
                            createTestExternalUser(),
                            UUID.randomUUID().toString(),
                        ).externalId,
                    ),
                    owners =
                    setOf(
                        externalCredentialProvider.createUser(
                            createTestExternalUser(),
                            UUID.randomUUID().toString(),
                        ).externalId,
                    ),
                    ownerGroups = setOf(externalCredentialProvider.createGroup(createTestExternalGroup()).externalId),
                    ownedGroups = setOf(externalCredentialProvider.createGroup(createTestExternalGroup()).externalId),
                    groupMembers = setOf(externalCredentialProvider.createGroup(createTestExternalGroup()).externalId),
                    parentGroups = setOf(externalCredentialProvider.createGroup(createTestExternalGroup()).externalId),
                ),
            )

        assertNotNull(created)
        // Check also that any references are cleared by recreating the group with the same data,
        // but no groups and permissions
        externalCredentialProvider.deleteGroup(created.externalId)
        // Check that old references are cleared
        assertEquals(emptySet(), externalCredentialProvider.getUserById(created.userMembers.first())?.groups)
        assertEquals(emptySet(), externalCredentialProvider.getUserById(created.owners.first())?.ownedGroups)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(created.ownerGroups.first())?.ownedGroups)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(created.ownedGroups.first())?.ownerGroups)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(created.groupMembers.first())?.groupMembers)
        assertEquals(emptySet(), externalCredentialProvider.getGroupById(created.parentGroups.first())?.parentGroups)

        assertNull(externalCredentialProvider.getGroupById(created.externalId))

        val recreatedGroup =
            externalCredentialProvider.createGroup(
                created.copy(
                    externalId = "",
                    userMembers = emptySet(),
                    owners = emptySet(),
                    ownerGroups = emptySet(),
                    ownedGroups = emptySet(),
                    groupMembers = emptySet(),
                    parentGroups = emptySet(),
                ),
            )
        val freshAfterDelete = externalCredentialProvider.getGroupById(recreatedGroup.externalId)

        assertNotNull(freshAfterDelete)
        assertEquals(emptySet(), recreatedGroup.userMembers)
        assertEquals(emptySet(), recreatedGroup.owners)
        assertEquals(emptySet(), recreatedGroup.ownedGroups)
        assertEquals(emptySet(), recreatedGroup.groupMembers)
        assertEquals(emptySet(), recreatedGroup.ownerGroups)
        assertEquals(emptySet(), recreatedGroup.parentGroups)
        assertEquals(recreatedGroup, freshAfterDelete)
    }

    private fun createTestExternalUser(
        externalId: String = "",
        username: String = UUID.randomUUID().toString(),
        displayName: String = UUID.randomUUID().toString(),
        firstname: String = UUID.randomUUID().toString(),
        surname: String = UUID.randomUUID().toString(),
        mail: String = "${UUID.randomUUID()}@example.org",
        groups: Set<String> = emptySet(),
        ownedGroups: Set<String> = emptySet(),
        registeredSince: Instant = Instant.ofEpochSecond(0),
        disabled: Boolean = false,
    ) = ExternalUser(
        externalId = externalId,
        username = username,
        displayName = displayName,
        firstname = firstname,
        surname = surname,
        mail = mail,
        groups = groups,
        ownedGroups = ownedGroups,
        registeredSince = registeredSince,
        disabled = disabled,
    )

    private fun createTestExternalGroup(
        id: String = "",
        name: String = UUID.randomUUID().toString(),
        description: String = UUID.randomUUID().toString(),
        userMembers: Set<String> = emptySet(),
        groupMembers: Set<String> = emptySet(),
        parentGroups: Set<String> = emptySet(),
        owners: Set<String> = emptySet(),
        ownerGroups: Set<String> = emptySet(),
        ownedGroups: Set<String> = emptySet(),
    ): ExternalGroup =
        ExternalGroup(
            externalId = id,
            name = name,
            description = description,
            userMembers = userMembers,
            groupMembers = groupMembers,
            parentGroups = parentGroups,
            owners = owners,
            ownerGroups = ownerGroups,
            ownedGroups = ownedGroups,
        )
}
