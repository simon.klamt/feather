/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.testutils

import dev.maximilian.feather.ExternalGroup
import dev.maximilian.feather.ExternalUser
import dev.maximilian.feather.IExternalCredentialProvider
import dev.maximilian.feather.Permission
import java.time.Instant
import java.util.UUID

public class InMemoryExternalCredentialProvider : IExternalCredentialProvider {
    private var idUserMap: MutableMap<String, ExternalUser> = mutableMapOf()
    private var idGroupMap: MutableMap<String, ExternalGroup> = mutableMapOf()
    private val idPasswordMap: MutableMap<String, String?> = mutableMapOf()
    private val idImageMap: MutableMap<String, ByteArray?> = mutableMapOf()

    // This function will be removed in the future, no test should longer depend on it
    override fun getPermissions(): Map<String, Set<Permission>> = emptyMap()

    override fun createUser(
        user: ExternalUser,
        password: String?,
    ): ExternalUser {
        val newId = UUID.randomUUID().toString()
        val newUser =
            user.copy(
                externalId = newId,
                registeredSince = Instant.now(),
                groups = user.groups + user.ownedGroups,
            )

        idPasswordMap[newId] = password
        idUserMap[newId] = newUser

        user.groups.forEach { g ->
            idGroupMap[g] =
                requireNotNull(idGroupMap[g]).let {
                    it.copy(userMembers = it.userMembers + newId)
                }
        }

        user.ownedGroups.forEach { g ->
            idGroupMap[g] =
                requireNotNull(idGroupMap[g]).let {
                    it.copy(owners = it.owners + newId)
                }
        }

        return newUser
    }

    override fun getUserById(userId: String): ExternalUser? = idUserMap[userId]

    override fun getUsersAndGroups(): Pair<Collection<ExternalUser>, Collection<ExternalGroup>> = idUserMap.values to idGroupMap.values

    override fun authenticateUserByUsernameOrMail(
        usernameOrMail: String,
        password: String,
    ): String? =
        idUserMap.entries.firstOrNull { it.value.username == usernameOrMail || it.value.mail == usernameOrMail }
            ?.takeIf {
                idPasswordMap[it.key] == password
            }?.key

    override fun updateUserPassword(
        externalId: String,
        newPassword: String,
    ): Boolean {
        if (!idPasswordMap.contains(externalId)) return false
        idPasswordMap[externalId] = newPassword
        return true
    }

    override fun deleteUser(externalId: String) {
        idPasswordMap.remove(externalId)
        idUserMap.remove(externalId)

        idGroupMap.filter {
            it.value.userMembers.contains(externalId) || it.value.owners.contains(externalId)
        }.map { it.key }.forEach { g ->
            idGroupMap[g] =
                requireNotNull(idGroupMap[g]).let {
                    it.copy(owners = it.owners - externalId, userMembers = it.userMembers - externalId)
                }
        }
    }

    override fun updatePhotoForUser(
        externalId: String,
        image: ByteArray,
    ) {
        idImageMap[externalId] = image
    }

    override fun deletePhotoForUser(externalId: String) {
        idImageMap[externalId] = null
    }

    override fun getPhotoForUser(externalId: String): ByteArray? = idImageMap[externalId]

    override fun updateUser(user: ExternalUser): ExternalUser? {
        val freshUser = idUserMap[user.externalId] ?: return null
        val validatedUser =
            user.copy(
                registeredSince = freshUser.registeredSince,
                groups = user.groups + user.ownedGroups,
            )

        // First remove all groups
        idGroupMap.filter {
            it.value.userMembers.contains(validatedUser.externalId) || it.value.owners.contains(validatedUser.externalId)
        }.map { it.key }.forEach { g ->
            idGroupMap[g] =
                requireNotNull(idGroupMap[g]).let {
                    it.copy(
                        owners = it.owners - validatedUser.externalId,
                        userMembers = it.userMembers - validatedUser.externalId,
                    )
                }
        }

        // Then add again
        validatedUser.groups.forEach { g ->
            idGroupMap[g] =
                requireNotNull(idGroupMap[g]).let {
                    it.copy(userMembers = it.userMembers + validatedUser.externalId)
                }
        }

        validatedUser.ownedGroups.forEach { g ->
            idGroupMap[g] =
                requireNotNull(idGroupMap[g]).let {
                    it.copy(owners = it.owners + validatedUser.externalId)
                }
        }

        idUserMap[user.externalId] = validatedUser

        return validatedUser
    }

    override fun createGroup(group: ExternalGroup): ExternalGroup {
        val newId = UUID.randomUUID().toString()
        val newGroup =
            group.copy(
                externalId = newId,
                userMembers = group.userMembers + group.owners,
                groupMembers = group.groupMembers + group.ownerGroups,
                parentGroups = group.parentGroups + group.ownedGroups,
            )
        idGroupMap[newId] = newGroup
        fixGroupMemberships(newGroup)

        return newGroup
    }

    private fun fixGroupMemberships(toFix: ExternalGroup) {
        // Remove every membership existing before with this group
        idUserMap =
            idUserMap.map { (userId, user) ->
                userId to
                    user.copy(
                        groups = user.groups - toFix.externalId,
                        ownedGroups = user.ownedGroups - toFix.externalId,
                    )
            }.toMap().toMutableMap()

        idGroupMap =
            idGroupMap.map { (groupId, group) ->
                groupId to
                    group.copy(
                        userMembers = group.userMembers - toFix.externalId,
                        groupMembers = group.groupMembers - toFix.externalId,
                        parentGroups = group.parentGroups - toFix.externalId,
                        owners = group.owners - toFix.externalId,
                        ownerGroups = group.ownerGroups - toFix.externalId,
                        ownedGroups = group.ownedGroups - toFix.externalId,
                    )
            }.toMap().toMutableMap()

        toFix.userMembers.forEach {
            val otherUser = requireNotNull(idUserMap[it])
            idUserMap[otherUser.externalId] = otherUser.copy(groups = otherUser.groups + toFix.externalId)
        }

        toFix.owners.forEach {
            val otherUser = requireNotNull(idUserMap[it])
            idUserMap[otherUser.externalId] = otherUser.copy(ownedGroups = otherUser.ownedGroups + toFix.externalId)
        }

        toFix.parentGroups.forEach {
            val otherGroup = requireNotNull(idGroupMap[it])
            idGroupMap[otherGroup.externalId] = otherGroup.copy(groupMembers = otherGroup.groupMembers + toFix.externalId)
        }

        toFix.groupMembers.forEach {
            val otherGroup = requireNotNull(idGroupMap[it])
            idGroupMap[otherGroup.externalId] = otherGroup.copy(parentGroups = otherGroup.parentGroups + toFix.externalId)
        }

        toFix.ownerGroups.forEach {
            val otherGroup = requireNotNull(idGroupMap[it])
            idGroupMap[otherGroup.externalId] = otherGroup.copy(ownedGroups = otherGroup.ownedGroups + toFix.externalId)
        }

        toFix.ownedGroups.forEach {
            val otherGroup = requireNotNull(idGroupMap[it])
            idGroupMap[otherGroup.externalId] = otherGroup.copy(ownerGroups = otherGroup.ownerGroups + toFix.externalId)
        }
    }

    override fun getGroupById(id: String): ExternalGroup? = idGroupMap[id]

    override fun updateGroup(group: ExternalGroup): ExternalGroup? {
        if (!idGroupMap.contains(group.externalId)) return null

        val validatedGroup =
            group.copy(
                userMembers = group.userMembers + group.owners,
                groupMembers = group.groupMembers + group.ownerGroups,
                parentGroups = group.parentGroups + group.ownedGroups,
            )

        fixGroupMemberships(validatedGroup)
        idGroupMap[validatedGroup.externalId] = validatedGroup

        return validatedGroup
    }

    override fun deleteGroup(externalId: String) {
        val freshGroup = idGroupMap[externalId] ?: return
        fixGroupMemberships(
            freshGroup.copy(
                userMembers = emptySet(),
                groupMembers = emptySet(),
                parentGroups = emptySet(),
                owners = emptySet(),
                ownerGroups = emptySet(),
                ownedGroups = emptySet(),
            ),
        )
        idGroupMap.remove(externalId)
    }

    override fun close() { /* No-Op */ }
}
