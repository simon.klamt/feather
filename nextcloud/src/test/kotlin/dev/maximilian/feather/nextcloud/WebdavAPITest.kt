/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import com.github.sardine.SardineFactory
import dev.maximilian.feather.nextcloud.webdavAPI.WebdavAPI
import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.WebdavReal
import kotlin.test.Test
import kotlin.test.assertEquals

class WebdavAPITest {
    private val webdavURL =
        getEnv(
            "NEXTCLOUD_WEBDAV",
            TestUtil.baseUrl + "/remote.php/dav/files/" + TestUtil.user + "/",
        )

    private val webdavAPI =
        WebdavAPI(
            webdavURL,
            WebdavReal(SardineFactory.begin(TestUtil.user, TestUtil.password)),
        )

    @Test
    fun `Test list folders shows correct entries`() {
        val path = "Testvorlage"
        val parentFolder = "$path/Gruppenintern"
        webdavAPI.createFolder(path)
        webdavAPI.createFolder(parentFolder)
        webdavAPI.createFolder("$parentFolder/Erkundungintern")
        webdavAPI.createFolder("$parentFolder/intern")
        val result = webdavAPI.listFolders(parentFolder)
        webdavAPI.delete(path)
        assertEquals(listOf("Erkundungintern", "intern"), result.sorted())
    }

    @Test
    fun `Test listFolders`() {
        var folders = webdavAPI.listFolders("/")

        if (folders.isEmpty()) {
            // test at least once with a folder
            webdavAPI.createFolder(generateTestPath().second)

            folders = webdavAPI.listFolders("/")
        }

        assert(!folders.isEmpty())

        folders.forEach {
            check(!it.isEmpty())
        }
    }

    @Test
    fun `Test listFiles at root`() {
        var files = webdavAPI.listFiles("/")

        if (files.isEmpty()) {
            // test at least once with a file
            val testContent = "This file was automatically created by feather."
            webdavAPI.createFile(generateTestPath().second, testContent)

            files = webdavAPI.listFiles("/")
        }

        assert(!files.isEmpty())

        files.forEach {
            check(!it.isEmpty())
        }
    }

    @Test
    fun `Test listFiles in subfolder`() {
        webdavAPI.createFolder("/blabla")
        val testContent = "This file was automatically created by feather."
        webdavAPI.createFile("/blabla/testfile.md", testContent)
        var files = webdavAPI.listFiles("/blabla")
        webdavAPI.delete("/blabla")
        assertEquals(listOf("testfile.md"), files)
    }

    @Test
    fun `Test createDeleteFolder`() {
        val parentFolder = generateTestPath().second
        webdavAPI.createFolder(parentFolder)

        val childFolder = generateTestPath(parentFolder)
        webdavAPI.createFolder(childFolder.second)

        val foldersAfterCreation = webdavAPI.listFolders(parentFolder)
        assert(foldersAfterCreation.contains(childFolder.first))

        webdavAPI.delete(childFolder.second)
        val foldersAfterDelete = webdavAPI.listFolders(parentFolder)
        assert(!foldersAfterDelete.contains(childFolder.first))
    }

    @Test
    fun `Test createDeleteFile`() {
        val parentFolder = generateTestPath().second
        webdavAPI.createFolder(parentFolder)

        val testContent = "This file was automatically created by feather."
        val filePath = generateTestPath(parentFolder)
        webdavAPI.createFile(filePath.second, testContent)

        val filesAfterCreation = webdavAPI.listFiles(parentFolder)
        assert(filesAfterCreation.contains(filePath.first))

        webdavAPI.delete(filePath.second)
        val filesAfterDelete = webdavAPI.listFiles(parentFolder)
        assert(!filesAfterDelete.contains(filePath.first))
    }

    @Test
    fun `Test createFileContent`() {
        val parentFolder = generateTestPath().second
        webdavAPI.createFolder(parentFolder)

        val testContent = "This file was automatically created by feather."
        val filePath = generateTestPath(parentFolder)
        webdavAPI.createFile(filePath.second, testContent)

        val content = webdavAPI.getFile(filePath.second)

        webdavAPI.delete(filePath.second)
        assertEquals(content, testContent)
    }

    @Test
    fun `Test copyFolder`() {
        val parentFolder = generateTestPath().second
        webdavAPI.createFolder(parentFolder)

        val testContent = "This file was automatically created by feather."
        val filePath = generateTestPath(parentFolder)
        webdavAPI.createFile(filePath.second, testContent)
        webdavAPI.createFolder(parentFolder + "Target")

        webdavAPI.copy(parentFolder, parentFolder + "Target")

        val files = webdavAPI.listFiles(parentFolder + "Target")
        assert(files.contains(filePath.first))
    }
}
