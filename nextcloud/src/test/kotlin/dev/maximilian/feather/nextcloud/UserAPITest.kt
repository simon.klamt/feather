/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import dev.maximilian.feather.nextcloud.ocs.NextcloudUser
import dev.maximilian.feather.nextcloud.ocs.UserAPI
import dev.maximilian.feather.testutils.FakeData
import kotlinx.coroutines.runBlocking
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

class UserAPITest {
    companion object {
        private val userAPI = UserAPI(TestUtil.nextcloud.apiClient, TestUtil.baseUrl)
    }

    @Test
    fun `Test createUser`() {
        runBlocking {
            val userData = generateTestUser()
            val result = userAPI.createUser(userData)
            check(result)

            val user = requireNotNull(userAPI.getUserInfo(userData.userid))
            assertEquals(user.displayname, userData.displayName)
            assertEquals(user.email, userData.email)
        }
    }

    @Test
    fun `Test changeMailAddress`() {
        runBlocking {
            val userData = generateTestUser()
            val result = userAPI.createUser(userData)
            check(result)

            val newMail = FakeData.generateMailAddress()
            userAPI.changeMailAddress(userData.userid, newMail)

            val user = requireNotNull(userAPI.getUserInfo(userData.userid))
            assertEquals(user.displayname, userData.displayName)
            assertEquals(user.email, newMail)
        }
    }

    @Test
    fun `Test getAllUserID`() {
        runBlocking {
            val userList = userAPI.getAllUsers()
            check(userList.contains(TestUtil.user))
        }
    }

    @Test
    fun `Test getAllUsersWithDetails`() {
        runBlocking {
            val usersToCreate = (0 until 5).map { generateTestUser() }
            usersToCreate.forEach { userAPI.createUser(it) }
            val userList = userAPI.getAllUsersWithDetails(usersToCreate.map { it.userid }.toSet())
            val mappedResultList =
                userList.map {
                    NextcloudUser(
                        displayName = it.displayname!!,
                        email = it.email!!,
                        userid = it.id,
                        password = "",
                        groups = emptyList(),
                        subadmin = emptyList(),
                        quota = it.quota.quota,
                        language = "",
                    )
                }

            assertEquals(usersToCreate.map { it.copy(password = "") }.toSet(), mappedResultList.toSet())
        }
    }

    @Test
    fun `Test getUserInfo Quota correct`() {
        runBlocking {
            val quota = Random.nextLong(10, 100)
            val user = generateTestUser(quota = quota)
            userAPI.createUser(user)
            val retrievedUser = requireNotNull(userAPI.getUserInfo(user.userid))
            assertEquals(quota, retrievedUser.quota.quota)
        }
    }

    @Test
    fun `Test getUserInfo email correct`() {
        runBlocking {
            val user = generateTestUser()
            userAPI.createUser(user)
            val retrievedUser = requireNotNull(userAPI.getUserInfo(user.userid))
            assertEquals(user.email, retrievedUser.email)
        }
    }

    @Test
    fun `Test enableUser and disableUser`() {
        runBlocking {
            val userData = generateTestUser()
            val result = userAPI.createUser(userData)
            check(result)

            val user = requireNotNull(userAPI.getUserInfo(userData.userid))
            assert(user.enabled)

            userAPI.disableUser(userData.userid)

            val user2 = requireNotNull(userAPI.getUserInfo(userData.userid))
            assert(!user2.enabled)

            userAPI.enableUser(userData.userid)

            val user3 = requireNotNull(userAPI.getUserInfo(userData.userid))
            assert(user3.enabled)
        }
    }
}
