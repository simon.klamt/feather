/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs.entities

import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class CreateShareResponseSubEntity(
    @SerialName("id")
    public val id: Int,
    @SerialName("share_type")
    public val shareType: ShareType,
    @SerialName("displayname_file_owner")
    public val displayNameFileOwner: String? = null,
    @SerialName("displayname_owner")
    public val displayNameOwner: String? = null,
)
