/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.nextcloud.Ocs
import dev.maximilian.feather.nextcloud.getOrNull
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.NextcloudUserDataEntity
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.delete
import io.ktor.client.request.forms.submitForm
import io.ktor.client.request.get
import io.ktor.client.request.put
import io.ktor.http.HttpMethod
import io.ktor.http.parameters
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable

internal class UserAPI(private val client: HttpClient, baseUrl: String) : IUserAPI {
    private val basePath = "ocs/v1.php/cloud"
    private val userPath = "$baseUrl/$basePath/users"

    override suspend fun createUser(
        userid: String,
        password: String?, // can be empty to send invitation mail
        displayName: String,
        email: String?, // must not be empty if password is empty
        groups: List<String>,
        subadmin: List<String>,
        quota: Long,
        language: String,
    ): Boolean {
        require(password != null || email != null) {
            "Either email or password must be set"
        }

        return client.submitForm(
            url = userPath,
            formParameters =
            parameters {
                append("userid", userid)
                append("displayName", displayName)
                append("quota", "$quota")
                append("language", language)

                if (password != null) {
                    append("password", password)
                }

                if (email != null) {
                    append("email", email)
                }

                groups.forEach {
                    append("group[]", it)
                }

                subadmin.forEach {
                    append("subadmin[]", it)
                }
            },
        ).body<Ocs<AddUserResponseSubEntity>>().ocs.meta.statusCode.let {
            it == 100 || it == 102
        }
    }

    override suspend fun getUserInfo(id: String): NextcloudUserDataEntity? =
        client.getOrNull<Ocs<NextcloudUserDataEntity>>("$userPath/$id")?.ocs?.data

    override suspend fun getAllUsers(): List<String> = client.get(userPath).body<Ocs<UserList>>().ocs.data.users

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun getAllUsersWithDetails(forUsers: Set<String>?): List<NextcloudUserDataEntity> =
        coroutineScope {
            val searchSet = forUsers ?: getAllUsers()
            val producedWork: ReceiveChannel<String> =
                produce {
                    searchSet.forEach { send(it) }
                }
            val resultChannel = Channel<NextcloudUserDataEntity>(searchSet.size)

            // Parallel n workers
            (0 until 20).map {
                launch {
                    for (work in producedWork) {
                        getUserInfo(work)?.let { res -> resultChannel.send(res) }
                    }
                }
            }.let { joinAll(*it.toTypedArray()) }
            resultChannel.close()

            resultChannel.receiveAsFlow().toList()
        }

    override suspend fun changeMailAddress(
        userid: String,
        newMailAddress: String,
    ) {
        client.submitForm(
            url = "$userPath/$userid",
            formParameters =
            parameters {
                append("key", "email")
                append("value", newMailAddress)
            },
        ) {
            method = HttpMethod.Put
        }.body<Ocs<List<String>>>().ocs.meta.also {
            require(it.statusCode == 100) {
                "::changeMailAddress Failure response ${it.statusCode}: ${it.status} - ${it.message}"
            }
        }
    }

    override suspend fun deleteUser(userID: String) {
        client.delete("$userPath/$userID").body<Unit>()
    }

    override suspend fun enableUser(userID: String) {
        client.put("$userPath/$userID/enable").body<Unit>()
    }

    override suspend fun disableUser(userID: String) {
        client.put("$userPath/$userID/disable").body<Unit>()
    }
}

@Serializable
private data class UserList(
    val users: List<String>,
)

@Serializable
private data class AddUserResponseSubEntity(
    val id: String,
)
