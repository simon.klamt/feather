/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.groupfolders

import dev.maximilian.feather.nextcloud.Ocs
import dev.maximilian.feather.nextcloud.SuccessResponse
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.PermissionTypeSetSerializer
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.delete
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

internal class GroupFoldersGroupsAPI(
    private val client: HttpClient,
    baseUrl: String,
    private val groupFolderId: Int,
) : IGroupFoldersGroupsAPI {
    private val basePath = "apps/groupfolders"
    private val groupsPath = "$baseUrl/$basePath/folders/$groupFolderId/groups"

    override suspend fun getGroupFolderId(): Int = groupFolderId

    override suspend fun addGroup(group: String): Boolean =
        client.post(groupsPath) {
            setBody(AddGroupRequest(group))
        }.body<Ocs<SuccessResponse>>().ocs.data.success

    // permissions are a bitmask of dev.maximilian.feather.nextcloud.ocs.general.PermissionType
    override suspend fun changePermissions(
        group: String,
        newPermissions: Set<PermissionType>,
    ): Boolean =
        client.post("$groupsPath/$group") {
            setBody(ChangePermissionsRequest(newPermissions))
        }.body<Ocs<SuccessResponse>>().ocs.data.success

    override suspend fun deleteGroup(group: String) {
        client.delete("$groupsPath/$group").body<Unit>()
    }
}

@Serializable
private data class AddGroupRequest(
    val group: String,
)

@Serializable
private data class ChangePermissionsRequest(
    @SerialName("permissions")
    @Serializable(with = PermissionTypeSetSerializer::class)
    val permissions: Set<PermissionType>,
)
