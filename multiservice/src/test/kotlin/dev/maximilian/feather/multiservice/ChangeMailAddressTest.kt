/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.multiservice.api.internal.ChangeMailAddressRequest
import dev.maximilian.feather.testutils.FakeData
import kong.unirest.core.Empty
import kong.unirest.core.HttpResponse
import org.eclipse.jetty.http.HttpStatus
import java.util.*
import kotlin.test.Test
import kotlin.test.assertEquals

class ChangeMailAddressTest {
    private val apiUtilities = ApiTestUtilities()
    private val scenario = apiUtilities.startWithDummyService()

    @Test
    fun `POST change mail address leads to CREATED (201) and starts mail change process`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        val newMail = FakeData.generateMailAddress()
        val result =
            executeChangeMail(
                user.id,
                password,
                newMail,
                true,
            )
        assertEquals(HttpStatus.CREATED_201, result.status)
    }

    @Test
    fun `POST change mail address without verified leads to BAD REQUEST (400)`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        val result =
            executeChangeMail(
                user.id,
                password,
                FakeData.generateMailAddress(),
                false,
            )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST change mail address with wrong password leads to FORBIDDEN (403)`() {
        val (user, _) = apiUtilities.createAndLoginUser()
        val result =
            executeChangeMail(
                user.id,
                UUID.randomUUID().toString(),
                FakeData.generateMailAddress(),
                true,
            )
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `POST change mail address without @ in email leads to BAD REQUEST (400)`() {
        val (user, password) = apiUtilities.createAndLoginUser()

        val result =
            executeChangeMail(
                user.id,
                password,
                FakeData.generateMailAddress().replace("@", ""),
                true,
            )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST change mail address with Umlaut in email leads to BAD REQUEST (400)`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        val result =
            executeChangeMail(
                user.id,
                password,
                "ä${FakeData.generateMailAddress()}",
                true,
            )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST change mail address without suffix in email leads to BAD REQUEST (400)`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        val result =
            executeChangeMail(
                user.id,
                password,
                "asdcas@maximilian",
                true,
            )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    private fun executeChangeMail(
        userId: Int,
        password: String,
        newMail: String,
        verified: Boolean,
    ): HttpResponse<Empty> {
        val changeMailRequest =
            ChangeMailAddressRequest(
                password,
                newMail,
                verified,
            )
        return apiUtilities.restConnection.post("${scenario.basePath}/change_mail/step1/$userId").body(changeMailRequest).asEmpty()
    }
}
