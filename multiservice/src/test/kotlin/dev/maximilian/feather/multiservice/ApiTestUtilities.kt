/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.AppBuilder
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.authorization.CredentialAuthorizer
import dev.maximilian.feather.authorization.api.AuthorizerApi
import dev.maximilian.feather.authorization.api.LoginRequest
import dev.maximilian.feather.authorization.api.SessionAnswer
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.multiservice.mockserver.EmptyCheckUserEvent
import dev.maximilian.feather.multiservice.mockserver.GroupSyncMock
import dev.maximilian.feather.multiservice.mockserver.ServiceMock
import dev.maximilian.feather.multiservice.mockserver.SessionTerminatorMock
import dev.maximilian.feather.multiservice.mockserver.UserDeletionEventMock
import dev.maximilian.feather.multiservice.mockserver.UserRemovedCallbackChecker
import dev.maximilian.feather.multiservice.settings.MultiServiceConfig
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.multiservice.settings.TestMailSettings
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.Config
import kong.unirest.core.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper
import org.jetbrains.exposed.sql.Database
import redis.clients.jedis.JedisPool
import java.sql.DriverManager
import java.util.*
import kotlin.test.assertNotNull

class ApiTestUtilities {
    companion object {
        internal val db = Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:feather;DB_CLOSE_DELAY=-1") })

        private const val BASE_PATH_PREFIX = "http://127.0.0.1"
        private const val BASE_PATH_SUFFIX = "/v1"

        internal val pool: JedisPool by lazy { ServiceConfig.JEDIS_POOL }
    }

    private val accountController = ServiceConfig.ACCOUNT_CONTROLLER

    private val credentialProvider = accountController

    val gdprController = GdprController(db, ActionController(db, accountController), accountController)

    private val backgroundJobManager = BackgroundJobManager(pool)

    private val syncEvent = GroupSyncMock()

    internal val restConnection =
        UnirestInstance(
            Config().setObjectMapper(
                JacksonObjectMapper(
                    jacksonObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                        .registerModule(JavaTimeModule()),
                ),
            )
                .addDefaultHeader("Accept", "application/json"),
        )

    var scenario: TestScenario? = null

    var removedUserChecker = UserRemovedCallbackChecker()

    fun startWithDummyService(): TestScenario {
        val userDeletionEvents = listOf(UserDeletionEventMock())
        val sessionTerminatorEvent = SessionTerminatorMock()

        val app = AppBuilder().createApp("test", "test-utilities", "http://127.0.0.1", emptySet(), 0)

        AuthorizerApi(
            app,
            AuthorizationController(
                mutableSetOf(CredentialAuthorizer("credential", credentialProvider, "http://127.0.0.1")),
                accountController,
                db,
                ActionController(db, accountController),
            ),
            true,
        )
        val callbackEvents =
            MultiServiceEvents(
                syncEvent,
                userDeletionEvents,
                emptyList(),
                sessionTerminatorEvent,
                EmptyCheckUserEvent(),
                emptyList(),
                emptyList(),
                listOf(removedUserChecker),
            )
        val multiserviceConfig =
            MultiServiceConfig(
                credentialProvider,
                backgroundJobManager,
                db,
                TestMailSettings.mailSetting,
                callbackEvents,
                gdprController,
            )
        val ms = Multiservice(multiserviceConfig, accountController)

        val testService = ServiceMock()

        ms.services.add(testService)
        ms.startApis(app)

        scenario = TestScenario(testService, "$BASE_PATH_PREFIX:${app.port()}$BASE_PATH_SUFFIX")

        return scenario!!
    }

    fun loginUser(
        usernameOrEmail: String,
        password: String,
    ): SessionAnswer {
        val session =
            restConnection
                .post("${scenario!!.basePath}/authorizer/credential")
                .body(LoginRequest(usernameOrEmail, password))
                .asObject(SessionAnswer::class.java)

        assertNotNull(session.body)
        assert(usernameOrEmail == session.body.user.username || usernameOrEmail == session.body.user.mail)

        return session.body
    }

    internal fun createAndLoginUser(admin: Boolean = false): Pair<User, String> {
        val password = UUID.randomUUID().toString()
        val user =
            accountController.createUser(
                TestUser.generateTestUser(permissions = if (admin) setOf(Permission.ADMIN) else emptySet()),
                password,
            )
        val sessionAnswer = loginUser(user.username, password)
        return sessionAnswer.user to password
    }

    data class TestScenario(
        val serviceMock: ServiceMock,
        val basePath: String,
    )
}
