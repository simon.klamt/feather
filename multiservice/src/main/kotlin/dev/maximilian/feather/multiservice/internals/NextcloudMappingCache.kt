/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.NextcloudUserDataEntity
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPool
import redis.clients.jedis.params.SetParams

internal class NextcloudMappingCache(var pool: JedisPool) {
    private val setParams = SetParams().ex(300)
    private val objectMapper = jacksonObjectMapper().registerModule(JavaTimeModule())

    fun deleteFromNextcloudMappingCache(userId: Int) {
        // explicit enforcement of premature expiration
        redisTransaction { jedis ->
            // compare NextcloudManager::getNextcloudUser
            jedis.del("binding.nextcloud.$userId")
            null
        }
    }

    fun getUser(userId: Int): String? {
        return pool.resource.use { it["binding.nextcloud.$userId"] }
    }

    fun store(
        userId: Int,
        userInfo: NextcloudUserDataEntity,
    ) {
        pool.resource.use {
            it.set("binding.nextcloud.$userId", objectMapper.writeValueAsString(userInfo), setParams)
        }
    }

    fun decodeDatatEntity(cachedValue: String): NextcloudUserDataEntity? {
        return objectMapper.readValue(cachedValue)
    }

    private fun <T> redisTransaction(block: (Jedis) -> T): T = pool.resource.use(block)
}
