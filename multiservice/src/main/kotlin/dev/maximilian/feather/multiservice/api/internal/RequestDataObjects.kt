/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import java.util.Collections.emptySet

internal data class MultiServiceUserCreateRequest(
    val displayName: String,
    val password1: String,
    val password2: String,
    val mail: String,
    val firstname: String,
    val surname: String,
    val groups: Set<Int> = emptySet(),
    val permissions: Set<String> = emptySet(),
    val verified: Boolean,
    val gdpr: Int?,
)

internal data class MultiServiceUserDeleteRequest(
    val password: String,
    val verified: Boolean,
)

internal data class MultiServiceUsersDeleteRequest(
    val password: String,
    val users: List<Int>,
    val verified: Boolean,
)
