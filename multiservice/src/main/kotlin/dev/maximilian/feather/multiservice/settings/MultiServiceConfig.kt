/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.settings

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.multiservice.BackgroundJobManager
import org.jetbrains.exposed.sql.Database

data class MultiServiceConfig(
    val credentialProvider: ICredentialProvider,
    val backgroundJobManager: BackgroundJobManager,
    val db: Database,
    val mailSettings: MultiServiceMailSettings,
    val callbackEvents: MultiServiceEvents,
    val gdprController: GdprController,
)
