/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.useroperations

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.UserRequestInfo
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.UserCreationEvent
import mu.KLogging
import java.util.UUID

data class CreateUserConfig(
    val userToBeCreated: User,
    val password: String,
    val gdprAcceptId: Int?,
)

internal class CreateUser(
    private val credentialProvider: ICredentialProvider,
    private val services: List<IControllableService>,
    private val backgroundJobManager: BackgroundJobManager,
    private val userCreationEvent: List<UserCreationEvent>,
    private val gdprController: GdprController,
) {
    companion object : KLogging()

    fun createUser(
        jobId: UUID?,
        config: CreateUserConfig,
    ): User {
        logger.info { "Create::createUser Creating user (${config.userToBeCreated.username})" }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Erzeuge neuen Account in Benutzerverwaltung") }

        val createdUser = credentialProvider.createUser(config.userToBeCreated, config.password)

        if (config.gdprAcceptId != null) {
            gdprController.storeAcceptanceInfo(UserRequestInfo(createdUser, false), config.gdprAcceptId)
        }

        services.forEach { service ->
            kotlin.runCatching { service.createUser(createdUser, jobId) }
                .onSuccess {
                    logger.info(
                        "Create::createUser Create user in ${service.serviceName} successfull.",
                        it,
                    )
                }
                .onFailure { logger.error(it) { "Create::createUser Create user in ${service.serviceName} not successful." } }
        }

        userCreationEvent.forEach { it.userCreated(createdUser) }

        return createdUser
    }
}
