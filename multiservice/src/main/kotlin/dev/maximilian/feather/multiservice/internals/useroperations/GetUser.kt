/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.useroperations

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import java.time.Instant

internal class GetUser(val credentialProvider: ICredentialProvider) {
    companion object {
        private val GROUPS_TO_SEE_CONTACT_INFO = Regex("(rg-.+)|(kg-.+)|([a-z]{3}-iog\\d{2})")
    }

    fun getAllUsers(callingUser: User): List<User> {
        val usersAndGroups = credentialProvider.getUsersAndGroups()
        val users = usersAndGroups.first

        return if (callingUser.isAdmin()) {
            users.map { it.redact(callingUser, showDatePropertiesAndPermission = true, showContactInfo = true) }
        } else {
            val groups = usersAndGroups.second.toList()
            val m = groups.filter { g -> g.hasOwnerRights(callingUser.id, groups) }.map { it.id }.toSet()
            val relevantGroups = callingUser.getGroupsRecursive(groups).filter { g ->
                g.name.matches(GROUPS_TO_SEE_CONTACT_INFO)
            }.toSet()
            users.map { user ->
                val showContactInfo = user.groups.any { m.contains(it) } or
                    relevantGroups.any { g -> user.isMemberRecursive(g, groups) }
                user.redact(callingUser, false, showContactInfo = showContactInfo)
            }
        }
    }

    fun getSingleUser(
        callingUser: User,
        targetUser: User,
    ): User {
        if (callingUser.isAdmin() || (callingUser.id == targetUser.id)) {
            return targetUser.redact(callingUser, true, true)
        } else {
            val groups = credentialProvider.getGroups().toList()
            val showContactInfo = targetUser.groups.mapNotNull { id -> groups.find { it.id == id } }.any { g -> g.hasOwnerRights(callingUser.id, groups) } ||
                targetUser.getGroupsRecursive(groups).any { g ->
                    g.hasOwnerRights(callingUser.id, groups) or
                        (g.name.matches(GROUPS_TO_SEE_CONTACT_INFO) and callingUser.isMemberRecursive(g, groups))
                }
            return targetUser.redact(callingUser, false, showContactInfo = showContactInfo)
        }
    }

    private fun User.isAdmin() = permissions.contains(Permission.ADMIN)

    private fun Group.isUserMemberRecursive(
        userId: Int,
        allGroups: List<Group>,
        depthCount: Int = 0,
    ): Boolean =
        userMembers.contains(userId) ||
            // implicit membership via the child groups
            groupMembers.any {
                val group = allGroups.find { g -> g.id == it }
                if (depthCount > 15) {
                    throw Exception("Cycling dependency in userMember by $userId!")
                }
                group?.isUserMemberRecursive(userId, allGroups, depthCount + 1) ?: false
            }

    private fun getGroupsRecursive(
        rootGroupId: Int,
        allGroups: List<Group>,
        to: MutableSet<Group>,
    ) {
        val rootGroup = allGroups.find { it.id == rootGroupId }!!
        to.add(rootGroup) || return
        rootGroup.parentGroups.forEach { getGroupsRecursive(it, allGroups, to) }
    }

    private fun User.getGroupsRecursive(
        allGroups: List<Group>,
    ): Set<Group> {
        val ret = HashSet<Group>()
        groups.forEach { getGroupsRecursive(it, allGroups, ret) }
        return ret
    }

    private fun User.isMemberRecursive(
        group: Group,
        allGroups: List<Group>,
        depthCount: Int = 0,
    ): Boolean = groups.contains(group.id) ||
        // implicit membership via the child groups
        group.groupMembers.any {
            val subgroup = allGroups.find { g -> g.id == it }
            if (depthCount > 15) {
                throw Exception("Cycling dependency in userMember!")
            }
            subgroup?.let { isMemberRecursive(it, allGroups, depthCount + 1) } ?: false
        }

    private fun Group.hasOwnerRights(userId: Int, allGroups: List<Group>): Boolean =
        owners.contains(userId) || ownerGroups.any {
            val group = allGroups.find { g -> g.id == it }
            group?.isUserMemberRecursive(userId, allGroups) ?: false
        }

    private fun User.redact(
        forUser: User,
        showDatePropertiesAndPermission: Boolean,
        showContactInfo: Boolean,
    ) = if (this.id == forUser.id) {
        this
    } else {
        this.copy(
            id = this.id,
            registeredSince = if (showDatePropertiesAndPermission) this.registeredSince else Instant.ofEpochSecond(0),
            firstname = if (showContactInfo) this.firstname else "",
            surname = if (showContactInfo) this.surname else "",
            permissions = if (showDatePropertiesAndPermission) this.permissions else emptySet(),
            groups = this.groups,
            ownedGroups = this.ownedGroups,
            mail = if (showContactInfo) this.mail else "",
            displayName = this.displayName,
            username = this.username,
            lastLoginAt = if (showDatePropertiesAndPermission) this.lastLoginAt else Instant.ofEpochSecond(0),
        )
    }
}
