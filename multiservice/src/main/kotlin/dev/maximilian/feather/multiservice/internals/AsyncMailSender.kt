/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals

import dev.maximilian.feather.multiservice.settings.SMTPSetting
import jakarta.mail.Message
import jakarta.mail.PasswordAuthentication
import jakarta.mail.Session
import jakarta.mail.Transport
import jakarta.mail.internet.AddressException
import jakarta.mail.internet.InternetAddress
import jakarta.mail.internet.MimeMessage
import mu.KotlinLogging
import java.util.Properties
import java.util.concurrent.LinkedBlockingQueue
import kotlin.concurrent.timer

data class Mail(
    val sender: InternetAddress,
    val receiver: Set<InternetAddress>,
    val subject: String,
    val body: String,
)

private data class QueuedMail(
    val mail: Mail,
    val failureCounter: Int,
)

// former named MailManager
internal class AsyncMailSender(val smtpSetting: SMTPSetting) {
    companion object {
        private val mailRegex = "(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])".toRegex()

        fun validateMail(
            mailAddress: String,
            personal: String,
        ) {
            InternetAddress(mailAddress, personal).validate()

            if (!mailRegex.matches(mailAddress)) {
                throw AddressException("This mail server does not support this mail format.")
            }
        }
    }

    private val mailQueue = LinkedBlockingQueue<QueuedMail>()

    private val mailAuthenticator =
        object : jakarta.mail.Authenticator() {
            override fun getPasswordAuthentication() = PasswordAuthentication(smtpSetting.username, smtpSetting.password)
        }.takeIf { smtpSetting.username.trim().isNotBlank() && smtpSetting.password.trim().isNotBlank() }
    private val mailProps =
        Properties().apply {
            this["mail.smtp.host"] = smtpSetting.host
            this["mail.smtp.port"] = smtpSetting.port
            this["mail.smtp.auth"] = smtpSetting.auth
            this["mail.smtp.starttls.enable"] = smtpSetting.startTLS
        }
    private val mailSession = Session.getInstance(mailProps, mailAuthenticator)

    private val logger = KotlinLogging.logger {}

    init {
        logger.info { "Mail manager uses mail host ${smtpSetting.host}:${smtpSetting.port}" }
        if (smtpSetting.auth != "false" && mailAuthenticator != null) {
            logger.info { "Mail authentication with user ${smtpSetting.username} (password length: ${smtpSetting.password})" }
        } else if (smtpSetting.auth != "false" || mailAuthenticator != null) {
            logger.error { "Inconsistent authentication properties" }
        } else {
            logger.info { "No mail authentication" }
        }
        timer(daemon = true, period = 30 * 1000L) { work() }

        Runtime.getRuntime().addShutdownHook(
            Thread {
                val size = mailQueue.size
                if (size > 0) {
                    logger.warn { "Losing $size queued messages" }
                }
            },
        )
    }

    private fun work() {
        if (mailQueue.peek() != null) {
            val queuedMail = mailQueue.take()
            try {
                val mail = queuedMail.mail

                val message = MimeMessage(mailSession)

                message.setFrom(mail.sender)
                message.setRecipients(Message.RecipientType.TO, mail.receiver.toTypedArray())
                message.subject = mail.subject
                message.setText(mail.body)

                Transport.send(message)

                logger.info { "Successfully sent mail with subject '${mail.subject}'" }
            } catch (e: Exception) {
                logger.error { "Failed to sent mail with subject '${queuedMail.mail.subject}' - Reason: '$e'" }
                // requeue mail message a second time (but not infinite times)
                if (queuedMail.failureCounter < 3) {
                    mailQueue.put(QueuedMail(queuedMail.mail, queuedMail.failureCounter + 1))
                }
            }
        }
    }

    fun submitMail(mail: Mail) {
        mailQueue.put(QueuedMail(mail, 0))
    }
}
