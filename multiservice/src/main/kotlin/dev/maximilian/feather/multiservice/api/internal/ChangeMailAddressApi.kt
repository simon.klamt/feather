/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.multiservice.internals.AsyncMailSender
import dev.maximilian.feather.multiservice.internals.FeatherMustache
import dev.maximilian.feather.multiservice.internals.Mail
import dev.maximilian.feather.multiservice.internals.SitenameData
import dev.maximilian.feather.multiservice.internals.connectors.ChangeMailAddressDatabaseConnector
import dev.maximilian.feather.multiservice.internals.connectors.InvitationDatabaseConnector
import dev.maximilian.feather.multiservice.internals.toMailChangeMailAddressData
import dev.maximilian.feather.multiservice.internals.toMailChangeMailAddressNotificationData
import dev.maximilian.feather.multiservice.internals.useroperations.ChangeMailAddress
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.NotFoundResponse
import io.javalin.http.bodyAsClass
import io.javalin.http.pathParamAsClass
import jakarta.mail.internet.AddressException
import jakarta.mail.internet.InternetAddress
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mu.KotlinLogging
import java.util.UUID

public data class ChangeMailAddressRequest(
    val password: String,
    val mail: String,
    val verified: Boolean,
)

internal class ChangeMailAddressApi(
    app: Javalin,
    private val credentialProvider: ICredentialProvider,
    private val services: List<IControllableService>,
    private val invitationDatabaseConnector: InvitationDatabaseConnector,
    private val changeMailAddressDatabaseConnector: ChangeMailAddressDatabaseConnector,
    private val asyncMailSender: AsyncMailSender,
    private val mailSettings: MultiServiceMailSettings,
) {
    private val logger = KotlinLogging.logger {}

    init {
        app.routes {
            path("/change_mail") {
                post("/step1/{id}", ::startChangeOfMailAddress)
                get("/step2/{token}", ::getChangeMailAddressToken)
                post("/step2/{token}", ::completeChangeOfMailAddress)
            }
        }
    }

    private fun startChangeOfMailAddress(ctx: Context) {
        val session = ctx.session()
        val changingUser = session.user

        val idFromPath = ctx.pathParamAsClass<Int>("id").getOrThrow { BadRequestResponse("User id needs to be an integer") }
        if (changingUser.id != idFromPath) {
            throw ForbiddenResponse()
        }

        val body = ctx.bodyAsClass<ChangeMailAddressRequest>()
        val newMail = body.mail.lowercase()

        if (credentialProvider.authenticateUserByUsernameOrMail(changingUser.username, body.password) == null) {
            throw ForbiddenResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Der Besitz der Mail-Adresse muss bestätigt werden"
        if (newMail.isEmpty()) errorMessages += "Eine E-Mail ist benötigt"
        if (newMail.length !in (5..100)) errorMessages += "Eine E-Mail benötigt mindestens 5 und maximal 100 Zeichen"

        try {
            AsyncMailSender.validateMail(newMail, "${changingUser.firstname} ${changingUser.surname}")
        } catch (e: AddressException) {
            val reason = e.toString()
            errorMessages += "Die angegebene E-Mail-Adresse ist nicht RFC822 konform (Grund: '$reason')"
        }

        if (newMail == changingUser.mail) errorMessages += "Die Mail-Adresse hat sich nicht geändert"
        if (credentialProvider.getUserByMail(newMail) != null) errorMessages += "Diese E-Mail Adresse ist bereits registriert"
        if (invitationDatabaseConnector.getInvitationByMail(credentialProvider, newMail) != null) errorMessages += "Diese E-Mail Adresse wurde bereits eingeladen"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not change mail address ${newMail} due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            ctx.status(201)

            GlobalScope.launch {
                logger.info("${changingUser.id} wants to change his mail address to: ${newMail}")

                val tokenData = changeMailAddressDatabaseConnector.createChangeMailAddress(changingUser, newMail, credentialProvider)
                val mailSubject =
                    FeatherMustache.MAIL_CHANGE_MAIL_ADDRESS_SUBJECT.execute(SitenameData(mailSettings.siteName))
                val mailDataNew = FeatherMustache.MAIL_CHANGE_MAIL_ADDRESS_CONTENT.execute(tokenData.toMailChangeMailAddressData())
                val mailDataOld =
                    FeatherMustache.MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION1.execute(
                        tokenData.toMailChangeMailAddressNotificationData(),
                    )

                // notification mail to old address
                asyncMailSender.submitMail(
                    Mail(
                        sender = mailSettings.senderAddress,
                        body = mailDataOld,
                        receiver = setOf(InternetAddress(changingUser.mail, "${changingUser.firstname} ${changingUser.surname}")),
                        subject = mailSubject,
                    ),
                )

                // mail with change link to new address for verification
                asyncMailSender.submitMail(
                    Mail(
                        sender = mailSettings.senderAddress,
                        body = mailDataNew,
                        receiver = setOf(InternetAddress(body.mail, "${changingUser.firstname} ${changingUser.surname}")),
                        subject = mailSubject,
                    ),
                )
            }
        }
    }

    private fun getChangeMailAddressToken(ctx: Context) {
        val uuid = ctx.pathParamAsClass<UUID>("{token}").getOrThrow { BadRequestResponse("token is not a valid uuid") }
        val token =
            uuid.let {
                changeMailAddressDatabaseConnector.getChangeMailAddress(it, credentialProvider)
            } ?: throw NotFoundResponse("$uuid not found")

        ctx.json(token)
    }

    private fun completeChangeOfMailAddress(ctx: Context) {
        val uuid = ctx.pathParamAsClass<UUID>("{token}").getOrThrow { BadRequestResponse("token is not a valid uuid") }
        val token =
            uuid.let {
                changeMailAddressDatabaseConnector.getChangeMailAddress(it, credentialProvider)
            } ?: throw NotFoundResponse("$uuid not found")

        // Sanity checks!
        val errorMessages = mutableListOf<String>()

        if (credentialProvider.getUserByMail(token.newMail) != null) errorMessages += "Diese E-Mail Adresse ist bereits registriert"
        if (invitationDatabaseConnector.getInvitationByMail(credentialProvider, token.newMail) != null) errorMessages += "Diese E-Mail Adresse wurde bereits eingeladen"

        if (errorMessages.isNotEmpty()) {
            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            logger.info { "Mail address of user ${token.user.id} successfully changed from ${token.user.mail} to ${token.newMail}" }

            val oldMail = token.user.mail

            val changeMailAddressService = ChangeMailAddress(credentialProvider, services)

            try {
                logger.info { "About to change mail address for user with id ${token.user.id} to ${token.newMail}" }

                changeMailAddressService.changeMailAddress(token.user, token.newMail)
            } catch (e: Exception) {
                logger.warn(e) { "Will not change mail address for user with id ${token.user.id} due to exception" }

                errorMessages += arrayOf("Interner Fehler (Benachrichtige die IT)")
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }

            changeMailAddressDatabaseConnector.removeChangeMailAddress(token)
            // CacheManager.refreshCacheAsync()
            ctx.status(204)

            GlobalScope.launch {
                val mailSubject =
                    FeatherMustache.MAIL_CHANGE_MAIL_ADDRESS_SUBJECT.execute(SitenameData(mailSettings.siteName))
                val mailData =
                    FeatherMustache.MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION2.execute(
                        token.toMailChangeMailAddressNotificationData(),
                    )

                // notification mail to old address
                asyncMailSender.submitMail(
                    Mail(
                        sender = mailSettings.senderAddress,
                        body = mailData,
                        receiver = setOf(InternetAddress(oldMail, "${token.user.firstname} ${token.user.surname}")),
                        subject = mailSubject,
                    ),
                )

                // notification mail to new address
                asyncMailSender.submitMail(
                    Mail(
                        sender = mailSettings.senderAddress,
                        body = mailData,
                        receiver = setOf(InternetAddress(token.newMail, "${token.user.firstname} ${token.user.surname}")),
                        subject = mailSubject,
                    ),
                )
            }
        }
    }
}
