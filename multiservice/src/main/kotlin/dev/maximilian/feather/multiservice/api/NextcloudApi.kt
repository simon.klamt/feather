/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api

import dev.maximilian.feather.Permission
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.NotFoundResponse
import io.javalin.http.pathParamAsClass

class NextcloudApi(app: Javalin, private val nextcloudService: NextcloudService) {
    init {
        app.get("/bindings/nextcloud/{userId}/quota", ::handleGetUserQuota)
    }

    private fun handleGetUserQuota(ctx: Context) {
        val session = ctx.session()
        val lookupUserId = ctx.pathParamAsClass<Int>("userId").getOrThrow { BadRequestResponse("userId needs to be numeric") }

        if (!session.user.permissions.contains(Permission.ADMIN) && session.user.id != lookupUserId) throw ForbiddenResponse()

        val lookupUser = nextcloudService.getNextcloudUser(lookupUserId) ?: throw NotFoundResponse("User $lookupUserId not found")

        ctx.json(lookupUser.quota)
    }
}
