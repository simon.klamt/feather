/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.multiservice.api.internal.AccountApi
import dev.maximilian.feather.multiservice.api.internal.ChangeMailAddressApi
import dev.maximilian.feather.multiservice.api.internal.ForgotApi
import dev.maximilian.feather.multiservice.api.internal.GroupApi
import dev.maximilian.feather.multiservice.api.internal.InvitationApi
import dev.maximilian.feather.multiservice.api.internal.MultiServicePlausibilityApi
import dev.maximilian.feather.multiservice.api.internal.MultiServiceUserApi
import dev.maximilian.feather.multiservice.api.internal.UserApi
import dev.maximilian.feather.multiservice.internals.AsyncMailSender
import dev.maximilian.feather.multiservice.internals.connectors.ChangeMailAddressDatabaseConnector
import dev.maximilian.feather.multiservice.internals.connectors.InvitationDatabaseConnector
import dev.maximilian.feather.multiservice.internals.connectors.LostPasswordDatabaseConnector
import dev.maximilian.feather.multiservice.settings.MultiServiceConfig
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import io.javalin.Javalin

class Multiservice(private val config: MultiServiceConfig, accountController: AccountController) {
    companion object {
        var multiServiceMailSettings: MultiServiceMailSettings? = null
    }

    init {
        multiServiceMailSettings = config.mailSettings
    }

    val services = mutableListOf<IControllableService>()

    private val invitationConnector = InvitationDatabaseConnector(config.db, accountController)
    private val lostPasswordConnector = LostPasswordDatabaseConnector(config.db, accountController)
    private val changeMailConnector = ChangeMailAddressDatabaseConnector(config.db, accountController)
    private val asyncMail = AsyncMailSender(config.mailSettings.smtp)

    fun startApis(app: Javalin) {
        InvitationApi(
            app,
            config.credentialProvider,
            services,
            config.callbackEvents,
            invitationConnector,
            config.backgroundJobManager,
            asyncMail,
            config.mailSettings,
            config.gdprController,
        )

        GroupApi(
            app,
            config.credentialProvider,
            config.callbackEvents.userMayAddChecks,
            config.callbackEvents.userRemovedFromGroupEvent,
            config.callbackEvents.userAdded,
        )

        UserApi(
            app,
            config.credentialProvider,
            services,
            config.callbackEvents.sessionTerminator,
            invitationConnector,
        )

        ForgotApi(
            app,
            config.credentialProvider,
            lostPasswordConnector,
            asyncMail,
            config.mailSettings,
        )

        ChangeMailAddressApi(
            app,
            config.credentialProvider,
            services,
            invitationConnector,
            changeMailConnector,
            asyncMail,
            config.mailSettings,
        )

        AccountApi(app, config.credentialProvider)

        MultiServiceUserApi(
            app,
            config.credentialProvider,
            services,
            config.callbackEvents,
            invitationConnector,
            config.backgroundJobManager,
            config.gdprController,
        )
        MultiServicePlausibilityApi(
            app,
            services,
            config.backgroundJobManager,
            config.credentialProvider,
            config.callbackEvents.checkUserEvent,
        )
    }
}
