/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.gdpr

import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.testutils.InMemoryExternalCredentialProvider
import dev.maximilian.feather.testutils.TestUser
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.assertDoesNotThrow
import java.sql.DriverManager
import java.time.Instant
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import kotlin.test.assertNull

/**
 * This class tests the low level logic of the gdpr database class
 */
class GdprControllerDatabaseTest {
    private val dbName = UUID.randomUUID().toString()
    private val database =
        Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$dbName;DB_CLOSE_DELAY=-1") })
    private val accountController =
        AccountController(database, InMemoryExternalCredentialProvider(), mutableMapOf("MIGRATE_OLD_IDS" to "42"))
    private val gdprDatabase = GdprDatabase(database, accountController)

    @Test
    fun `Create document returns created document`() {
        val document = generateTestGdprDocument()
        val created = gdprDatabase.createDocument(document)

        assertEquals(document, created.copy(id = 0, creationDate = document.creationDate))
    }

    @Test
    fun `Get not existing document returns null`() {
        assertNull(gdprDatabase.getDocument(-42))
    }

    @Test
    fun `Get document works`() {
        val created = gdprDatabase.createDocument(generateTestGdprDocument())
        val returned = gdprDatabase.getDocument(created.id)

        assertNotNull(returned)
        assertEquals(created, returned)
    }

    @Test
    fun `Get latest document id works`() {
        val documents = (0 until 4).map { gdprDatabase.createDocument(generateTestGdprDocument()) }
        val returned = gdprDatabase.getLatestDocumentId()

        assertNotNull(returned)
        assertEquals(documents.last().id, returned)
    }

    @Test
    fun `Get all documents works`() {
        val documents = (0 until 4).map { gdprDatabase.createDocument(generateTestGdprDocument()) }
        val returned =
            gdprDatabase
                .getAllDocuments()
                .filter { documents.any { d -> it.id == d.id } }

        assertNotNull(returned)
        assertEquals(documents.toSet(), returned.toSet())
    }

    @Test
    fun `Store acceptance and get self acceptance works`() {
        val document = gdprDatabase.createDocument(generateTestGdprDocument())
        val user = accountController.createUser(TestUser.generateTestUser())

        val returnedAcceptance = gdprDatabase.storeAcceptanceInfo(document, user)
        val storedAcceptance = gdprDatabase.getAcceptancesOf(document, user)

        assertEquals(returnedAcceptance, storedAcceptance)
    }

    @Test
    fun `Get document acceptances works`() {
        val document = gdprDatabase.createDocument(generateTestGdprDocument())
        val users = (0 until 5).map { accountController.createUser(TestUser.generateTestUser()) }

        users.forEach { gdprDatabase.storeAcceptanceInfo(document, it) }

        val result = gdprDatabase.getAcceptancesOf(document, users.associateBy { it.id })

        assertEquals(
            users.map { GdprAcceptance(it, Instant.ofEpochSecond(0), document, GdprAcceptanceType.ACCEPTED) },
            result.map { it.copy(timestamp = Instant.ofEpochSecond(0)) },
        )

        require(result.all { it.timestamp > Instant.now().minusSeconds(10) })
        require(result.all { it.timestamp < Instant.now() })
    }

    @Test
    fun `Accept not existing document throws`() {
        val document = generateTestGdprDocument(id = -42)
        val exception =
            assertFailsWith<IllegalArgumentException> {
                gdprDatabase.storeAcceptanceInfo(
                    document,
                    accountController.createUser(TestUser.generateTestUser()),
                )
            }

        assertEquals("Gdpr document with id ${document.id} does not exist", exception.message)
    }

    @Test
    fun `Delete user mapping works`() {
        val document = gdprDatabase.createDocument(generateTestGdprDocument())
        val user = accountController.createUser(TestUser.generateTestUser())

        gdprDatabase.storeAcceptanceInfo(document, user)

        assert(gdprDatabase.getAcceptancesOf(document, listOf(user).associateBy { it.id }).any { it.user == user })

        gdprDatabase.deleteUserMapping(user)

        assert(gdprDatabase.getAcceptancesOf(document, listOf(user).associateBy { it.id }).none { it.user == user })
    }

    @Test
    fun `Test consistency check`() {
        val document1 = generateTestGdprDocument(forceDate = Instant.now().minusSeconds(1))
        val exception =
            assertFailsWith<IllegalArgumentException> {
                gdprDatabase.createDocument(document1)
            }

        assertEquals("The force date needs to be in the future", exception.message)

        val document2 = generateTestGdprDocument(validDate = Instant.now().minusSeconds(1))
        val exception2 =
            assertFailsWith<IllegalArgumentException> {
                gdprDatabase.createDocument(document2)
            }

        assertEquals("The force date needs to be before the valid date", exception2.message)
    }

    @Test
    fun `Test accept multiple times the same document`() {
        val document = gdprDatabase.createDocument(generateTestGdprDocument())
        val user = accountController.createUser(TestUser.generateTestUser())

        gdprDatabase.storeAcceptanceInfo(document, user)

        assertDoesNotThrow { gdprDatabase.storeAcceptanceInfo(document, user) }

        val storedAcceptance = gdprDatabase.getAcceptancesOf(document, listOf(user).associateBy { it.id }).firstOrNull { it.user == user }
        assertNotNull(storedAcceptance)
    }
}
