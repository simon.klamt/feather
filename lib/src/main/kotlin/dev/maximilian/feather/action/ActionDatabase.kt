package dev.maximilian.feather.action

import dev.maximilian.feather.account.AccountController
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

internal class ActionDatabase(
    private val db: Database,
    accountController: AccountController,
) {
    private val actionTable = ActionTable(accountController)

    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(actionTable)
        }
    }

    internal fun insertAction(action: Action<*>) {
        transaction(db) {
            actionTable.insert {
                it[this.userId] = action.userId
                it[this.actionName] = action.name
                it[this.payload] = action.payloadToString()
            }
        }
    }

    internal fun insertActions(actions: List<Action<*>>) {
        transaction(db) {
            actionTable.batchInsert(actions, shouldReturnGeneratedValues = false) {
                this[actionTable.userId] = it.userId
                this[actionTable.actionName] = it.name
                this[actionTable.payload] = it.payloadToString()
            }
        }
    }

    internal fun getActionsByUser(
        userId: Int,
        producer: (String, Int, String) -> Action<*>,
    ) = transaction(db) {
        actionTable.select { actionTable.userId eq userId }.map {
            producer(it[actionTable.actionName], it[actionTable.userId].value, it[actionTable.payload])
        }
    }

    internal fun getOpenActions(producer: (String, Int, String) -> Action<*>): Map<Int, List<Action<*>>> =
        transaction(db) {
            actionTable.selectAll().map {
                it[actionTable.userId].value to
                    producer(it[actionTable.actionName], it[actionTable.userId].value, it[actionTable.payload])
            }.groupBy { it.first }.map { it.key to it.value.map { v -> v.second } }.toMap()
        }

    internal fun getActionsByName(
        actionName: String,
        producer: (String, Int, String) -> Action<*>,
    ) = transaction(db) {
        actionTable.select { actionTable.actionName eq actionName }.map {
            producer(it[actionTable.actionName], it[actionTable.userId].value, it[actionTable.payload])
        }
    }

    internal fun deleteActionsByName(actionName: String) {
        transaction(db) {
            actionTable.deleteWhere { actionTable.actionName eq actionName }
        }
    }

    internal fun deleteActionsByNameAndUser(
        actionName: String,
        userId: Int,
    ) {
        transaction(db) {
            actionTable.deleteWhere { actionTable.actionName eq actionName and (actionTable.userId eq userId) }
        }
    }

    internal fun deleteActionsByNameUserAndPayload(
        actionName: String,
        userId: Int,
        payload: String,
    ) {
        transaction(db) {
            actionTable.deleteWhere { actionTable.actionName eq actionName and (actionTable.userId eq userId) and (actionTable.payload eq payload) }
        }
    }

    internal fun hasUserActions(userId: Int): Boolean =
        transaction(db) {
            actionTable.slice(actionTable.userId).select { actionTable.userId eq userId }.limit(1).count() != 0L
        }

    internal fun getUsersWithOpenActions(): List<Int> =
        transaction(db) {
            actionTable.slice(actionTable.userId).selectAll().map { it[actionTable.userId].value }
        }

    private class ActionTable(accountController: AccountController) : IntIdTable("actions") {
        val userId = reference("user_id", accountController.userIdColumn, onDelete = ReferenceOption.CASCADE)
        val actionName = varchar("action_name", 50)
        val payload = text("payload")
    }
}
