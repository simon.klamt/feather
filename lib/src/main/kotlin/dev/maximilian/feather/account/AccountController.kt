/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.account

import dev.maximilian.feather.ExternalGroup
import dev.maximilian.feather.ExternalUser
import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.IExternalCredentialProvider
import dev.maximilian.feather.User
import mu.KotlinLogging
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import java.time.Instant

public class AccountController(
    db: Database,
    public val externalUserProvider: IExternalCredentialProvider,
    properties: MutableMap<String, String>,
) : ICredentialProvider {
    private val logger = KotlinLogging.logger { }

    private val accountDatabase = AccountDatabase(db)
    public val userIdColumn: Column<EntityID<Int>> = accountDatabase.userIdColumn
    private val accountMigration = AccountMigration(db, accountDatabase, properties)

    init {
        syncLdap()
        accountMigration.checkAndDoMigration(externalUserProvider.getPermissions())
    }

    /**
     * This method deletes all users from database, which were also deleted from credential provider
     * In case a user id has changed in the credential provider, but username and email are the same, the entry is corrected
     * This method returns the set of dbUsers without the deleted and with the corrected ones
     */
    private fun fixOrDeleteUsers(
        dbUsers: Set<User>,
        credentialProviderUsers: Set<ExternalUser>,
    ): Set<User> {
        val credentialProviderUsersById: Map<String, ExternalUser> =
            credentialProviderUsers.associateBy { it.externalId }
        val dbUsersByCredentialProviderId: Map<String, User> = accountDatabase.associateByExternalIds(dbUsers)

        val notAvailableAnymore: List<User> =
            (dbUsersByCredentialProviderId.keys - credentialProviderUsersById.keys).mapNotNull { dbUsersByCredentialProviderId[it] }
        val fixMap =
            notAvailableAnymore.map { u -> u to credentialProviderUsers.firstOrNull { c -> u.username == c.username && u.mail == c.mail } }

        fixMap.filter { it.second == null }.map { it.first }.forEach {
            logger.info { "Deleting user ${it.id} - ${it.displayName}" }
            accountDatabase.deleteUser(it)
        }

        // Return the dbUsers with all changes made to the set in the database in this method
        return (
            dbUsers.filter { fixMap.none { f -> it.id == f.first.id } } +
                fixMap.mapNotNull { it.second?.let { u -> it.first to u } }
                    .map {
                        accountDatabase.updateExternalProviderId(it.first, it.second.externalId).also { _ ->
                            logger.warn { "Fixing credential provider id of user with username \"${it.first.username}\", now: ${it.second.externalId}" }
                        }
                    }
            ).toSet()
    }

    /**
     * This method deletes all groups from database, which were also deleted from credential provider
     * In case a group id has changed in the credential provider, but name is the same, the entry is corrected
     * This method returns the set of dbGroups without the deleted and with the corrected ones
     */
    private fun fixOrDeleteGroups(
        dbGroups: Set<Group>,
        credentialProviderGroups: Set<ExternalGroup>,
    ): Set<Group> {
        val credentialProviderGroupsById: Map<String, ExternalGroup> =
            credentialProviderGroups.associateBy { it.externalId }
        val dbGroupsByCredentialProviderId: Map<String, Group> = accountDatabase.associateByExternalIds(dbGroups)

        val notAvailableAnymore: List<Group> =
            (dbGroupsByCredentialProviderId.keys - credentialProviderGroupsById.keys).mapNotNull { dbGroupsByCredentialProviderId[it] }
        val fixMap =
            notAvailableAnymore.map { u -> u to credentialProviderGroups.firstOrNull { c -> u.name == c.name } }

        fixMap.filter { it.second == null }.map { it.first }.forEach {
            logger.info { "Deleting group ${it.id} - ${it.name}" }
            accountDatabase.deleteGroup(it)
        }

        // Return the dbUsers with all changes made to the set in the database in this method
        return (
            dbGroups.filter { fixMap.none { f -> it.id == f.first.id } } +
                fixMap.mapNotNull { it.second?.let { u -> it.first to u } }
                    .map {
                        accountDatabase.updateExternalProviderId(it.first, it.second.externalId).also { _ ->
                            logger.warn { "Fixing credential provider id of group with name \"${it.first.name}\", now: ${it.second.externalId}" }
                        }
                    }
            ).toSet()
    }

    /**
     * This method syncs all new users from credential provider into our database
     * This does not sync groups! Only the user data
     */
    private fun syncNewUsers(
        dbUser: Set<User>,
        credentialProviderUsers: Set<ExternalUser>,
    ): Set<User> {
        val dbUsersByCredentialProviderId: Map<String, User> = accountDatabase.associateByExternalIds(dbUser)
        val userIdsToCreate = credentialProviderUsers.map { it.externalId } - dbUsersByCredentialProviderId.keys

        return (
            credentialProviderUsers.filter { it.externalId in userIdsToCreate }.map { u ->
                accountDatabase.createUser(
                    User(
                        id = 0,
                        username = u.username,
                        displayName = u.displayName,
                        firstname = u.firstname,
                        surname = u.surname,
                        mail = u.mail,
                        groups = emptySet(),
                        ownedGroups = emptySet(),
                        registeredSince = u.registeredSince,
                        permissions = emptySet(),
                        disabled = u.disabled,
                        lastLoginAt = Instant.ofEpochSecond(0),
                    ),
                    u.externalId,
                )
            } + dbUser
            ).toSet()
    }

    /**
     * This method syncs all new groups from credential provider into our database
     * This does not sync memberships, since we first need to ensure that all groups where we depend on are created!
     */
    private fun syncNewGroups(
        dbGroups: Set<Group>,
        credentialProviderGroups: Set<ExternalGroup>,
    ): Set<Group> {
        val dbGroupsByCredentialProviderId: Map<String, Group> = accountDatabase.associateByExternalIds(dbGroups)
        val groupIdsToCreate = credentialProviderGroups.map { it.externalId } - dbGroupsByCredentialProviderId.keys

        return (
            credentialProviderGroups.filter { it.externalId in groupIdsToCreate }.map { g ->
                accountDatabase.createGroup(
                    Group(
                        id = 0,
                        name = g.name,
                        description = g.description,
                        userMembers = emptySet(),
                        groupMembers = emptySet(),
                        parentGroups = emptySet(),
                        owners = emptySet(),
                        ownerGroups = emptySet(),
                        ownedGroups = emptySet(),
                    ),
                    g.externalId,
                )
            } + dbGroups
            ).toSet()
    }

    /**
     * This method syncs all user attributes to our database
     * The group memberships is no user attribute and thus not gets synced by this method!!
     */
    private fun syncUserAttributeChanges(
        dbUser: Set<User>,
        credentialProviderUsers: Set<ExternalUser>,
    ): Map<User, ExternalUser> {
        val dbUsersByCredentialProviderId: Map<String, User> = accountDatabase.associateByExternalIds(dbUser)
        val credentialProviderUsersMap: Map<String, ExternalUser> =
            credentialProviderUsers.associateBy { it.externalId }

        if (dbUsersByCredentialProviderId.size != credentialProviderUsersMap.size) {
            logger.warn {
                "A user defined in database can not be found in ldap, but the sync methods before were called correctly, this smells like a bug."
            }
        }

        // At this point, |dbUser| = |credentialProviderUsers| and a bijection can be built.
        // If this is not the point, the sync methods called before this method were faulty
        // or ldap was modified in parallel
        return dbUsersByCredentialProviderId.mapNotNull { (externalId, u) ->
            credentialProviderUsersMap[externalId]?.let {
                u to it
            }.also {
                if (it == null) {
                    logger.warn {
                        "A user defined in database can not be found in ldap, but the sync methods before were called correctly, this smells like a bug. The not found external id is: $externalId"
                    }
                }
            }
        }.associate { (u, e) ->
            val updatedUser =
                if (u.equalsExternalInBasicData(e)) {
                    u
                } else {
                    accountDatabase.updateUser(
                        u.copy(
                            username = e.username,
                            displayName = e.displayName,
                            firstname = e.firstname,
                            surname = e.surname,
                            mail = e.mail,
                            disabled = e.disabled,
                            registeredSince = e.registeredSince,
                        ),
                    )
                }

            updatedUser to e
        }
    }

    /**
     * This method syncs all group attributes to our database
     * The group memberships not gets synced by this method!!
     */
    private fun syncGroupAttributeChanges(
        dbGroups: Set<Group>,
        credentialProviderGroups: Set<ExternalGroup>,
    ): Map<Group, ExternalGroup> {
        val dbGroupsByCredentialProviderId: Map<String, Group> = accountDatabase.associateByExternalIds(dbGroups)
        val credentialProviderGroupsMap: Map<String, ExternalGroup> =
            credentialProviderGroups.associateBy { it.externalId }

        if (dbGroupsByCredentialProviderId.size != credentialProviderGroupsMap.size) {
            logger.warn {
                "A group defined in database can not be found in ldap, but the sync methods before were called correctly, this smells like a bug."
            }
        }

        // At this point, |dbGroups| = |credentialProviderGroups| and a bijection can be built.
        // If this is not the point, the sync methods called before this method were faulty
        // or ldap was modified in parallel
        return dbGroupsByCredentialProviderId.mapNotNull { (externalId, u) ->
            credentialProviderGroupsMap[externalId]?.let {
                u to it
            }.also {
                if (it == null) {
                    logger.warn {
                        "A group defined in database can not be found in ldap, but the sync methods before were called correctly, this smells like a bug. The not found external id is: $externalId"
                    }
                }
            }
        }.associate { (u, e) ->
            val updatedGroup =
                if (u.equalsExternalInBasicData(e)) {
                    u
                } else {
                    accountDatabase.updateGroup(
                        u.copy(
                            name = e.name,
                            description = e.description,
                        ),
                    )
                }

            updatedGroup to e
        }
    }

    private fun syncMemberships(
        users: Map<User, ExternalUser>,
        groups: Map<Group, ExternalGroup>,
    ) {
        val userIdExternalIdMap: Map<Int, String> = users.map { (u, e) -> u.id to e.externalId }.toMap()
        val groupIdExternalIdMap: Map<Int, String> = groups.map { (u, e) -> u.id to e.externalId }.toMap()

        val userExternalIdIdMap: Map<String, Int> = users.map { (u, e) -> e.externalId to u.id }.toMap()
        val groupExternalIdIdMap: Map<String, Int> = groups.map { (u, e) -> e.externalId to u.id }.toMap()

        groups.forEach { (g, e) ->
            if (!g.equalsExternalMemberships(e, userIdExternalIdMap, groupIdExternalIdMap)) {
                accountDatabase.updateGroup(
                    g.copy(
                        userMembers = e.userMembers.mapNotNull { userExternalIdIdMap[it] }.toSet(),
                        owners = e.owners.mapNotNull { userExternalIdIdMap[it] }.toSet(),
                        parentGroups = e.parentGroups.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
                        groupMembers = e.groupMembers.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
                        ownerGroups = e.ownerGroups.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
                        ownedGroups = e.ownedGroups.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
                    ),
                )
            }
        }
    }

    /** Syncs the user images from the credential provider to the local database.
     */
    private fun syncImages(users: Map<User, ExternalUser>) {
        users.forEach { (user, externalUser) ->
            val newImage = externalUserProvider.getPhotoForUser(externalUser.externalId)
            val oldImage = accountDatabase.getPhotoForUser(user)
            if (newImage == null) {
                if (oldImage != null) accountDatabase.deletePhotoForUser(user)
            } else if (!oldImage.contentEquals(newImage)) {
                accountDatabase.updatePhotoForUser(user, newImage)
            }
        }
    }

    /**
     * Syncs users from credential provider to local database
     * The credential provider is the single source of truth
     * But possible the id at the credential provider changes (Yes, this case happens)
     * As a result we check if users are not in the credential provider whether there is a user in the credential provider with same mail and user id
     */
    private fun syncLdap() {
        val (credentialProviderUsers, credentialProviderGroups) =
            externalUserProvider.getUsersAndGroups()
                .let { it.first.toSet() to it.second.toSet() }

        // Sync all users
        val dbUsers = fixOrDeleteUsers(accountDatabase.getUsers().toSet(), credentialProviderUsers)
        val dbWithCreatedUsers = syncNewUsers(dbUsers, credentialProviderUsers)
        val dbWithSyncedUsers = syncUserAttributeChanges(dbWithCreatedUsers, credentialProviderUsers)

        // Sync all groups
        val dbGroups = fixOrDeleteGroups(accountDatabase.getGroups().toSet(), credentialProviderGroups)
        val dbWithCreatedGroups = syncNewGroups(dbGroups, credentialProviderGroups)
        val dbWithSyncedGroups = syncGroupAttributeChanges(dbWithCreatedGroups, credentialProviderGroups)

        // And finally sync memberships
        syncMemberships(dbWithSyncedUsers, dbWithSyncedGroups)

        // Sync images
        syncImages(dbWithSyncedUsers)
    }

    override fun getUser(id: Int): User? = accountDatabase.getUserById(id)

    override fun getUserByExternalId(id: String): User? = accountDatabase.getUserByExternalId(id)

    override fun getUserExternalId(user: User): String? = accountDatabase.getUserExternalId(user)

    override fun getUserByMail(mail: String): User? = accountDatabase.getUserByMail(mail)

    override fun getUserByUsername(username: String): User? = accountDatabase.getUserByUsername(username)

    override fun getPhotoForUser(user: User): ByteArray? = accountDatabase.getPhotoForUser(user)

    override fun updatePhotoForUser(
        user: User,
        image: ByteArray,
    ) {
        accountDatabase.getExternalProviderId(user)?.let {
            externalUserProvider.updatePhotoForUser(it, image)
        }
        accountDatabase.updatePhotoForUser(user, image)
    }

    override fun deletePhotoForUser(user: User) {
        accountDatabase.getExternalProviderId(user)?.let {
            externalUserProvider.deletePhotoForUser(it)
        }
        accountDatabase.deletePhotoForUser(user)
    }

    override fun getUsers(): Collection<User> = accountDatabase.getUsers()

    override fun createUser(
        user: User,
        password: String?,
    ): User =
        externalUserProvider.createUser(user.toExternalUser("", accountDatabase.getGroupIdExternalIdMap()), password)
            .let { createdUser ->
                accountDatabase.createUser(
                    user.mergeExternalUser(
                        createdUser,
                        accountDatabase.getGroupExternalIdIdMap(),
                    ),
                    createdUser.externalId,
                )
            }

    override fun deleteUser(user: User) {
        accountDatabase.getExternalProviderId(user)?.also(externalUserProvider::deleteUser)
        accountDatabase.deleteUser(user)
    }

    override fun updateUser(user: User): User? {
        val externalId = accountDatabase.getExternalProviderId(user) ?: return null
        val externalUpdated =
            externalUserProvider.updateUser(user.toExternalUser(externalId, accountDatabase.getGroupIdExternalIdMap()))

        return if (externalUpdated == null) {
            // External user does not exist anymore, to be consistent, remove internal user also
            accountDatabase.deleteUser(user)
            null
        } else {
            accountDatabase.updateUser(user)
        }
    }

    override fun authenticateUserByUsernameOrMail(
        usernameOrMail: String,
        password: String,
    ): User? {
        val authenticatedId =
            externalUserProvider.authenticateUserByUsernameOrMail(usernameOrMail, password) ?: return null

        return accountDatabase.getUserByExternalId(authenticatedId).let {
            if (it != null) {
                it
            } else {
                val externalUser = externalUserProvider.getUserById(authenticatedId) ?: return null
                accountDatabase.createUser(
                    User(
                        id = 0,
                        username = externalUser.username,
                        displayName = externalUser.displayName,
                        firstname = externalUser.firstname,
                        surname = externalUser.surname,
                        mail = externalUser.mail,
                        groups = externalUser.groups.map { 0 }.toSet(),
                        ownedGroups = externalUser.ownedGroups.map { 0 }.toSet(),
                        registeredSince = externalUser.registeredSince,
                        permissions = emptySet(),
                        disabled = externalUser.disabled,
                        lastLoginAt = Instant.ofEpochSecond(0),
                    ),
                    authenticatedId,
                ).apply {
                    accountDatabase.updateLastLoginTimestamp(this)
                }
            }
        }
    }

    override fun updateUserPassword(
        user: User,
        password: String,
    ): Boolean =
        accountDatabase.getExternalProviderId(user)?.let { externalUserProvider.updateUserPassword(it, password) }
            ?: false

    override fun updateLastLoginOf(user: User): User? = accountDatabase.updateLastLoginTimestamp(user)

    override fun getGroup(id: Int): Group? = accountDatabase.getGroup(id)

    override fun getGroupByName(groupname: String): Group? = accountDatabase.getGroupByName(groupname)

    override fun getGroups(): Collection<Group> = accountDatabase.getGroups()

    override fun createGroup(group: Group): Group =
        externalUserProvider.createGroup(
            group.toExternalGroup(
                "",
                accountDatabase.getUserIdExternalIdMap(),
                accountDatabase.getGroupIdExternalIdMap(),
            ),
        ).let { createdGroup ->
            accountDatabase.createGroup(
                group.mergeExternalGroup(
                    createdGroup,
                    accountDatabase.getUserExternalIdIdMap(),
                    accountDatabase.getGroupExternalIdIdMap(),
                ),
                createdGroup.externalId,
            )
        }

    override fun deleteGroup(group: Group) {
        accountDatabase.getExternalProviderId(group)?.also(externalUserProvider::deleteGroup)
        accountDatabase.deleteGroup(group)
    }

    override fun updateGroup(group: Group): Group? {
        val externalId = accountDatabase.getExternalProviderId(group) ?: return null
        val externalUpdated =
            externalUserProvider.updateGroup(
                group.toExternalGroup(
                    externalId,
                    accountDatabase.getUserIdExternalIdMap(),
                    accountDatabase.getGroupIdExternalIdMap(),
                ),
            )

        return if (externalUpdated == null) {
            // External user does not exist anymore, to be consistent, remove internal user also
            accountDatabase.deleteGroup(group)
            null
        } else {
            accountDatabase.updateGroup(group)
        }
    }

    override fun getUsersAndGroups(): Pair<Collection<User>, Collection<Group>> = getUsers() to getGroups()

    override fun close() {
        externalUserProvider.close()
    }

    private fun User.toExternalUser(
        externalId: String,
        groupIdExternalIdMap: Map<Int, String>,
    ) = ExternalUser(
        externalId = externalId,
        username = username,
        displayName = displayName,
        firstname = firstname,
        surname = surname,
        mail = mail,
        groups = groups.mapNotNull { groupIdExternalIdMap[it] }.toSet(),
        registeredSince = registeredSince,
        ownedGroups = ownedGroups.mapNotNull { groupIdExternalIdMap[it] }.toSet(),
        disabled = disabled,
    )

    private fun User.mergeExternalUser(
        externalUser: ExternalUser,
        groupExternalIdIdMap: Map<String, Int>,
    ) = copy(
        username = externalUser.username,
        displayName = externalUser.displayName,
        firstname = externalUser.firstname,
        surname = externalUser.surname,
        mail = externalUser.mail,
        groups = externalUser.groups.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
        ownedGroups = externalUser.ownedGroups.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
        disabled = externalUser.disabled,
        registeredSince = externalUser.registeredSince,
    )

    private fun Group.toExternalGroup(
        externalId: String,
        userIdExternalIdMap: Map<Int, String>,
        groupIdExternalIdMap: Map<Int, String>,
    ) = ExternalGroup(
        externalId = externalId,
        name = name,
        description = description,
        userMembers = userMembers.mapNotNull { userIdExternalIdMap[it] }.toSet(),
        groupMembers = groupMembers.mapNotNull { groupIdExternalIdMap[it] }.toSet(),
        parentGroups = parentGroups.mapNotNull { groupIdExternalIdMap[it] }.toSet(),
        owners = owners.mapNotNull { userIdExternalIdMap[it] }.toSet(),
        ownerGroups = ownerGroups.mapNotNull { groupIdExternalIdMap[it] }.toSet(),
        ownedGroups = ownedGroups.mapNotNull { groupIdExternalIdMap[it] }.toSet(),
    )

    private fun Group.mergeExternalGroup(
        externalGroup: ExternalGroup,
        userExternalIdIdMap: Map<String, Int>,
        groupExternalIdIdMap: Map<String, Int>,
    ) = copy(
        name = externalGroup.name,
        description = externalGroup.description,
        userMembers = externalGroup.userMembers.mapNotNull { userExternalIdIdMap[it] }.toSet(),
        groupMembers = externalGroup.groupMembers.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
        parentGroups = externalGroup.parentGroups.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
        owners = externalGroup.owners.mapNotNull { userExternalIdIdMap[it] }.toSet(),
        ownerGroups = externalGroup.ownerGroups.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
        ownedGroups = externalGroup.ownedGroups.mapNotNull { groupExternalIdIdMap[it] }.toSet(),
    )

    private fun User.equalsExternalInBasicData(externalUser: ExternalUser): Boolean =
        this.username == externalUser.username && this.displayName == externalUser.displayName && this.firstname == externalUser.firstname && this.surname == externalUser.surname && this.mail == externalUser.mail && this.disabled == externalUser.disabled && this.registeredSince == externalUser.registeredSince

    private fun Group.equalsExternalInBasicData(externalGroup: ExternalGroup): Boolean =
        this.name == externalGroup.name && this.description == externalGroup.description

    private fun Group.equalsExternalMemberships(
        externalGroup: ExternalGroup,
        userIdExternalIdMap: Map<Int, String>,
        groupIdExternalIdMap: Map<Int, String>,
    ): Boolean =
        this.name == externalGroup.name && this.description == externalGroup.description && this.userMembers.mapNotNull { userIdExternalIdMap[it] }
            .toSet() == externalGroup.userMembers && this.owners.mapNotNull { userIdExternalIdMap[it] }
            .toSet() == externalGroup.owners && this.groupMembers.mapNotNull { groupIdExternalIdMap[it] }
            .toSet() == externalGroup.groupMembers && this.parentGroups.mapNotNull { groupIdExternalIdMap[it] }
            .toSet() == externalGroup.parentGroups && this.ownerGroups.mapNotNull { groupIdExternalIdMap[it] }
            .toSet() == externalGroup.ownerGroups && this.ownedGroups.mapNotNull { groupIdExternalIdMap[it] }
            .toSet() == externalGroup.ownedGroups
}
