package dev.maximilian.feather

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.exists
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.UUID
import kotlin.math.max

public object TableUserGroupIdMigrator {
    private val logger = KotlinLogging.logger { }

    public fun migrateTable(
        db: Database,
        tableName: String,
        tableIdColumn: String,
        userIdColumnName: String,
        userExternalIdIdMap: Map<String, Int>,
        dryRun: Boolean = true,
    ) {
        transaction(db) {
            if (!Table(tableName).exists()) {
                logger.warn { "Cannot migrate ids of $tableName because table does not exist" }
                return@transaction
            }

            // This is repeatable in case something brake while migrating
            exec("ALTER TABLE \"$tableName\" ADD COLUMN IF NOT EXISTS \"${userIdColumnName}_temp\" integer")
            val idOldId: Map<Any, String>? =
                exec("SELECT \"$tableIdColumn\", \"$userIdColumnName\", \"${userIdColumnName}_temp\" FROM $tableName") {
                    val resultMap = mutableMapOf<Any, String>()
                    while (it.next()) {
                        resultMap[it.getObject(tableIdColumn)] = it.getString(userIdColumnName)
                    }

                    resultMap
                }

            if (idOldId == null) {
                logger.error { "Cannot migrate ids of $tableName because no result set was found" }
                return@transaction
            }

            val resultBatch =
                idOldId.map { (tableId, oldUserId) ->
                    val tableIdString =
                        when (tableId) {
                            is Int -> "$tableId"
                            is String, is UUID -> "'$tableId'"
                            else -> {
                                logger.error { "Cannot migrate ids of $tableName because the table id column is neither integer nor string" }
                                return@transaction
                            }
                        }

                    "UPDATE \"$tableName\" SET \"${userIdColumnName}_temp\" = ${oldUserId.toIntOrNull() ?: userExternalIdIdMap[oldUserId] ?: "null"} WHERE \"$tableIdColumn\" = $tableIdString"
                }

            execInBatch(resultBatch)

            if (!dryRun) {
                exec("DELETE FROM \"$tableName\" WHERE \"${userIdColumnName}_temp\" IS NULL")
                exec("ALTER TABLE \"$tableName\" DROP COLUMN \"${userIdColumnName}\"")
                exec("ALTER TABLE \"$tableName\" RENAME COLUMN \"${userIdColumnName}_temp\" TO \"${userIdColumnName}\"")
            }
        }
    }

    public fun migrateInviteTable(
        db: Database,
        userExternalIdIdMap: Map<String, Int>,
        groupExternalIdIdMap: Map<String, Int>,
        dryRun: Boolean = true,
    ) {
        migrateTable(db, "invites", "id", "inviterId", userExternalIdIdMap, dryRun)

        val mapper = jacksonObjectMapper()

        transaction(db) {
            if (!Table("invites").exists()) {
                logger.warn { "Cannot migrate invite table because table does not exist" }
                return@transaction
            }

            // This is repeatable in case something brake while migrating
            exec("ALTER TABLE \"invites\" ADD COLUMN IF NOT EXISTS \"groups_temp\" jsonb")
            val idOldId: Map<Any, Set<String>>? =
                exec("SELECT \"groups\", \"id\", \"groups_temp\" FROM invites") {
                    val resultMap = mutableMapOf<Any, Set<String>>()

                    while (it.next()) {
                        resultMap[it.getObject("id")] = mapper.readValue(it.getString("groups"))
                    }

                    resultMap
                }

            if (idOldId == null) {
                logger.error { "Cannot migrate ids of invites because no result set was found" }
                return@transaction
            }

            val resultBatch =
                idOldId.map { (tableId, oldUserId) ->
                    val tableIdString =
                        when (tableId) {
                            is Int -> "$tableId"
                            is String, is UUID -> "'$tableId'"
                            else -> {
                                logger.error { "Cannot migrate ids of invites because the table id column is neither integer nor string" }
                                return@transaction
                            }
                        }

                    "UPDATE \"invites\" SET \"groups_temp\" = '${oldUserId.mapNotNull { groupExternalIdIdMap[it] }.let {
                        mapper.writeValueAsString(
                            it,
                        )
                    }}' WHERE \"id\" = $tableIdString"
                }

            execInBatch(resultBatch)

            if (!dryRun) {
                exec("ALTER TABLE \"invites\" DROP COLUMN \"groups\"")
                exec("ALTER TABLE \"invites\" RENAME COLUMN \"groups_temp\" TO \"groups\"")
            }
        }
    }

    public fun migrateTrialAndSupportMemberTable(
        db: Database,
        userExternalIdIdMap: Map<String, Int>,
        groupIdExternalMap: Map<String, Int>,
        dryRun: Boolean = true,
    ) {
        migrateTable(db, "trial", "ldapID", "ldapID", userExternalIdIdMap, false)
        migrateTable(db, "dkpusermapping", "ldapID", "ldapID", userExternalIdIdMap, false)

        if (!dryRun) {
            transaction(db) {
                val tables = listOf<Table>(Table("trial"), Table("supportmember"), Table("dkpusermapping"))
                if (tables.any { !it.exists() }) {
                    logger.warn { "Cannot migrate support membership tables because some table(s) do(es) not exist" }
                    return@transaction
                }

                exec("ALTER TABLE \"trial\" DROP COLUMN IF EXISTS \"id\"")
                exec("ALTER TABLE \"supportmember\" DROP COLUMN IF EXISTS \"id\"")
                exec("ALTER TABLE \"dkpusermapping\" DROP COLUMN IF EXISTS \"id\"")

                exec(
                    "CREATE SEQUENCE IF NOT EXISTS supportmember_externalID_seq START ${max(userExternalIdIdMap.values.maxOrNull() ?: 0, groupIdExternalMap.values.maxOrNull() ?: 0) + 1}",
                )
                exec(
                    "ALTER TABLE IF EXISTS public.supportmember ALTER COLUMN \"externalID\" SET DEFAULT nextval('\"supportmember_externalid_seq\"'::regclass)",
                )
            }
        }
    }
}
