/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather

public class PermissionException(user: User?, action: String, requiredPermissions: Set<Permission>? = null) :
    Exception(
        "User with id ${user?.id ?: "NOT_LOGGED_IN"} is not allowed to execute action \"$action\".${
            requiredPermissions?.let { " One of the permissions $requiredPermissions is required, user has ${user?.permissions ?: emptySet()}." } ?: ""
        }",
    ) {
    public val unauthorized: Boolean = user == null
}

public class MiniSessionException(user: User, action: String) : Exception(
    "User with id ${user.id} is not allowed to execute action \"$action\" because the session currently active is a minimal session",
)
