package dev.maximilian.feather.gdpr

import java.time.Instant

public data class GdprDocument(
    val id: Int,
    val creationDate: Instant,
    val forceDate: Instant,
    val validDate: Instant,
    val content: String,
)
