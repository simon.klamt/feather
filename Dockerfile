FROM alpine:3.18

WORKDIR /usr/local/app

RUN apk upgrade --no-cache && \
    apk add --no-cache openjdk17-jre-headless && \
    addgroup -S app && \
    adduser -S -G app app && \
    chown app:app /usr/local/app

USER app

COPY core/build/install/ ./

CMD ["/usr/local/app/feather/bin/feather"]
